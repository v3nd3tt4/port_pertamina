/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.32-MariaDB : Database - db_penjadwalan_pelabuhan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_penjadwalan_pelabuhan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_penjadwalan_pelabuhan`;

/*Table structure for table `detailjetty` */

DROP TABLE IF EXISTS `detailjetty`;

CREATE TABLE `detailjetty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpelabuhan` int(11) DEFAULT NULL,
  `idproduk` int(11) DEFAULT NULL,
  `idjetty` int(11) DEFAULT NULL,
  `kap` int(11) DEFAULT NULL,
  `occ` int(11) DEFAULT NULL,
  `jalurpipa` int(11) DEFAULT NULL,
  `diameterpipa` int(11) DEFAULT NULL,
  `satuandiameter` int(11) DEFAULT NULL,
  `utilisasi` text,
  `maxload` int(11) DEFAULT NULL,
  `kedalamanair` int(11) DEFAULT NULL,
  `flowratemax` int(11) DEFAULT NULL,
  `ket` text,
  PRIMARY KEY (`id`),
  KEY `FK_detailjetty` (`idjetty`),
  CONSTRAINT `FK_detailjetty` FOREIGN KEY (`idjetty`) REFERENCES `listjetty` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `detailjetty` */

insert  into `detailjetty`(`id`,`idpelabuhan`,`idproduk`,`idjetty`,`kap`,`occ`,`jalurpipa`,`diameterpipa`,`satuandiameter`,`utilisasi`,`maxload`,`kedalamanair`,`flowratemax`,`ket`) values 
(1,1,2,1,6500,62,1,8,1,'Discharge, Back Loading',NULL,12,NULL,'as'),
(2,1,1,1,6500,62,2,8,1,'asas',NULL,12,NULL,'as');

/*Table structure for table `detailshipment` */

DROP TABLE IF EXISTS `detailshipment`;

CREATE TABLE `detailshipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idshipment` int(11) DEFAULT NULL,
  `idproduk` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `idsatuan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_detailshipment` (`idproduk`),
  KEY `FK_detailshipment1` (`idsatuan`),
  CONSTRAINT `FK_detailshipment` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`id`),
  CONSTRAINT `FK_detailshipment1` FOREIGN KEY (`idsatuan`) REFERENCES `listsatuan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `detailshipment` */

insert  into `detailshipment`(`id`,`idshipment`,`idproduk`,`jumlah`,`idsatuan`) values 
(1,1,1,200,1),
(2,2,1,200,1),
(3,3,1,200,1);

/*Table structure for table `estimasiwaktu` */

DROP TABLE IF EXISTS `estimasiwaktu`;

CREATE TABLE `estimasiwaktu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkapal` int(11) DEFAULT NULL,
  `idpelabuhan` int(11) DEFAULT NULL,
  `idlistket` int(11) DEFAULT NULL,
  `estimasiwaktu` time DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1' COMMENT '0=ubah2 1=fixed',
  PRIMARY KEY (`id`),
  KEY `FK_estimasiwaktu` (`idlistket`),
  KEY `FK_estimasiwaktu1` (`idpelabuhan`),
  KEY `FK_estimasiwaktu2` (`idkapal`),
  CONSTRAINT `FK_estimasiwaktu` FOREIGN KEY (`idlistket`) REFERENCES `listketerangan` (`id`),
  CONSTRAINT `FK_estimasiwaktu1` FOREIGN KEY (`idpelabuhan`) REFERENCES `pelabuhan` (`id`),
  CONSTRAINT `FK_estimasiwaktu2` FOREIGN KEY (`idkapal`) REFERENCES `kapal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `estimasiwaktu` */

insert  into `estimasiwaktu`(`id`,`idkapal`,`idpelabuhan`,`idlistket`,`estimasiwaktu`,`status`) values 
(1,1,1,1,'02:00:00','1'),
(2,1,1,2,'01:00:00','1'),
(3,1,1,3,'00:00:00','0'),
(4,1,1,4,'02:00:00','1'),
(5,1,1,5,'01:00:00','1'),
(6,2,1,1,'02:00:00','1'),
(7,2,1,2,'01:00:00','1');

/*Table structure for table `histstok` */

DROP TABLE IF EXISTS `histstok`;

CREATE TABLE `histstok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `idstok` int(11) DEFAULT NULL,
  `idshipment` int(11) DEFAULT NULL,
  `pumpable` int(11) DEFAULT NULL,
  `mutasi` int(11) DEFAULT NULL,
  `status` enum('real','proyeksi') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `histstok` */

insert  into `histstok`(`id`,`tanggal`,`idstok`,`idshipment`,`pumpable`,`mutasi`,`status`) values 
(1,'2018-01-01 00:00:00',1,0,400,0,'real'),
(2,'2018-01-02 00:00:00',1,NULL,200,0,'real'),
(3,'2018-01-02 00:00:00',1,1,NULL,200,'proyeksi');

/*Table structure for table `kapal` */

DROP TABLE IF EXISTS `kapal`;

CREATE TABLE `kapal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `idtipe` int(11) DEFAULT NULL,
  `kapasitas` int(11) DEFAULT NULL,
  `satuankapasitas` int(11) DEFAULT NULL,
  `flowrate` int(11) DEFAULT NULL,
  `satuanflowrate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_kapal` (`idtipe`),
  CONSTRAINT `FK_kapal` FOREIGN KEY (`idtipe`) REFERENCES `listtipekapal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `kapal` */

insert  into `kapal`(`id`,`nama`,`idtipe`,`kapasitas`,`satuankapasitas`,`flowrate`,`satuanflowrate`) values 
(1,'SPOB. Jelita Nadia',1,3000,1,200,NULL),
(2,'MT. Stephanie XVIII',1,3000,1,200,NULL),
(3,'MT. Satria Satu   ',1,3000,1,200,NULL),
(4,'OB. Patra 2304 – TB Patra 1204 ',1,3000,1,200,NULL),
(5,'MT. Almira XXII      ',5,1600,1,200,1),
(6,'MT. IRIANI',5,NULL,1,200,NULL),
(7,'MT. PETRO MARINE 2200',5,NULL,1,200,NULL),
(8,'OB. FLAMINGGO 9',2,NULL,1,200,NULL),
(9,'OB. FLAMINGO 8',2,NULL,1,200,NULL),
(10,'OB. OSCO PETRO V',1,NULL,1,200,NULL),
(11,'OB. SEJAHTERA 2016',2,NULL,1,200,NULL),
(12,'OB. SENTANA AGRO',2,NULL,1,200,NULL),
(13,'OB. SENTANA MULIA',2,NULL,1,200,NULL),
(14,'MT. MATINDOK',1,NULL,1,200,NULL),
(15,'SRIKANDI',3,NULL,1,200,NULL),
(16,'SPOB. CITRA S 4001',4,NULL,1,200,NULL),
(17,'MT. MARGARET XI',1,NULL,NULL,200,NULL),
(18,'SHINTA',2,NULL,1,200,NULL),
(19,'MT. BAHARI MAJU I',1,NULL,1,200,NULL);

/*Table structure for table `listjetty` */

DROP TABLE IF EXISTS `listjetty`;

CREATE TABLE `listjetty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `listjetty` */

insert  into `listjetty`(`id`,`nama`) values 
(1,'Jetty 1'),
(2,'Jetty 2'),
(3,'Jetty 3');

/*Table structure for table `listketerangan` */

DROP TABLE IF EXISTS `listketerangan`;

CREATE TABLE `listketerangan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(60) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1' COMMENT '0=fluktuatif 1=fixed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `listketerangan` */

insert  into `listketerangan`(`id`,`nama`,`status`) values 
(1,'Arrival - Berthed','1'),
(2,'Berthed - Comm(Load/Discharge)','1'),
(3,'Comm(Load/Discharge) - Comp(Load/Discharge)','0'),
(4,'Comp(Load/Discharge) - Unberthed','1'),
(5,'Unberthed - Departure','1'),
(6,'Departure - Tide','1');

/*Table structure for table `listsatuan` */

DROP TABLE IF EXISTS `listsatuan`;

CREATE TABLE `listsatuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `simbol` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `listsatuan` */

insert  into `listsatuan`(`id`,`nama`,`simbol`) values 
(1,'KiloLiter','KL'),
(2,'Liter','L'),
(3,'Inch','Inch');

/*Table structure for table `liststatus` */

DROP TABLE IF EXISTS `liststatus`;

CREATE TABLE `liststatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `liststatus` */

insert  into `liststatus`(`id`,`nama`) values 
(1,'ON SCHEDULE'),
(2,'DEVIATION'),
(3,'MOVE TO PORT ACTIVITY');

/*Table structure for table `listtipekapal` */

DROP TABLE IF EXISTS `listtipekapal`;

CREATE TABLE `listtipekapal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `listtipekapal` */

insert  into `listtipekapal`(`id`,`nama`) values 
(1,'OB 1'),
(2,'OB 2'),
(3,'SPOB 1'),
(4,'SPOB 2'),
(5,'Bulk Lighter'),
(6,'Small 1'),
(7,'Small 2'),
(8,'GP'),
(9,'GP dari Plaju');

/*Table structure for table `listwaiting` */

DROP TABLE IF EXISTS `listwaiting`;

CREATE TABLE `listwaiting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `listwaiting` */

insert  into `listwaiting`(`id`,`nama`) values 
(1,'WAITING PREPARATION'),
(2,'WAITING JETTY'),
(3,'WAITTING ULLAGE'),
(4,'WAITTING TIDE'),
(5,'WAITING PILOT'),
(6,'WAITING SHIP UNREADY'),
(7,'WAITING CARGO'),
(8,'WAITING CARGO DOCUMENT'),
(9,'WAITING WEATHER'),
(10,'WAITING SHIP DOCUMENT');

/*Table structure for table `pelabuhan` */

DROP TABLE IF EXISTS `pelabuhan`;

CREATE TABLE `pelabuhan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `pelabuhan` */

insert  into `pelabuhan`(`id`,`kode`,`nama`) values 
(1,'F123','Panjang'),
(2,'F126','Jambi'),
(3,'F127','Pangkal Balam'),
(4,'F129','Pulau Baai'),
(5,NULL,'TG. GEREM'),
(6,NULL,'TL. KABUNG'),
(7,NULL,'OTM'),
(8,NULL,'TG UBAN'),
(9,NULL,'Sambu');

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

insert  into `produk`(`id`,`nama`) values 
(1,'PREMIUM'),
(2,'PERTAMAX'),
(3,'SOLAR'),
(4,'PERTAMINA DEX'),
(5,'PERTAMAX TURBO'),
(6,'MFO'),
(7,'FAME');

/*Table structure for table `rute` */

DROP TABLE IF EXISTS `rute`;

CREATE TABLE `rute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkapal` int(11) DEFAULT NULL,
  `idasal` int(11) DEFAULT NULL COMMENT 'idpelabuhan asal kapal',
  `idtujuan` int(11) DEFAULT NULL COMMENT 'idpelabuahan tujuan kapal',
  `seatime` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rute` (`idkapal`),
  CONSTRAINT `FK_rute` FOREIGN KEY (`idkapal`) REFERENCES `kapal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `rute` */

insert  into `rute`(`id`,`idkapal`,`idasal`,`idtujuan`,`seatime`) values 
(1,1,4,1,'29:00:00'),
(2,1,4,5,'32:00:00'),
(3,1,4,6,'12:00:00'),
(4,2,4,1,'29:00:00'),
(5,2,4,5,'32:00:00'),
(6,2,4,6,'12:00:00'),
(7,3,4,1,'29:00:00'),
(8,3,4,5,'32:00:00'),
(9,3,4,6,'12:00:00'),
(10,4,4,1,'29:00:00'),
(11,4,4,5,'32:00:00'),
(12,4,4,6,'12:00:00'),
(13,5,4,1,'29:00:00'),
(14,5,4,5,'32:00:00'),
(15,5,4,6,'12:00:00'),
(16,2,1,3,'48:00:00'),
(17,2,3,5,'48:00:00'),
(18,2,3,7,'48:00:00'),
(19,2,3,8,'96:00:00'),
(20,2,3,9,'96:00:00'),
(21,2,4,5,'48:00:00'),
(22,2,4,7,'48:00:00'),
(23,5,4,6,'30:00:00');

/*Table structure for table `sandarjetty` */

DROP TABLE IF EXISTS `sandarjetty`;

CREATE TABLE `sandarjetty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idjetty` int(11) DEFAULT NULL,
  `idkapal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sandarjetty` (`idjetty`),
  KEY `FK_sandarjetty1` (`idkapal`),
  CONSTRAINT `FK_sandarjetty` FOREIGN KEY (`idjetty`) REFERENCES `listjetty` (`id`),
  CONSTRAINT `FK_sandarjetty1` FOREIGN KEY (`idkapal`) REFERENCES `kapal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sandarjetty` */

insert  into `sandarjetty`(`id`,`idjetty`,`idkapal`) values 
(1,1,2),
(2,1,6);

/*Table structure for table `shipment` */

DROP TABLE IF EXISTS `shipment`;

CREATE TABLE `shipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noshipment` varchar(40) DEFAULT NULL,
  `idkapal` int(11) DEFAULT NULL,
  `idasal` int(11) DEFAULT NULL,
  `idtujuan` int(11) DEFAULT NULL,
  `arrival` datetime DEFAULT NULL COMMENT 'waktu kedatangan kapal',
  `berthed` datetime DEFAULT NULL COMMENT 'waktu kapal sandar',
  `comm` datetime DEFAULT NULL COMMENT 'waktu kapal mulai muat/bongkat kargo',
  `comp` datetime DEFAULT NULL COMMENT 'waktu kapal selesai bongkar/muat kargo',
  `unberthed` datetime DEFAULT NULL COMMENT 'waktu kapal angkar jangkar',
  `departure` datetime DEFAULT NULL COMMENT 'waktu kapal berangkat',
  `waiting1` int(11) DEFAULT NULL,
  `waiting2` int(11) DEFAULT NULL,
  `waiting3` int(11) DEFAULT NULL,
  `waiting4` int(11) DEFAULT NULL,
  `waiting5` int(11) DEFAULT NULL,
  `status` enum('proses','done','simulasi') DEFAULT 'simulasi' COMMENT '0=proses 1=done',
  `antrian` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_shipment` (`idkapal`),
  CONSTRAINT `FK_shipment` FOREIGN KEY (`idkapal`) REFERENCES `kapal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `shipment` */

insert  into `shipment`(`id`,`noshipment`,`idkapal`,`idasal`,`idtujuan`,`arrival`,`berthed`,`comm`,`comp`,`unberthed`,`departure`,`waiting1`,`waiting2`,`waiting3`,`waiting4`,`waiting5`,`status`,`antrian`) values 
(1,NULL,2,3,1,'2018-08-01 10:00:00','2018-08-01 12:00:00','2018-08-01 13:00:00','2018-08-01 14:00:00','2018-08-01 16:00:00','2018-08-01 17:00:00',NULL,NULL,NULL,NULL,NULL,'done',NULL),
(2,NULL,1,3,1,'2018-10-30 19:00:37','2018-10-30 21:00:37','2018-10-30 22:00:37','2018-10-30 23:00:37','2018-10-31 01:00:37','2018-10-31 02:00:37',NULL,NULL,NULL,NULL,NULL,'simulasi',NULL),
(3,NULL,1,4,1,'2018-11-02 19:00:34','2018-11-02 21:00:34','2018-11-02 22:00:34','2018-11-02 23:00:34','2018-11-03 01:00:34','2018-11-03 02:00:34',NULL,NULL,NULL,NULL,NULL,'simulasi',NULL);

/*Table structure for table `stok` */

DROP TABLE IF EXISTS `stok`;

CREATE TABLE `stok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpelabuhan` int(11) DEFAULT NULL,
  `idproduk` int(11) DEFAULT NULL,
  `pumpable` int(11) DEFAULT NULL,
  `dot` int(11) DEFAULT NULL,
  `safestok` int(11) DEFAULT NULL,
  `deadstok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `stok` */

insert  into `stok`(`id`,`idpelabuhan`,`idproduk`,`pumpable`,`dot`,`safestok`,`deadstok`) values 
(1,1,1,400,200,1200,200),
(2,3,1,800,200,800,200);

/*Table structure for table `tb_chat` */

DROP TABLE IF EXISTS `tb_chat`;

CREATE TABLE `tb_chat` (
  `id_chat` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(255) DEFAULT NULL,
  `tanggal` varchar(255) DEFAULT NULL,
  `isi_chat` text,
  `status` enum('read','unread') DEFAULT 'unread',
  PRIMARY KEY (`id_chat`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `tb_chat` */

insert  into `tb_chat`(`id_chat`,`id_user`,`tanggal`,`isi_chat`,`status`) values 
(1,'pertamina','2018-11-01 09:39:40','ddd','unread'),
(2,'pertamina','2018-11-01 09:39:45','test','unread'),
(3,'pertamina','2018-11-01 09:41:18','iaka','unread'),
(4,'pertamina','2018-11-01 09:41:19','','unread'),
(5,'pertamina','2018-11-01 09:47:58','tes','unread'),
(6,'pertamina','2018-11-01 09:48:01','ya','unread'),
(7,'pertamina','2018-11-01 09:48:32','','unread'),
(8,'pertamina','2018-11-01 09:48:41','','unread'),
(9,'pertamina','2018-11-01 09:49:06','test','unread'),
(10,'pertamina','2018-11-01 09:49:13','test lagi','unread'),
(11,'pertamina','2018-11-01 09:49:27','test lagi ya','unread'),
(12,'pertamina','2018-11-01 09:49:28','test lagi ya','unread'),
(13,'pertamina','2018-11-01 09:49:34','test lagi ya','unread'),
(14,'pertamina','2018-11-01 09:49:35','test lagi ya','unread'),
(15,'pertamina','2018-11-01 09:49:35','test lagi ya','unread'),
(16,'pertamina','2018-11-01 09:50:17','ok','unread'),
(17,'pertamina','2018-11-01 09:50:38','ya','unread'),
(18,'pertamina','2018-11-01 09:50:49','d','unread'),
(19,'pertamina','2018-11-01 09:50:50','d','unread'),
(20,'pertamina','2018-11-01 09:51:08','d','unread'),
(21,'pertamina','2018-11-01 09:51:56','yu','unread'),
(22,'pertamina','2018-11-01 09:52:04','mas adam','unread'),
(23,'pertamina','2018-11-01 09:52:58','ddd','unread'),
(24,'pertamina','2018-11-01 09:53:20','ega','unread'),
(25,'pertamina','2018-11-01 09:53:28','ya ta','unread'),
(26,'pertamina','2018-11-01 09:53:46','ini mas','unread'),
(27,'pertamina','2018-11-01 09:55:03','mas arief','unread'),
(28,'pertamina','2018-11-01 09:55:13','tes lagi','unread'),
(29,'pertamina','2018-11-01 09:56:15','ridho','unread'),
(30,'pertamina','2018-11-01 09:56:27','danzen','unread'),
(31,'pertamina','2018-11-01 09:56:45','test lagi','unread');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
