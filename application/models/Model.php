<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model {

    public function get($table) {
        return $this->db->get($table);
    }

	function insertdata($table, $data){
		return $this->db->insert($table, $data);
	}

	function getdataall($table){
		return $this->db->get($table);
	}

	function getdata($table, $param = array()){
		return $this->db->get_where($table, $param);
	}

	function updatedata($table, $param = array(), $data){       
        $this->db->where($param);
        return $this->db->update($table, $data); 

    }

    function removedata($table, $param = array()){        
        $this->db->where($param);
        return $this->db->delete($table); 
    }

    function kueri($query){
        return $this->db->query($query);
    }

    function getjoinwhere($table,$join = array(),$where = null) {
        $this->db->select('*');
        $this->db->from($table);
        foreach($join as $row) {
            $j = explode('#',$row);
            $this->db->join($j[0],$j[1]);
        }
        if($where!=null) {
            $this->db->where($where);
        }
        return $this->db->get();
    }

    public function konversitime($time){
        $pecahjam = explode(':', $time);
        $jam = $pecahjam[0];
        $menit = $pecahjam[1];
        $detik = $pecahjam[2];
        $waktu = '+'.$jam.' hour +'.$menit.' minutes +'.$detik.' seconds';
        return $waktu;
    }

    public function konversitimeminus($time){
        $pecahjam = explode(':', $time);
        $jam = $pecahjam[0];
        $menit = $pecahjam[1];
        $detik = $pecahjam[2];
        $waktu = '-'.$jam.' hour -'.$menit.' minutes -'.$detik.' seconds';
        return $waktu;
    }

    

}