<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class simulasiplanner extends CI_Controller {

    private $table = 'sandarjetty';
    private $table_jetty = 'listjetty';
    private $table_kapal = 'kapal';


    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
        
        $hakakses=$this->session->userdata('akses');
        if($hakakses=='planner minyak'){
            $id='0';
        }elseif($hakakses=='planner lpg'){
            $id='1';
        }

        $idpelabuhansession = $this->session->userdata('idpelabuhan');

        $parampelabuhan = $this->input->get('idpelabuhanbantuan', true);

        if(!empty($parampelabuhan)){
            $queryloading = "SELECT shipment.*,pelabuhan.nama as namapelabuhan from shipment JOIN detailshipment ON shipment.id = detailshipment.idshipment JOIN produk ON detailshipment.idproduk = produk.id JOIN pelabuhan ON shipment.idpelabuhanbantuan=pelabuhan.id where status in ('simulasi', 'proses') AND produk.jenis = '$id' and idpelabuhanbantuan = '$parampelabuhan'  group by shipment.id  order by proses ASC";
        }else{
            $queryloading = "SELECT shipment.*,pelabuhan.nama as namapelabuhan from shipment JOIN detailshipment ON shipment.id = detailshipment.idshipment JOIN produk ON detailshipment.idproduk = produk.id JOIN pelabuhan ON shipment.idpelabuhanbantuan=pelabuhan.id where status in ('simulasi', 'proses') AND produk.jenis = '$id' and idpelabuhanbantuan = '$idpelabuhansession'  group by shipment.id  order by proses ASC";
        }
        // var_dump($queryloading);exit();
		$data = array(
			'page' => 'simulasiplanner/data',
            'link' =>'',
            'menu' =>'simulasiplanner',		
            'data' => $this->Model->kueri("SELECT * from shipment JOIN detailshipment ON shipment.id = detailshipment.idshipment JOIN produk ON detailshipment.idproduk = produk.id where status in ('simulasi', 'proses') AND produk.jenis = '$id' "),
            // 'datadischarge' => $this->Model->kueri("SELECT shipment.*, pelabuhan.nama as namapelabuhan from shipment JOIN detailshipment ON shipment.id = detailshipment.idshipment JOIN produk ON detailshipment.idproduk = produk.id where status in ('simulasi', 'proses') AND produk.jenis = '$id' and proses = '1' group by shipment.id "),

            // 'dataloading' => $this->Model->kueri("SELECT shipment.*,pelabuhan.nama as namapelabuhan from shipment JOIN detailshipment ON shipment.id = detailshipment.idshipment JOIN produk ON detailshipment.idproduk = produk.id JOIN pelabuhan ON shipment.idpelabuhanbantuan=pelabuhan.id where status in ('simulasi', 'proses') AND produk.jenis = '$id' and proses = '0' group by shipment.id"),

            'dataloading' => $this->Model->kueri($queryloading),

            'waiting' => $this->Model->getdataall('listwaiting'),
            'jetty' => $this->Model->get($this->table_jetty)->result(),
            'kapal' => $this->Model->getdata($this->table_kapal,array('jenisangkut'=>$id))->result(),
            'produk' => $this->Model->getdata('produk',array('jenis'=>$id))->result(),
            'pelabuhan' => $this->Model->get('pelabuhan')->result(),
            'satuan' => $this->Model->get('listsatuan')->result(),
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li class="active">Simulasi Planner</li>
			</ol>',
            'script' => 'script/script_simulasiplanner'
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

    public function formtambah(){

        $data = array(
            'page' => 'simulasiplanner/formtambah',
            'link' =>'',
            'menu' =>'simulasiplanner',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li><a href="#">Simulasi Planner</a></li>
				<li class="active">Tambah Simulasi Planner</li>
			</ol>',
            'jetty' => $this->Model->get($this->table_jetty)->result(),
            'kapal' => $this->Model->get($this->table_kapal)->result(),
            'produk' => $this->Model->get('produk')->result(),
            'pelabuhan' => $this->Model->get('pelabuhan')->result(),
            'satuan' => $this->Model->get('listsatuan')->result(),
        );
        $this->load->view('template_pertamina/wrapper', $data);
       
    

    }

    public function prosessimulasi(){
        
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktusandar);
        $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($_POST['waktudatang'])));

        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktucomm = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktucomm);
        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

        $query = $this->Model->kueri("select * from kapal where id = 1 ");
        $flowrate = $query->row()->flowrate;
        $hitung = number_format($_POST['jumlah']/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
        
        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

        //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));



        //asal idpelabuhan ambil dari id asal post
        $query = $this->Model->kueri("select * from stok where idpelabuhan = 3");
        $data = $query->row();
        $ketahananstok = number_format(($data->pumpable - $_POST['jumlah'])/$data->dot);
        $dataasal = array(
            'stokawal' => $data->pumpable,
            'stokmutasi' => $_POST['jumlah'],
            'sisa' => $data->pumpable-$_POST['jumlah'],
            'ketahanstok' => $ketahananstok
        );


        //tujuan idpelabuhan ambil dari id tujuan post
        $query = $this->Model->kueri("select * from stok where idpelabuhan = 1");
        $data = $query->row();
        $ketahananstok = number_format(($data->pumpable - $_POST['jumlah'])/$data->dot);
        $datatujuan = array(
            'stokawal' => $data->pumpable,
            'stokmutasi' => $_POST['jumlah'],
            'sisa' => $data->pumpable-$_POST['jumlah'],
            'ketahanstok' => $ketahananstok
        );

        // $datetime1 = new DateTime($_POST['waktudatang']);
        // $datetime2 = new DateTime($waktuberangkat);
        // $interval = $datetime1->diff($datetime2);
        // $date = $interval->format('%H:%i:%s');
        // // $elapsed = $interval->format('%H:%i HRS');
        // $elapsed = date('H:i', strtotime($date)).' HRS';
        // // $elapsed=date_format(date_create($interval),'H:i');

        $date1 = new DateTime($_POST['waktudatang']);
        $date2 = new DateTime($waktuberangkat);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $minutes = $diff->i;
        $hours = $hours + ($diff->days*24);

        $elapsed = $hours.':'.$minutes.' HRS';

        $datawaktu = array(
            'idkapal' => $_POST['idkapal'],
            'idasal' => $_POST['idasal'],
            'idtujuan' => $_POST['idtujuan'],
            'idproduk' => $_POST['idproduk'],
            'jumlah' => $_POST['jumlah'],
            'idsatuan' => $_POST['idsatuan'],
            'waktu_datang' =>$_POST['waktudatang'],
            'waktu_sandar' => $waktusandar,
            'waktu_comm' => $waktucomm,
            'waktu_comp' => $waktucomp,
            'waktu_lepas' => $waktulepas,
            'waktu_berangkat' => $waktuberangkat,
            'ipt' => $elapsed

        ); //insert data waktu ke tabel shipment, dan detail shipment


        // var_dump($datawaktu);exit();
        $data = array(
            'page' => 'simulasiplanner/hasilprosessimulasi',
            'link' =>'',
            'menu' =>'simulasiplanner',
            'datawaktu' => $datawaktu, 
            'dataasal' => $dataasal,
            'datatujuan' => $datatujuan         
        );
        $this->load->view('template_pertamina/wrapper', $data);
    }

    public function simpansimulasi(){
        $datawaktu = $this->input->post('datawaktu', true);
        $dataasal = $this->input->post('asal', true);
        $datatujuan = $this->input->post('tujuan', true);

        $datawaktu = json_decode($datawaktu);
        $dataasal = json_decode($dataasal);
        $datatujuan = json_decode($datatujuan);
        
        $datawaktu1 = array(
            'idkapal' => $datawaktu->idkapal,
            'idasal' => $datawaktu->idasal,
            'idtujuan' => $datawaktu->idtujuan,
            'arrival' => $datawaktu->waktu_datang,
            'berthed' => $datawaktu->waktu_sandar,
            'comm' => $datawaktu->waktu_comm,
            'comp' => $datawaktu->waktu_comp,
            'unberthed' => $datawaktu->waktu_lepas,
            'departure' => $datawaktu->waktu_berangkat,
        );
        $this->db->trans_begin();

        $this->db->insert('shipment', $datawaktu1);
        $idshipment = $this->db->insert_id();


        $datadetailshipment = array(
            'idshipment' => $idshipment,
            'idproduk' => $datawaktu->idproduk,
            'jumlah' => $datawaktu->jumlah,
            'idsatuan' => $datawaktu->idsatuan
        );

        $this->db->insert('detailshipment', $datadetailshipment);
        if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
        }
        else
        {

                $this->db->trans_commit();
                redirect(base_url().'simulasiplanner');
        }
        // var_dump($datadetailshipment);exit();
    }

    public function prosessimulasinya($id){
        $update = $this->Model->updatedata('shipment', array('id' => $id), array('status' => 'proses'));
        if($update){

        }else{

        }
        redirect(base_url().'simulasiplanner');
    }

    public function donesimulasi($id){
        $update = $this->Model->updatedata('shipment', array('id' => $id), array('status' => 'done'));
        if($update){

        }else{

        }
        redirect(base_url().'simulasiplanner');
    }

    public function getWaktu(){
        $idtujuan = $this->input->post('idtujuan', true);
        $idasal = $this->input->post('idasal', true);
        $idkapal=$this->input->post('idkapal',true);
        $proses=$this->input->post('proses',true);
        $waktudatang=date('Y-m-d', strtotime($this->input->post('waktudatang', true)));
        $waitingullage = $this->input->post('val_waiting_ullage',true);
        $idjetty=$this->input->post('idjetty',true);
        $waitingullagenya = 0;
        if($proses=='0'){
            $query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where status='simulasi' and idpelabuhanbantuan='$idasal' and date(arrival)='$waktudatang' and idjetty='$idjetty' order by departure desc limit 1";
        }elseif($proses=='1'){
            $query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where status='simulasi' and idpelabuhanbantuan='$idtujuan' order by departure desc limit 1";  
            if($waitingullage != ''){
                $waitingullagenya = $waitingullage;
            }
        }
        
        $query1 = $this->Model->kueri($query);

        $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");

        if($query1->num_rows()==NULL){            
            $estwaktusandar = $query2->row()->estimasiwaktu;
            
        }else{
            $estwaktusandar = $query1->row()->departure;            
        }
        

        
        $waktu = $this->Model->konversitime($estwaktusandar);
        $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($_POST['waktudatang'])));        

        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktucomm = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktucomm);
        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;

        // $hitung = number_format($_POST['jumlah']/$flowrate);
        $sumjumlah = 0;
        for($i=0;$i<count($this->input->post('idproduk1', true));$i++){
            $sumjumlah += $this->input->post('jumlahproduk', true)[$i];
        }
        $hitung = $sumjumlah/$flowrate;
        $format = $hitung+($waitingullagenya*24).':00:00';
        // echo $format;
        $waktu = $this->Model->konversitime($format);
        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
        
        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

        //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

        // $datetime1 = new DateTime($_POST['waktudatang']);
        // $datetime2 = new DateTime($waktuberangkat);
        // $interval = $datetime1->diff($datetime2);
        // $date = $interval->format('%H:%i:%s');
        // // $elapsed = $interval->format('%H:%i HRS');
        // $elapsed = date('H:i', strtotime($date)).' HRS';

        $date1 = new DateTime($_POST['waktudatang']);
        $date2 = new DateTime($waktuberangkat);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $minutes = $diff->i;
        $hours = $hours + ($diff->days*24);
        $elapsed = $hours.':'.$minutes.' HRS';

        $datawaktu = array(            
            'waktu_datang' =>$_POST['waktudatang'],
            'waktu_sandar' => $waktusandar,
            'waktu_comm' => $waktucomm,
            'waktu_comp' => $waktucomp,
            'waktu_lepas' => $waktulepas,
            'waktu_berangkat' => $waktuberangkat,
            'ipt' => $elapsed,
            'jumlah' => $hitung
        );

        echo json_encode($datawaktu); //insert data waktu ke tabel shipment, dan detail shipment


    }

    public function e_getWaktu(){
        $idkapal=$this->input->post('e_idkapal');
         // var_dump($idkapal);
         // die();
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktusandar);
        $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($_POST['e_waktudatang'])));

        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktucomm = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktucomm);
        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;
        //var_dump($flowrate);
        $hitung = number_format($_POST['jumlah']/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
        
        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

        //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

        // $datetime1 = new DateTime($_POST['e_waktudatang']);
        // $datetime2 = new DateTime($waktuberangkat);
        // $interval = $datetime1->diff($datetime2);
        // $date = $interval->format('%H:%i:%s');
        // // $elapsed = $interval->format('%H:%i HRS');
        // $elapsed = date('H:i', strtotime($date)).' HRS';

        $date1 = new DateTime($_POST['e_waktudatang']);
        $date2 = new DateTime($waktuberangkat);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $minutes = $diff->i;
        $hours = $hours + ($diff->days*24);
        $elapsed = $hours.':'.$minutes.' HRS';

        $datawaktu = array(            
            'e_waktu_datang' =>$_POST['e_waktudatang'],
            'e_waktu_sandar' => $waktusandar,
            'e_waktu_comm' => $waktucomm,
            'e_waktu_comp' => $waktucomp,
            'e_waktu_lepas' => $waktulepas,
            'e_waktu_berangkat' => $waktuberangkat,
            'e_ipt' => $elapsed

        );

        echo json_encode($datawaktu); //insert data waktu ke tabel shipment, dan detail shipment


    }

    public function store(){

        // $queryseatime = $this->Model->getdata('rute', array('idkapal' =>  $this->input->post('idkapal', true), 'idasal' => $this->input->post('idasal', true), 'idtujuan'=> $this->input->post('idtujuan', true)));
        $idkapal = $this->input->post('idkapal', true);
        $idasal = $this->input->post('idasal', true);
        $idtujuan =  $this->input->post('idtujuan', true);
        $proses=$this->input->post('proses',true);
        if($proses=='0'){
            $idpelabuhanbantuan=$idasal;
            $idpelabuhanbantuan1=$idtujuan;
        }else{
            $idpelabuhanbantuan=$idtujuan;
            $idpelabuhanbantuan1=$idtujuan;
        }
        $queryseatime = $this->Model->kueri("
            SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
            ");

        if(empty($this->input->post('idproduk1', true))){
            echo '<script>alert("Produk harus ditambahkan terlebih dahulu");window.location.href = "'.base_url().'simulasiplanner"; </script>';
            exit();
        }

        if($this->input->post('proses', true) == 1){
            if($queryseatime->num_rows() > 0){
                $cekseatime = $this->seatime(date('Y-m-d H:i:s'), $queryseatime->row()->seatime, $this->input->post('waktudatang', true));
                if($cekseatime === false){
                    echo '<script>alert("waktu seatime tidak diperbolehkan");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                    exit();
                }
                
            }
        }
        $tglarrival = date('Y-m-d', strtotime($this->input->post('waktudatang', true)));

        $idjetty = $this->input->post('idjetty', true);

        $queryantrian = "SELECT * FROM shipment
        WHERE idjetty = '$idjetty' AND idpelabuhanbantuan = '$idpelabuhanbantuan' AND DATE(arrival) = '$tglarrival'";
        $exqueryantrian = $this->Model->kueri($queryantrian);

        if($exqueryantrian->num_rows() == 0){
            $antrian = 1;
        }else{
            $antrian = $exqueryantrian->row()->antrian + 1;
        }

        $queryantrian1 = "SELECT * FROM shipment
        WHERE idjetty = '$idjetty' AND idpelabuhanbantuan = '$idpelabuhanbantuan1' AND DATE(arrival) = '$tglarrival'";
        $exqueryantrian1 = $this->Model->kueri($queryantrian1);

        if($exqueryantrian1->num_rows() == 0){
            $antrian1 = 1;
        }else{
            $antrian1 = $exqueryantrian1->row()->antrian + 1;
        }
        
        
        if($this->input->post('proses', true) == '0'){
            //untuk simpan loading shipment proses 0 proses bantuan 0
            $datawaktu1 = array(
                'idkapal' => $this->input->post('idkapal', true),
                'idasal' => $this->input->post('idasal', true),
                'idtujuan' => $this->input->post('idtujuan', true),
                'arrival' => $this->input->post('waktudatang', true),
                'berthed' => $this->input->post('waktu_sandar', true),
                'comm' => $this->input->post('waktu_comm', true),
                'comp' => $this->input->post('waktu_comp', true),
                'unberthed' => $this->input->post('waktu_lepas', true),
                'departure' => $this->input->post('waktu_berangkat', true),
                'proses' => $this->input->post('proses', true),
                'idpelabuhanbantuan'=>$idpelabuhanbantuan,            
                'idjetty' => $this->input->post('idjetty', true),
                'antrian' => $antrian,
                'prosesbantuan'=>'0',
            );
            $this->db->trans_begin();

            $this->db->insert('shipment', $datawaktu1);

            $idshipment = $this->db->insert_id();
            
            $datadetailshipment = array();
            for($i=0;$i<count($this->input->post('idproduk1', true));$i++){
                $datadetailshipment[] = array(
                    'idshipment' => $idshipment,
                    'idproduk' => $this->input->post('idproduk1', true)[$i],
                    'jumlah' => $this->input->post('jumlahproduk', true)[$i],
                    'idsatuan' => $this->input->post('idsatuan1', true)[$i],
                );
            }
            $this->db->insert_batch('detailshipment', $datadetailshipment);
            $this->db->where(array('id' => $idshipment));
            $this->db->update('shipment', array('idbantuan' => $idshipment));

            //untuk simpan loading shipment proses 0 proses bantuan 1 

            $departure2=$this->input->post('waktu_berangkat', true);
            $queryseatime = $this->Model->kueri("
                SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
                ");
            if($queryseatime->num_rows() == 0){
                echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'welcome"; </script>';
                exit();
            }else{
                $waktu = $this->Model->konversitime($queryseatime->row()->seatime);
                //waktuarrival = waktu depature + seatime
                $waktuarrival2 = date('Y-m-d H:i:s',strtotime($waktu,strtotime($departure2)));
                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");
                $estwaktusandar2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktusandar2);
                $waktusandar2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktuarrival2)));  
                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
                $estwaktucomm2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktucomm2);
                $waktucomm2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktusandar2)));
                $query2 = $this->Model->kueri("select TIMEDIFF(comp,comm) as selisih from shipment where idbantuan='$idshipment' and prosesbantuan='0'");
                $estwaktucomp2 = $query2->row()->selisih;
                $waktu2 = $this->Model->konversitime($estwaktucomp2);
                $waktucomp2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktucomm2)));

                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
                $estwaktulepas2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktulepas2);
                $waktulepas2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktucomp2)));

                //berangkat
                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
                $estwaktuberangkat2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktuberangkat2);
                $waktuberangkat2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktulepas2)));

            }
            $datawaktu2 = array(
                'idkapal' => $this->input->post('idkapal', true),
                'idasal' => $this->input->post('idasal', true),
                'idtujuan' => $this->input->post('idtujuan', true),
                'arrival' => $waktuarrival2,
                'berthed' => $waktusandar2,
                'comm' => $waktucomm2,
                'comp' => $waktucomp2,
                'unberthed' => $waktulepas2,
                'departure' => $waktuberangkat2,
                'proses' => $this->input->post('proses', true),
                'idpelabuhanbantuan'=>$idpelabuhanbantuan1,            
                'idjetty' => $this->input->post('idjetty', true),
                'antrian' => $antrian1,
                'prosesbantuan'=>'1',
            );
            // $this->db->trans_begin();

            $this->db->insert('shipment', $datawaktu2);

            $idshipment2 = $this->db->insert_id();
            
            $datadetailshipment = array();
            for($i=0;$i<count($this->input->post('idproduk1', true));$i++){
                $datadetailshipment[] = array(
                    'idshipment' => $idshipment2,
                    'idproduk' => $this->input->post('idproduk1', true)[$i],
                    'jumlah' => $this->input->post('jumlahproduk', true)[$i],
                    'idsatuan' => $this->input->post('idsatuan1', true)[$i],
                );
            }
            $this->db->insert_batch('detailshipment', $datadetailshipment);
            $this->db->where(array('id' => $idshipment2));
            $this->db->update('shipment', array('idbantuan' => $idshipment));
            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
                    echo '<script>alert("Gagal disimpan");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                        exit();
            }
            else
            {

                    $this->db->trans_commit();
                    echo '<script>alert("Berhasil disimpan");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                        exit();
            }
        }else{
          // untuk simpan loading shipment proses 1
             $datawaktu3 = array(
                'idkapal' => $this->input->post('idkapal', true),
                'idasal' => $this->input->post('idasal', true),
                'idtujuan' => $this->input->post('idtujuan', true),
                'arrival' => $this->input->post('waktudatang', true),
                'berthed' => $this->input->post('waktu_sandar', true),
                'comm' => $this->input->post('waktu_comm', true),
                'comp' => $this->input->post('waktu_comp', true),
                'unberthed' => $this->input->post('waktu_lepas', true),
                'departure' => $this->input->post('waktu_berangkat', true),
                'proses' => $this->input->post('proses', true),
                'idpelabuhanbantuan'=>$idpelabuhanbantuan,            
                'idjetty' => $this->input->post('idjetty', true),
                'antrian' => $antrian,
                
            );
            $this->db->trans_begin();

            $this->db->insert('shipment', $datawaktu3);

            $idshipment = $this->db->insert_id();
            
            $datadetailshipment = array();
            for($i=0;$i<count($this->input->post('idproduk1', true));$i++){
                $datadetailshipment[] = array(
                    'idshipment' => $idshipment,
                    'idproduk' => $this->input->post('idproduk1', true)[$i],
                    'jumlah' => $this->input->post('jumlahproduk', true)[$i],
                    'idsatuan' => $this->input->post('idsatuan1', true)[$i],
                );
            }
            $this->db->insert_batch('detailshipment', $datadetailshipment);
            $this->db->where(array('id' => $idshipment));
            $this->db->update('shipment', array('idbantuan' => $idshipment));
            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
                    echo '<script>alert("Gagal disimpan");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                        exit();
            }
            else
            {

                    $this->db->trans_commit();
                    echo '<script>alert("Berhasil disimpan");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                        exit();
            }
        }
        
        
        
        

        
        

        // $datadetailshipment = array(
        //     'idshipment' => $idshipment,
        //     'idproduk' => $this->input->post('idproduk', true),
        //     'jumlah' => $this->input->post('jumlah', true),
        //     'idsatuan' => $this->input->post('idsatuan', true),
        // );

        
    }

    public function seatime($waktusekarang, $waktuseatime, $waktu_datang){
        $waktusekarang = $waktusekarang;
        $waktuseatime = $waktuseatime;
        $waktudatang = $waktu_datang;
        
        $waktu = $this->Model->konversitime($waktuseatime);
        $waktudiperbolehkan = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusekarang)));
        // echo $waktudiperbolehkan;
        if(strtotime($waktudatang) < strtotime($waktudiperbolehkan)){
            return false;
        }else{
            return true;
        }

    }

    public function proyeksistok() {
        
        $idproduk = $this->uri->segment(3);
        //ambil stok real
        $stokreal = $this->Model->getdata("stok",array("idpelabuhan"=>"1","idproduk"=>$idproduk))->row();
        if(!empty($stokreal)) {
            $stokawal = $stokreal->pumpable;
            $mutasi = $stokreal->dot;

            //dikurangin sampai seminggu tapi tergantung stok
            $tanggal_sekarang = date("Y-m-d");
            $arrayproyeksi = array();
            $mulai = new DateTime($tanggal_sekarang);
            $tanggal_proyeksi = $mulai->add(new DateInterval('P1D'));
            $mulaiproyeksi = new DateTime($tanggal_proyeksi->format('Y-m-d'));
            for($i=0;$i<7;$i++) {
                if($stokawal > 0) {
                    $j = $i + 1;
                    $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                    $arrayproyeksi[$i]['stok'] = $stokawal - $mutasi;
                    $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                    $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                    $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                    $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal - $mutasi ;
                    $stokawal = $arrayproyeksi[$i]['stok'];
                    $mulaiproyeksi->add(new DateInterval('P1D'));
                }
            }
            $data = array(
                "tanggalreal" => $tanggal_sekarang,
                "stokreal"    => $stokreal->pumpable,
                "mutasi"      => $stokreal->dot,
                "proyeksi"    => $arrayproyeksi
            );
            $this->load->view("simulasiplanner/ajaxproyeksi",$data);
        }
    }


    public function ambil_shipment(){

        $id = $this->input->post('id');
        $ambil = $this->Model->kueri("select * from shipment where id = $id ")->row();
        echo json_encode($ambil);

    }

    public function ambil_detail_shipment(){
        $id = $this->input->post('id');
        $ambil = $this->Model->kueri("select * from detailshipment where idshipment = $id ")->result();
        echo json_encode($ambil);
    }


    public function editsimulasi(){
        $this->load->view("simulasiplanner/formeditsimulasi");
    }

    public function proyeksistokasal() {
        $idpelabuhan = $this->input->post('idpelabuhan', true);
        $idkapal = $this->input->post('idkapal', true);
        $idtujuan = $this->input->post('idtujuan', true);
        $idproduk1 = $this->input->post('idproduk', true);
        $idproduk1 = explode('_', $idproduk1);   
        $idproduk = $idproduk1[0];  
        $jumlah = $idproduk1[1];  
        $tanggalset = $this->input->post('tgldatang', true);
        
        //ambil stok real
        $stokreal = $this->Model->getdata("stok",array("idpelabuhan"=>$idpelabuhan,"idproduk"=>$idproduk))->row();
        if(!empty($stokreal)) {
            $stokawal = $stokreal->pumpable;
            $mutasi = $stokreal->dot;

            //dikurangin sampai seminggu tapi tergantung stok
            $tanggal_sekarang = date("Y-m-d");

            $arrayproyeksi = array();
            $mulai = new DateTime($tanggal_sekarang);
            $tanggal_proyeksi = $mulai->modify('-3 days');
            $mulaiproyeksi = new DateTime($tanggal_proyeksi->format('Y-m-d'));
            for($i=0;$i<12;$i++) {
                if($stokawal > 0) {
                    $j = $i + 1;
                    
                    if($stokawal - $mutasi > 0){

                        if($i<2) {
                            $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                            $arrayproyeksi[$i]['stok'] =  $stokawal + $mutasi;
                            $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                            $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                            $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                            $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal - $mutasi ;
                            
                            $stokawal = $arrayproyeksi[$i]['stok'];
                            $mulaiproyeksi->add(new DateInterval('P1D'));
                        }else {
                            $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                            $arrayproyeksi[$i]['stok'] =  $stokawal - $mutasi;
                            $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                            $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                            $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                            $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal - $mutasi ;
                            
                            $stokawal = $arrayproyeksi[$i]['stok'];
                            $mulaiproyeksi->add(new DateInterval('P1D'));
                        }

                    }
                }
            }
            $data = array(
                "tanggalreal" => $tanggal_sekarang,
                "stokreal"    => $stokreal->pumpable,
                "mutasi"      => $stokreal->dot,
                "tanggalset" => $tanggalset,
                "jumlah"    => $jumlah, 
                "proyeksi"    => $arrayproyeksi,
                "idasal"    => $idpelabuhan,
                "idtujuan" => $idtujuan,
                "idkapal" => $idkapal,
            );
            $this->load->view("simulasiplanner/ajaxproyeksistokasal",$data);
        }
    }

    public function proyeksistoktujuan() {        
        $idpelabuhan = $this->input->post('idpelabuhan', true);
        $idproduk1 = $this->input->post('idproduk', true);
        $idproduk1 = explode('_', $idproduk1);   
        $idproduk = $idproduk1[0];  
        $jumlah = $idproduk1[1];  
        $tanggalset = $this->input->post('tgldatang', true);
        $idasal = $this->input->post('idasal', true);
        $idkapal = $this->input->post('idkapal', true);
        //ambil stok real
        $stokreal = $this->Model->getdata("stok",array("idpelabuhan"=>$idpelabuhan,"idproduk"=>$idproduk))->row();
        if(!empty($stokreal)) {

            $tanggal_sekarang = date("Y-m-d");
            $stokintransit = 0;
            //ambil shipment yang tanggal 
                $query = $this->Model->kueri("select *,sum(jumlah)as jumlah_total from shipment left join detailshipment on shipment.id=detailshipment.idshipment  
                    where idtujuan='$idpelabuhan' and date(arrival) = '$tanggal_sekarang' and idproduk='$idproduk'
                    group by shipment.id");
                if($query->num_rows() > 0) {
                    $stokawal = $stokreal->pumpable + $query->row()->jumlah_total;
                    $stokintransit = $stokawal; 
                }else {
                    $stokawal = $stokreal->pumpable;
                    $stokintransit = 0;
                }

            $mutasi = $stokreal->dot;

            //dikurangin sampai seminggu tapi tergantung stok
            $arrayproyeksi = array();
            $mulai = new DateTime($tanggal_sekarang);
            $tanggal_proyeksi = $mulai->add(new DateInterval('P1D'));
            $mulaiproyeksi = new DateTime($tanggal_proyeksi->format('Y-m-d'));
            // $mulaiproyeksi = new DateTime(date("Y-m-d"));
            for($i=0;$i<7;$i++) {
                if($stokawal > 0) {
                    $j = $i + 1;
                    if($stokawal - $mutasi > 0){
                        $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                        $arrayproyeksi[$i]['stok'] = $stokawal - $mutasi;
                        $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                        $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                        $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                        $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal + $mutasi ;
                        $stokawal = $arrayproyeksi[$i]['stok'];
                        $mulaiproyeksi->add(new DateInterval('P1D'));
                    }
                }
            }
            $data = array(
                "tanggalreal" => $tanggal_sekarang,
                "stokreal"    => $stokreal->pumpable,
                "stokintransit" => $stokintransit,
                "mutasi"      => $stokreal->dot,
                "proyeksi"    => $arrayproyeksi,
                "tanggalset" => $tanggalset,
                "jumlah"    => $jumlah,
                "idasal"    => $idpelabuhan,
                // "idasal" => $idasal,
                "idkapal" => $idkapal
            );
            $this->load->view("simulasiplanner/ajaxproyeksistoktujuan",$data);
        }
    }

    public function proyeksistoktujuandischarge(){
        $idpelabuhan = $this->input->post('idpelabuhan', true);
        $idproduk1 = $this->input->post('idproduk', true);
        $idproduk1 = explode('_', $idproduk1);   
        $idproduk = $idproduk1[0];  
        $jumlah = $idproduk1[1];  
        $tanggalset = $this->input->post('tgldatang', true);
        $idasal = $this->input->post('idasal', true);
        $idkapal = $this->input->post('idkapal', true);
        //ambil stok real
        $stokreal = $this->Model->getdata("stok",array("idpelabuhan"=>$idpelabuhan,"idproduk"=>$idproduk))->row();
        if(!empty($stokreal)) {

            $tanggal_sekarang = date("Y-m-d");
            $stokintransit = 0;
            //ambil shipment yang tanggal 
                $query = $this->Model->kueri("select *,sum(jumlah)as jumlah_total from shipment left join detailshipment on shipment.id=detailshipment.idshipment  
                    where idtujuan='$idpelabuhan' and date(arrival) = '$tanggal_sekarang' and idproduk='$idproduk'
                    group by shipment.id");
                $query2 = $this->Model->kueri("select * from stok where idproduk = '$idproduk' ");
                // $query3 = $this->model->kueri("select * from shipment join detailshipment on detailshipment.idshipment = shipment.id where idpelabuhanbantuan = '$idpelabuhan' and idproduk = '$idproduk'");
                if($query->num_rows() > 0) {
                    $stokawal = $stokreal->pumpable + $query->row()->jumlah_total;
                    $stokintransit = $stokawal; 
                    $stokdischarge = $query->row()->jumlah_total ;
                }else {
                    $stokawal = $stokreal->pumpable;
                    $stokintransit = 0;
                    $stokdischarge = 0;
                }

                $safestok = $query2->row()->safestok;
                $deadstok = $query2->row()->deadstok;
                $dotreal = $query2->row()->dot;
                $ullagereal = $safestok - $deadstok - $stokawal;

            $mutasi = $stokreal->dot;

            //dikurangin sampai seminggu tapi tergantung stok
            $arrayproyeksi = array();
            $mulai = new DateTime($tanggal_sekarang);
            $tanggal_proyeksi = $mulai->add(new DateInterval('P1D'));
            $mulaiproyeksi = new DateTime($tanggal_proyeksi->format('Y-m-d'));
            // $mulaiproyeksi = new DateTime(date("Y-m-d"));

            $awal       = new DateTime($tanggal_sekarang);
            $akhir = new DateTime($tanggalset); // tanggal sekarang berdasarkan tanggal di komputer
            $selisihawalakhir        = $akhir->diff($awal)->format("%a");

            // $perbedaanhari = date_diff($tanggal_sekarang, $tanggalset);
            // var_dump($perbedaanhari);
            for($i=0;$i<$selisihawalakhir;$i++) {
                if($stokawal > 0) {
                    $j = $i + 1;
                    if($stokawal - $mutasi > 0){

                        $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                        $tgl = $mulaiproyeksi->format('Y-m-d');
                        $queryloop = $this->Model->kueri("select *,sum(jumlah)as jumlah_total from shipment left join detailshipment on shipment.id=detailshipment.idshipment  
                    where idtujuan='$idpelabuhan' and date(arrival) = '".$tgl."' and idproduk='$idproduk'
                    group by shipment.id");
                        if($queryloop->num_rows() > 0){                            
                            $arrayproyeksi[$i]['stokdischargeloop'] = $queryloop->row()->jumlah_total;
                        }else{
                            $arrayproyeksi[$i]['stokdischargeloop'] = 0;
                        }

                        if($selisihawalakhir == $i+1){                            
                            $arrayproyeksi[$i]['stokdischargeloop'] = $jumlah;
                        }
                        $arrayproyeksi[$i]['stok'] = $stokawal - $mutasi;
                        $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                        $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                        $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                        $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal + $mutasi ;
                        // $arrayproyeksi[$i]['waitingullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal + $mutasi ;
                        $stokawal = $arrayproyeksi[$i]['stok'];
                        $mulaiproyeksi->add(new DateInterval('P1D'));
                    }
                }
            }
            $data = array(
                "tanggalreal" => $tanggal_sekarang,
                "ullagereal" => $ullagereal,
                "stokdischarge" => $stokdischarge,
                "dotreal" => $dotreal,
                "stokreal"    => $stokreal->pumpable,
                "stokintransit" => $stokintransit,
                "mutasi"      => $stokreal->dot,
                "proyeksi"    => $arrayproyeksi,
                "tanggalset" => $tanggalset,
                "jumlah"    => $jumlah,
                "idasal"    => $idpelabuhan,
                // "idasal" => $idasal,
                "idkapal" => $idkapal
            );
            $this->load->view("simulasiplanner/ajaxproyeksistoktujuandischarge",$data);
        }
    }

    function hapus_planner($id){
        // $id=$this->input->post('id');
        $hapus = $this->Model->removedata('shipment', array('id'=>$id) );
        $hapus2 = $this->Model->removedata('detailshipment', array('idshipment'=>$id) );
        if($hapus && $hapus2){
            echo '<script>alert("Berhasil dihapus");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                exit();
            
        }else{ 
            echo '<script>alert("Gagal dihapus");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                exit();             
        }
        
    }

    public function getwaktuedit(){
        $idshipment = $this->input->post('idshipment', true);
        $arrival = $this->input->post('arrival', true);
        $getshipment = $this->Model->getdata('shipment', array('id' => $idshipment));
        $getdetailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $idshipment));
        $idkapal = $getshipment->row()->idkapal;
        $idtujuan = $getshipment->row()->idtujuan;
        $idasal = $getshipment->row()->idasal;

        $query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where idtujuan='$idtujuan' order by departure desc limit 1";
        $query1 = $this->Model->kueri($query);

        $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");

        // if($query1->num_rows() == 0){
            
        //     $estwaktusandar = $query2->row()->estimasiwaktu;
            
        // }else{
        //     $estwaktusandar = $query1->row()->departure;
            
        // }
        $estwaktusandar = $query2->row()->estimasiwaktu;
        // var_dump($estwaktusandar);exit();
        $queryseatime = $this->Model->kueri("
            SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
            ");
        if($queryseatime->num_rows() == 0){
            echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'welcome"; </script>';
            exit();
        }

        $waktu = $this->Model->konversitime($queryseatime->row()->seatime);

        //waktuarrival = waktu depature + seatime
        // $waktuarrival = date('Y-m-d H:i:s',strtotime($waktu,strtotime($arrival)));
        $waktuarrival = $arrival;
        
        $waktu = $this->Model->konversitime($estwaktusandar);
        $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktuarrival)));        

        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktucomm = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktucomm);
        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;

        // $hitung = number_format($_POST['jumlah']/$flowrate);
        $sumjumlah = 0;
        foreach($getdetailshipment->result() as $rowdetailshipment){
            $sumjumlah += $rowdetailshipment->jumlah;
        }
        $hitung = $sumjumlah/$flowrate;

        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
        
        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

        //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

        // $datetime1 = new DateTime($_POST['waktudatang']);
        // $datetime2 = new DateTime($waktuberangkat);
        // $interval = $datetime1->diff($datetime2);
        // $date = $interval->format('%H:%i:%s');
        // // $elapsed = $interval->format('%H:%i HRS');
        // $elapsed = date('H:i', strtotime($date)).' HRS';

        $date1 = new DateTime($waktuarrival);
        $date2 = new DateTime($waktuberangkat);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $minutes = $diff->i;
        $hours = $hours + ($diff->days*24);
        $elapsed = $hours.':'.$minutes.' HRS';

        $datawaktu = array(            
            'waktu_datang' =>$waktuarrival,
            'seatime' => $queryseatime->row()->seatime,
            'waktu_sandar' => $waktusandar,
            'waktu_comm' => $waktucomm,
            'waktu_comp' => $waktucomp,
            'waktu_lepas' => $waktulepas,
            'waktu_berangkat' => $waktuberangkat,
            'ipt' => $elapsed,
            'jumlah' => $hitung
        );
        echo json_encode($datawaktu);
    }

    public function updatewaktusimulasiplanner(){

        $array = array(
            'id' => $this->input->post('idshipmenteditwaktu'),
            'arrival' => $this->input->post('waktuarrivaledit'),
            'berthed' => $this->input->post('waktuberthededit'),
            'comm' => $this->input->post('waktucommdedit'),
            'comp' => $this->input->post('waktucompdedit'),
            'unberthed' => $this->input->post('waktuunberthededit'),
            'departure' => $this->input->post('waktudepartureedit')
        );
        
        $update = $this->Model->updatedata('shipment', array('id' => $this->input->post('idshipmenteditwaktu')), $array);
        if($update){
            echo '<script>alert("Berhasil diupdate");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                exit();
            
        }else{ 
            echo '<script>alert("Gagal diupdate");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                exit();             
        }
    }

    public function updateantrian(){
        $idshipment = $this->input->post('idshipmentupdateantrian', true);
        $antrian = $this->input->post('editantrian', true);

        $query = "select * from shipment where id = '$idshipment'";
        $exequery = $this->Model->kueri($query);
        $idkapal = $exequery->row()->idkapal;
        $waktuarrival = $exequery->row()->arrival;
        $idpelabuhanbantuan = $exequery->row()->idpelabuhanbantuan;
        $idjetty = $exequery->row()->idjetty;
        $proses=$exequery->row()->proses;
        $prosesbantuan=$exequery->row()->prosesbantuan;
        $idasal=$exequery->row()->idasal;
        $idtujuan=$exequery->row()->idtujuan;
        $idbantuan=$exequery->row()->idbantuan;
        $querydetailshipment = "select * from detailshipment where idshipment = '$idshipment'";
        $getdetailshipment = $this->Model->kueri($querydetailshipment);
        //jika antrian 1
        if($antrian == 1){
            $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");
            $estwaktusandar = $query2->row()->estimasiwaktu;
            $waktu = $this->Model->konversitime($estwaktusandar);
            $waktusandar = date('Y-m-d H:i:s', strtotime($waktu, strtotime($waktuarrival)));
        }
        //jika antrian >1
        else{
            $antrian1= $antrian - 1;
            $query2 = $this->Model->kueri("select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where idpelabuhanbantuan='$idpelabuhanbantuan' and idjetty='$idjetty' and antrian= '$antrian1'");
            $waktusandar = $query2->row()->departure;
        }
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktucomm = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktucomm);
        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;

        // $hitung = number_format($_POST['jumlah']/$flowrate);
        $sumjumlah = 0;
        foreach($getdetailshipment->result() as $rowdetailshipment){
            $sumjumlah += $rowdetailshipment->jumlah;
        }
        $hitung = $sumjumlah/$flowrate;

        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
        
        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

        //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

        // $datetime1 = new DateTime($_POST['waktudatang']);
        // $datetime2 = new DateTime($waktuberangkat);
        // $interval = $datetime1->diff($datetime2);
        // $date = $interval->format('%H:%i:%s');
        // // $elapsed = $interval->format('%H:%i HRS');
        // $elapsed = date('H:i', strtotime($date)).' HRS';

        $date1 = new DateTime($waktuarrival);
        $date2 = new DateTime($waktuberangkat);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $minutes = $diff->i;
        $hours = $hours + ($diff->days*24);
        $elapsed = $hours.':'.$minutes.' HRS';

        $data = array(
            'antrian' => $antrian,
            'berthed' => $waktusandar,
            'comm' => $waktucomm,
            'comp' => $waktucomp,
            'unberthed' => $waktulepas,
            'departure' => $waktuberangkat,
        );

        $update = $this->Model->updatedata('shipment', array('id' => $idshipment), $data);
        if($proses=='0' and $prosesbantuan=='0'){

            $departure2=$waktuberangkat;
            $queryseatime = $this->Model->kueri("
                SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
                ");
            if($queryseatime->num_rows() == 0){
                echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                exit();
            }else{
                $waktu = $this->Model->konversitime($queryseatime->row()->seatime);
                //waktuarrival = waktu depature + seatime
                $waktuarrival2 = date('Y-m-d H:i:s',strtotime($waktu,strtotime($departure2)));
                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");
                $estwaktusandar2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktusandar2);
                $waktusandar2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktuarrival2)));  
                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
                $estwaktucomm2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktucomm2);
                $waktucomm2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktusandar2)));
                
                $query2 = $this->Model->kueri("select TIMEDIFF(comp,comm) as selisih from shipment where idbantuan='$idshipment' and prosesbantuan='0'");
                $estwaktucomp2 = $query2->row()->selisih;
                $waktu2 = $this->Model->konversitime($estwaktucomp2);
                $waktucomp2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktucomm2)));

                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
                $estwaktulepas2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktulepas2);
                $waktulepas2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktucomp2)));

                //berangkat
                $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
                $estwaktuberangkat2 = $query2->row()->estimasiwaktu;
                $waktu2 = $this->Model->konversitime($estwaktuberangkat2);
                $waktuberangkat2 = date('Y-m-d H:i:s',strtotime($waktu2,strtotime($waktulepas2)));

            }
            $datawaktu2 = array(
                'arrival' => $waktuarrival2,
                'berthed' => $waktusandar2,
                'comm' => $waktucomm2,
                'comp' => $waktucomp2,
                'unberthed' => $waktulepas2,
                'departure' => $waktuberangkat2,
            );
            $update2 = $this->Model->updatedata('shipment', array('idbantuan' => $idbantuan,'prosesbantuan'=>'1'), $datawaktu2);
        }
        if($update){
            echo '<script>alert("Berhasil diupdate");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                exit();
            
        }else{ 
            echo '<script>alert("Gagal diupdate");window.location.href = "'.base_url().'simulasiplanner"; </script>';
                exit();             
        }   
    }
}