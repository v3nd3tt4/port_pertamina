<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	private $table = 'produk';

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
		$data = array(
			'page' => 'produk/data',
            'link' =>'',
			'menu' =>'produk',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li class="active">List Produk</li>
			</ol>',
			'list' => $this->Model->get($this->table)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'produk/formtambah',
			'link' =>'',
			'menu' =>'produk',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Produk</a></li>
				<li class="active">Tambah Produk</li>
			</ol>',
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'produk');
		}
		$data = array(
			'page' => 'produk/formedit',
			'link' =>'',
            'menu' =>'produk',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Produk</a></li>
				<li class="active">Ubah Produk</li>
			</ol>',
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"nama" => $this->input->post('nama',true),
			"jenis" => $this->input->post('jenis',true)
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'produk');
	}

	public function update() {
		$data = array(
			"nama" => $this->input->post('nama',true),
			"jenis" => $this->input->post('jenis',true)
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'produk');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'produk');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'produk');
	}
}
