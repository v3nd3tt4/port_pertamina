<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

    public function index()
    {
        $data = array(
            'page' => 'laporan/formcari',
            'link' =>'',
            'menu' => 'laporan',
            'breadcumb' => '<ol class="breadcrumb">
                <li><a href="#">Settings</a></li>
                <li class="active">Filter Laporan</li>
            </ol>',
            'list' => '',
            'pelabuhan'=>$this->Model->getdata('pelabuhan')->result(),
        );
        $this->load->view('template_pertamina/wrapper', $data);
    }

    public function viewlaporan(){
        $data = array(
            'page' => 'laporan/viewlaporan',
            'link' =>'',
            'menu' => 'laporan',
            'breadcumb' => '<ol class="breadcrumb">
                <li><a href="#">Settings</a></li>
                <li class="active">View Laporan</li>
            </ol>',
            'list' => '',
        );
        $this->load->view('template_pertamina/wrapper', $data);
    }
}