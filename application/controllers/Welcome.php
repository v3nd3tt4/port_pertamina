<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $table = 'sandarjetty';
    private $table_jetty = 'listjetty';
    private $table_kapal = 'kapal';


    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }
	public function index()
	{
		if($this->session->userdata('akses') == "planner lpg"){
			$jenis = "('1')";
		}elseif($this->session->userdata('akses') == "planner minyak"){
			$jenis = "('0')";
		}else{
			$jenis = "('0','1')";
		}
		$idpelabuhan = $this->session->userdata('idpelabuhan');
		$kueriportschedule = $this->Model->kueri("SELECT * FROM shipment WHERE `status` = 'simulasi' AND idpelabuhanbantuan='$idpelabuhan' group by shipment.id ORDER BY berthed ASC");

		// $keuerijetty1 = $this->Model->kueri("SELECT * FROM shipment WHERE `status` = 'proses' AND (idtujuan = '$idpelabuhan' OR idasal = '$idpelabuhan') and idjetty = '1'  ORDER BY berthed ASC");
		$keuerijetty1=$this->Model->kueri("SELECT * FROM shipment WHERE `status` = 'proses' AND idpelabuhanbantuan = '$idpelabuhan' and idjetty = '1' group by shipment.id ORDER BY berthed ASC");

		// $keuerijetty2 = $this->Model->kueri("SELECT * FROM shipment WHERE `status` = 'proses' AND (idtujuan = '$idpelabuhan' OR idasal = '$idpelabuhan') and idjetty = '2' ORDER BY berthed ASC");
		$keuerijetty2 = $this->Model->kueri("SELECT * FROM shipment WHERE `status` = 'proses' AND idpelabuhanbantuan = '$idpelabuhan' and idjetty = '2' group by shipment.id ORDER BY berthed ASC");
		
		/*  from japalid */
		$datajetty = $this->Model->kueri("SELECT * FROM listjetty ORDER BY id ASC")->result(); 
		/* end japalid */

		$data = array(
			'page' => 'dashboard_adminator2',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
			</ol>',
			'menu' => base_url(),
			'data' => $kueriportschedule,
            'waiting' => $this->Model->getdataall('listwaiting'),
            'jetty' => $this->Model->get($this->table_jetty)->result(),
            'kapal' => $this->Model->get($this->table_kapal)->result(),
            'produk' => $this->Model->get('produk')->result(),
            'pelabuhan' => $this->Model->get('pelabuhan')->result(),
            'satuan' => $this->Model->get('listsatuan')->result(),
            'script' => 'script/script_dashboard',
            'isi_chat' => $this->Model->kueri("select * from tb_chat order by id_chat ASC"),
            'jetty1' => $keuerijetty1,
			'jetty2' => $keuerijetty2,
			'datajetty' => $datajetty

		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function simpan_chat(){
		$isi_chat = $this->input->post('isi_chat', true);
		$iduser = '';
		if($this->input->post('id_user')=='Aplikasi') {
			$iduser = '';
		}else {
			$iduser = $this->session->userdata('namauser');
		}

		$data = array(
			'id_user' => $iduser,
			'tanggal' => date('Y-m-d H:i:s'),
			'isi_chat' => $isi_chat,
		);
		$simpan = $this->Model->insertdata('tb_chat', $data);
		if($simpan){
			$status = array('status'=>'sukses');
		}else{
			$status = array('status'=>'gagal');
		}

		echo json_encode($status);
	}

	public function pilihportschedule(){

	}

	public function batal_ke_jetty($id){
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('idjetty' => null, 'status' => 'simulasi'));
		$delete = $this->Model->removedata('shipment', array('idbantuan' => $id, 'prosesbantuan' => '1'));
		if($update){
			echo '<script>alert("berhasil dibatalkan");window.location.href = "'.base_url().'welcome";</script>';		
		}else{
			echo '<script>alert("Gagal dibatalkan");window.location.href = "'.base_url().'welcome";</script>';		
		}
	}

	public function simpanjetty2arbe(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting1' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2arbe2(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting2' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2arbe3(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting3' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2arbe4(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting4' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2arbe5(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting5' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tgl1(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('arrival' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tgl2(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('berthed' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tgl3(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('comm' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tgl4(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('comp' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tgl5(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('unberthed' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tgl6(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('departure' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}


	public function simpanjetty1arbe(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting1' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1arbe2(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting2' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1arbe3(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting3' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1arbe4(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting4' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1arbe5(){
		$id = $this->input->post('id', true);
		$update = $this->Model->updatedata('shipment', array('id' => $id), array('waiting5' => $this->input->post('status', true)));
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1tgl1(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$arrival=$this->input->post('status',true);

		$query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
		$waktu = $this->Model->konversitime($estwaktusandar);
        $berthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($arrival)));

        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
		$waktu = $this->Model->konversitime($estwaktusandar);
        $comm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($berthed)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;
        $hitung = number_format($jumlah/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $comp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comm)));

        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
		$departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			'arrival'=>$arrival,
			'berthed'=>$berthed,
			'comm'=>$comm,
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);

		$update = $this->Model->updatedata('shipment', array('id' => $id), $data);

		/* created by vendetta update waktu yang loading*/
		$datatosaveportschedule = $this->getwaktudariwaktudeparture($departure, $id);
		$datatosaveportschedule = json_decode($datatosaveportschedule);

		$datatoupdateloading = array(
			'arrival'=>$datatosaveportschedule->waktu_datang,
			'berthed'=>$datatosaveportschedule->waktu_datang,
			'comm'=>$datatosaveportschedule->waktu_comm,
			'comp'=>$datatosaveportschedule->waktu_comp,
			'unberthed'=>$datatosaveportschedule->waktu_lepas,
			'departure'=>$datatosaveportschedule->waktu_berangkat,
		);
		$update1 = $this->Model->updatedata('shipment', array('idbantuan' => $id,  'prosesbantuan'=>'1'), $datatoupdateloading);
		/* created by vendetta update waktu yang loading*/
		
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1tgl2(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$berthed=$this->input->post('status',true);


        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
		$waktu = $this->Model->konversitime($estwaktusandar);
        $comm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($berthed)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;
        $hitung = number_format($jumlah/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $comp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comm)));

        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			
			'berthed'=>$berthed,
			'comm'=>$comm,
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);

		/* created by vendetta update waktu yang loading*/
		$datatosaveportschedule = $this->getwaktudariwaktudeparture($departure, $id);
		$datatosaveportschedule = json_decode($datatosaveportschedule);

		$datatoupdateloading = array(
			'arrival'=>$datatosaveportschedule->waktu_datang,
			'berthed'=>$datatosaveportschedule->waktu_datang,
			'comm'=>$datatosaveportschedule->waktu_comm,
			'comp'=>$datatosaveportschedule->waktu_comp,
			'unberthed'=>$datatosaveportschedule->waktu_lepas,
			'departure'=>$datatosaveportschedule->waktu_berangkat,
		);
		$update1 = $this->Model->updatedata('shipment', array('idbantuan' => $id,  'prosesbantuan'=>'1'), $datatoupdateloading);
		/* created by vendetta update waktu yang loading*/

		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tglarrival(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$arrival=$this->input->post('status',true);

		$query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
		$waktu = $this->Model->konversitime($estwaktusandar);
        $berthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($arrival)));

        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
		$waktu = $this->Model->konversitime($estwaktusandar);
        $comm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($berthed)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;
        $hitung = number_format($jumlah/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $comp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comm)));

        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			'arrival'=>$arrival,
			'berthed'=>$berthed,
			'comm'=>$comm,
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);
		if($update){
			$data = array('status' => 'sukses','berthed'=>$berthed);
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tglberthed(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$berthed=$this->input->post('status',true);

        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
        $estwaktusandar = $query->row()->estimasiwaktu;
		$waktu = $this->Model->konversitime($estwaktusandar);
        $comm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($berthed)));

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;
        $hitung = number_format($jumlah/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $comp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comm)));

        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			'berthed'=>$berthed,
			'comm'=>$comm,
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);
		if($update){
			$data = array('status' => 'sukses','berthed'=>$berthed);
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tglcomm(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$comm=$this->input->post('status',true);

        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;
        $hitung = number_format($jumlah/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $comp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comm)));

        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			
			'comm'=>$comm,
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tglcomp(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$comp=$this->input->post('status',true);

        
        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tglunberthed(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$unberthed=$this->input->post('status',true);

        
        

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			
			
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty2tgldeparture(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$departure=$this->input->post('status',true);

		$data=array(
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1tgl3(){
		
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$comm=$this->input->post('status',true);

		$query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
        $flowrate = $query->row()->flowrate;

        $hitung = number_format($jumlah/$flowrate);
        $format = $hitung.':00:00';
        $waktu = $this->Model->konversitime($format);
        $comp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comm)));

        //lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			'comm'=>$comm,
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id), $data);

		/* created by vendetta update waktu yang loading*/
		$datatosaveportschedule = $this->getwaktudariwaktudeparture($departure, $id);
		$datatosaveportschedule = json_decode($datatosaveportschedule);

		$datatoupdateloading = array(
			'arrival'=>$datatosaveportschedule->waktu_datang,
			'berthed'=>$datatosaveportschedule->waktu_datang,
			'comm'=>$datatosaveportschedule->waktu_comm,
			'comp'=>$datatosaveportschedule->waktu_comp,
			'unberthed'=>$datatosaveportschedule->waktu_lepas,
			'departure'=>$datatosaveportschedule->waktu_berangkat,
		);
		$update1 = $this->Model->updatedata('shipment', array('idbantuan' => $id,  'prosesbantuan'=>'1'), $datatoupdateloading);
		/* created by vendetta update waktu yang loading*/

		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1tgl4(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$jumlah=$this->db->query("SELECT sum(jumlah) as total FROM detailshipment where idshipment='$id' group by idshipment")->row()->total;
		$comp=$this->input->post('status',true);
		//lepas
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
        $estwaktulepas = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktulepas);
        $unberthed = date('Y-m-d H:i:s',strtotime($waktu,strtotime($comp)));

         //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

		$data=array(
			'comp'=>$comp,
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id), $data);

		 /* created by vendetta update waktu yang loading*/
		$datatosaveportschedule = $this->getwaktudariwaktudeparture($departure, $id);
		$datatosaveportschedule = json_decode($datatosaveportschedule);

		$datatoupdateloading = array(
			'arrival'=>$datatosaveportschedule->waktu_datang,
			'berthed'=>$datatosaveportschedule->waktu_datang,
			'comm'=>$datatosaveportschedule->waktu_comm,
			'comp'=>$datatosaveportschedule->waktu_comp,
			'unberthed'=>$datatosaveportschedule->waktu_lepas,
			'departure'=>$datatosaveportschedule->waktu_berangkat,
		);
		$update1 = $this->Model->updatedata('shipment', array('idbantuan' => $id,  'prosesbantuan'=>'1'), $datatoupdateloading);
		/* created by vendetta update waktu yang loading*/

		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1tgl5(){
		$id = $this->input->post('id', true);
		$idkapal=$this->input->post('idkapal', true);
		$unberthed=$this->input->post('status',true);
		 //berangkat
        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
        $estwaktuberangkat = $query->row()->estimasiwaktu;
        $waktu = $this->Model->konversitime($estwaktuberangkat);
        $departure = date('Y-m-d H:i:s',strtotime($waktu,strtotime($unberthed)));

        $data=array(
			
			'unberthed'=>$unberthed,
			'departure'=>$departure
		);
		$update = $this->Model->updatedata('shipment', array('id' => $id),$data);

        /* created by vendetta update waktu yang loading*/
		$datatosaveportschedule = $this->getwaktudariwaktudeparture($departure, $id);
		$datatosaveportschedule = json_decode($datatosaveportschedule);

		$datatoupdateloading = array(
			'arrival'=>$datatosaveportschedule->waktu_datang,
			'berthed'=>$datatosaveportschedule->waktu_datang,
			'comm'=>$datatosaveportschedule->waktu_comm,
			'comp'=>$datatosaveportschedule->waktu_comp,
			'unberthed'=>$datatosaveportschedule->waktu_lepas,
			'departure'=>$datatosaveportschedule->waktu_berangkat,
		);
		$update1 = $this->Model->updatedata('shipment', array('idbantuan' => $id,  'prosesbantuan'=>'1'), $datatoupdateloading);
		/* created by vendetta update waktu yang loading*/

		
		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function simpanjetty1tgl6(){
		$id = $this->input->post('id', true);

		/* created by vendetta update waktu yang loading*/
		$datatosaveportschedule = $this->getwaktudariwaktudeparture($departure, $id);
		$datatosaveportschedule = json_decode($datatosaveportschedule);

		$update = $this->Model->updatedata('shipment', array('id' => $id), array('departure' => $this->input->post('status', true)));

		$datatoupdateloading = array(
			'arrival'=>$datatosaveportschedule->waktu_datang,
			'berthed'=>$datatosaveportschedule->waktu_datang,
			'comm'=>$datatosaveportschedule->waktu_comm,
			'comp'=>$datatosaveportschedule->waktu_comp,
			'unberthed'=>$datatosaveportschedule->waktu_lepas,
			'departure'=>$datatosaveportschedule->waktu_berangkat,
		);
		$update1 = $this->Model->updatedata('shipment', array('idbantuan' => $id,  'prosesbantuan'=>'1'), $datatoupdateloading);
		/* created by vendetta update waktu yang loading*/

		if($update){
			$data = array('status' => 'sukses');
			echo json_encode($data);
		}else{
			$data = array('status' => 'gagal');
			echo json_encode($data);
		}
	}

	public function selesai_shipment($id){
		// $id = $this->input->post('id', true);
		// $idshipment = $id;
		// $getshipment = $this->Model->getdata('shipment', array('id' => $id));
		// $getdetailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $id));

		// $idkapal = $getshipment->row()->idkapal;
		// $idasal = $getshipment->row()->idasal;
		// $idtujuan = $getshipment->row()->idtujuan;

		// $waktudeparture = $getshipment->row()->departure;


		// $query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where idtujuan='$idtujuan' order by departure desc limit 1";
  //       $query1 = $this->Model->kueri($query);

  //       $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");

  //       // if($query1->num_rows() == 0){
            
  //       //     $estwaktusandar = $query2->row()->estimasiwaktu;
            
  //       // }else{
  //       //     $estwaktusandar = $query1->row()->departure;
            
  //       // }
  //       $estwaktusandar = $query2->row()->estimasiwaktu;
  //       // var_dump($estwaktusandar);exit();
  //       $queryseatime = $this->Model->kueri("
  //           SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
  //           ");
  //       if($queryseatime->num_rows() == 0){
  //       	echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'welcome"; </script>';
  //                   exit();
  //       }

  //       $waktu = $this->Model->konversitime($queryseatime->row()->seatime);

  //       //waktuarrival = waktu depature + seatime
  //       $waktuarrival = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktudeparture)));
        
  //       $waktu = $this->Model->konversitime($estwaktusandar);
  //       $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktuarrival)));        

  //       $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
  //       $estwaktucomm = $query->row()->estimasiwaktu;
  //       $waktu = $this->Model->konversitime($estwaktucomm);
  //       $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

  //       $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
  //       $flowrate = $query->row()->flowrate;

  //       // $hitung = number_format($_POST['jumlah']/$flowrate);
  //       $sumjumlah = 0;
  //       foreach($getdetailshipment->result() as $rowdetailshipment){
  //           $sumjumlah += $rowdetailshipment->jumlah;
  //       }
  //       $hitung = $sumjumlah/$flowrate;

  //       $format = $hitung.':00:00';
  //       $waktu = $this->Model->konversitime($format);
  //       $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
        
  //       //lepas
  //       $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
  //       $estwaktulepas = $query->row()->estimasiwaktu;
  //       $waktu = $this->Model->konversitime($estwaktulepas);
  //       $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

  //       //berangkat
  //       $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
  //       $estwaktuberangkat = $query->row()->estimasiwaktu;
  //       $waktu = $this->Model->konversitime($estwaktuberangkat);
  //       $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

  //       // $datetime1 = new DateTime($_POST['waktudatang']);
  //       // $datetime2 = new DateTime($waktuberangkat);
  //       // $interval = $datetime1->diff($datetime2);
  //       // $date = $interval->format('%H:%i:%s');
  //       // // $elapsed = $interval->format('%H:%i HRS');
  //       // $elapsed = date('H:i', strtotime($date)).' HRS';

  //       $date1 = new DateTime($waktuarrival);
  //       $date2 = new DateTime($waktuberangkat);

  //       $diff = $date2->diff($date1);

  //       $hours = $diff->h;
  //       $minutes = $diff->i;
  //       $hours = $hours + ($diff->days*24);
  //       $elapsed = $hours.':'.$minutes.' HRS';

  //       $datawaktu = array(            
  //           'waktu_datang' =>$waktuarrival,
  //           'seatime' => $queryseatime->row()->seatime,
  //           'waktu_sandar' => $waktusandar,
  //           'waktu_comm' => $waktucomm,
  //           'waktu_comp' => $waktucomp,
  //           'waktu_lepas' => $waktulepas,
  //           'waktu_berangkat' => $waktuberangkat,
  //           'ipt' => $elapsed,
  //           'jumlah' => $hitung
  //       );

  //       $datatosave = array(
  //       	'idkapal' => $idkapal,
  //           'idasal' => $idasal,
  //           'idtujuan' => $idtujuan,
  //           'arrival' => $waktuarrival,
  //           'berthed' => $waktusandar,
  //           'comm' => $waktucomm,
  //           'comp' => $waktucomp,
  //           'unberthed' => $waktulepas,
  //           'departure' => $waktuberangkat,
  //           'proses' => '0',
  //           'status' => 'simulasi',
  //           'idbantuan' => $idshipment,
  //           'prosesbantuan' => '1'
  //       );
  //       if($getshipment->row()->proses == '0' && $getshipment->row()->prosesbantuan == '0'){
  //       	$dataproduktosave = array();
	 //        $this->db->trans_begin();

	 //        $this->db->insert('shipment', $datatosave);

	 //        $idshipmentnew = $this->db->insert_id();
	 //        foreach ($getdetailshipment->result() as $row) {
	 //        	$dataproduktosave[] = array(
	 //        		'idshipment' => $idshipmentnew,
	 //                'idproduk' => $row->idproduk,
	 //                'jumlah' => $row->jumlah,
	 //                'idsatuan' => $row->idsatuan,
	 //        	);
	 //        }

	 //        $this->db->insert_batch('detailshipment', $dataproduktosave);

	 //        $this->db->update('shipment', array('status' => 'done'), array('id' => $idshipment));
	 //        if ($this->db->trans_status() === FALSE)
	 //        {
	 //            $this->db->trans_rollback();
	 //            echo '<script>alert("Gagal diselesaikan");window.location.href = "'.base_url().'welcome"; </script>';
	 //                exit();
	 //        }
	 //        else
	 //        {
	 //            $this->db->trans_commit();
	 //            echo '<script>alert("Berhasil diselesaikan");window.location.href = "'.base_url().'welcome"; </script>';
	 //                exit();
	 //        }
  //       }else{
        	$update = $this->Model->updatedata('shipment', array('id' => $id), array('status' => 'done','idjetty'=>null));
			if($update){
				echo '<script>alert("Berhasil diselesaikan");window.location.href = "'.base_url().'welcome"; </script>';
	                exit();
	            
	        }else{ 
	        	echo '<script>alert("Gagal diselesaikan");window.location.href = "'.base_url().'welcome"; </script>';
	                exit();	            
	        }
        // }
        

        // echo json_encode($datawaktu); //insert data waktu ke tabel shipment, dan detail shipment
        // exit();	

	}

	public function proses_jetty_1($id){
		$idpelabuhan = $this->session->userdata('idpelabuhan');
		$idshipment = $id;
		
		$kueri = "SELECT * FROM shipment WHERE idjetty = '1' AND idpelabuhanbantuan = '$idpelabuhan'";
		// echo $kueri;exit();
		$cek = $this->Model->kueri($kueri);

		if($cek->num_rows() == 1){
			echo '<script>alert("Tidak diizinkan karena masih ada kapal di jetty 1");window.location.href = "'.base_url().'welcome";</script>';	
			exit();			
		}else{
			
			$getshipment = $this->Model->getdata('shipment', array('id' => $id));
			$getdetailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $id));

			$idkapal = $getshipment->row()->idkapal;
			$idasal = $getshipment->row()->idasal;
			$idtujuan = $getshipment->row()->idtujuan;

			$waktudeparture = $getshipment->row()->departure;


			$query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where idtujuan='$idtujuan' order by departure desc limit 1";
	        $query1 = $this->Model->kueri($query);

	        $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");

	        // if($query1->num_rows() == 0){
	            
	        //     $estwaktusandar = $query2->row()->estimasiwaktu;
	            
	        // }else{
	        //     $estwaktusandar = $query1->row()->departure;
	            
	        // }
	        $estwaktusandar = $query2->row()->estimasiwaktu;
	        // var_dump($estwaktusandar);exit();
	        $queryseatime = $this->Model->kueri("
	            SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
	            ");
	        // var_dump($queryseatime->num_rows());
	        // exit();
	        //ini error karena dengan seatime tidak ditemuka harusnya jetty tidak masuk
	        if($queryseatime->num_rows() == 0){
	        	echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'welcome"; </script>';
	            exit();
	        }else{
	        	$update = $this->Model->updatedata('shipment', array('id' => $id), array('idjetty' => '1', 'idpelabuhanbantuan' => $idpelabuhan));
	        	$waktu = $this->Model->konversitime($queryseatime->row()->seatime);

		        //waktuarrival = waktu depature + seatime
		        $waktuarrival = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktudeparture)));
		        
		        $waktu = $this->Model->konversitime($estwaktusandar);
		        $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktuarrival)));        

		        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
		        $estwaktucomm = $query->row()->estimasiwaktu;
		        $waktu = $this->Model->konversitime($estwaktucomm);
		        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

		        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
		        $flowrate = $query->row()->flowrate;

		        // $hitung = number_format($_POST['jumlah']/$flowrate);
		        $sumjumlah = 0;
		        foreach($getdetailshipment->result() as $rowdetailshipment){
		            $sumjumlah += $rowdetailshipment->jumlah;
		        }
		        $hitung = $sumjumlah/$flowrate;

		        $format = $hitung.':00:00';
		        $waktu = $this->Model->konversitime($format);
		        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
		        
		        //lepas
		        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
		        $estwaktulepas = $query->row()->estimasiwaktu;
		        $waktu = $this->Model->konversitime($estwaktulepas);
		        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

		        //berangkat
		        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
		        $estwaktuberangkat = $query->row()->estimasiwaktu;
		        $waktu = $this->Model->konversitime($estwaktuberangkat);
		        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

		        // $datetime1 = new DateTime($_POST['waktudatang']);
		        // $datetime2 = new DateTime($waktuberangkat);
		        // $interval = $datetime1->diff($datetime2);
		        // $date = $interval->format('%H:%i:%s');
		        // // $elapsed = $interval->format('%H:%i HRS');
		        // $elapsed = date('H:i', strtotime($date)).' HRS';

		        $date1 = new DateTime($waktuarrival);
		        $date2 = new DateTime($waktuberangkat);

		        $diff = $date2->diff($date1);

		        $hours = $diff->h;
		        $minutes = $diff->i;
		        $hours = $hours + ($diff->days*24);
		        $elapsed = $hours.':'.$minutes.' HRS';

		        $datawaktu = array(            
		            'waktu_datang' =>$waktuarrival,
		            'seatime' => $queryseatime->row()->seatime,
		            'waktu_sandar' => $waktusandar,
		            'waktu_comm' => $waktucomm,
		            'waktu_comp' => $waktucomp,
		            'waktu_lepas' => $waktulepas,
		            'waktu_berangkat' => $waktuberangkat,
		            'ipt' => $elapsed,
		            'jumlah' => $hitung
		        );

		        $datatosave = array(
		        	'idkapal' => $idkapal,
		            'idasal' => $idasal,
		            'idtujuan' => $idtujuan,
		            'arrival' => $waktuarrival,
		            'berthed' => $waktusandar,
		            'comm' => $waktucomm,
		            'comp' => $waktucomp,
		            'unberthed' => $waktulepas,
		            'departure' => $waktuberangkat,
		            'proses' => '0',
		            'status' => 'simulasi',
		            'idbantuan' => $idshipment,
		            'prosesbantuan' => '1',
		            'idpelabuhanbantuan'=>$idtujuan,
		        );
		        if($getshipment->row()->proses == '0' && $getshipment->row()->prosesbantuan == '0'){
		        	$dataproduktosave = array();
			        $this->db->trans_begin();

			        $this->db->insert('shipment', $datatosave);

			        $idshipmentnew = $this->db->insert_id();
			        foreach ($getdetailshipment->result() as $row) {
			        	$dataproduktosave[] = array(
			        		'idshipment' => $idshipmentnew,
			                'idproduk' => $row->idproduk,
			                'jumlah' => $row->jumlah,
			                'idsatuan' => $row->idsatuan,
			        	);
			        }

			        $this->db->insert_batch('detailshipment', $dataproduktosave);

			        $this->db->update('shipment', array('status' => 'proses'), array('id' => $idshipment));
			        if ($this->db->trans_status() === FALSE)
			        {
			            $this->db->trans_rollback();
			            echo '<script>alert("Gagal diproses ke jetty 1");window.location.href = "'.base_url().'welcome"; </script>';
			                exit();
			        }
			        else
			        {
			            $this->db->trans_commit();
			            echo '<script>alert("Berhasil diproses ke jetty 1");window.location.href = "'.base_url().'welcome"; </script>';
			                exit();
			        }
		        }else{
					$update = $this->Model->updatedata('shipment', array('id' => $id), array('status' => 'proses'));
					if($update){
						echo '<script>alert("berhasil diproses ke jetty 1");window.location.href = "'.base_url().'welcome";</script>';		
					}else{
						echo '<script>alert("Gagal diproses ke jetty 1");window.location.href = "'.base_url().'welcome";</script>';		
					}
				}
	        }
		}

	}

	public function proses_jetty_2($id){
		$idpelabuhan = $this->session->userdata('idpelabuhan');
		$idshipment = $id;
		
		$kueri = "SELECT * FROM shipment WHERE idjetty = '2' AND idpelabuhanbantuan = '$idpelabuhan'";
		// echo $kueri;exit();
		$cek = $this->Model->kueri($kueri);

		if($cek->num_rows() == 1){
			echo '<script>alert("Tidak diizinkan karena masih ada kapal di jetty 2");window.location.href = "'.base_url().'welcome";</script>';	
			exit();			
		}else{
			
			$getshipment = $this->Model->getdata('shipment', array('id' => $id));
			$getdetailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $id));

			$idkapal = $getshipment->row()->idkapal;
			$idasal = $getshipment->row()->idasal;
			$idtujuan = $getshipment->row()->idtujuan;

			$waktudeparture = $getshipment->row()->departure;


			$query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where idtujuan='$idtujuan' order by departure desc limit 1";
	        $query1 = $this->Model->kueri($query);

	        $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");

	        // if($query1->num_rows() == 0){
	            
	        //     $estwaktusandar = $query2->row()->estimasiwaktu;
	            
	        // }else{
	        //     $estwaktusandar = $query1->row()->departure;
	            
	        // }
	        $estwaktusandar = $query2->row()->estimasiwaktu;
	        // var_dump($estwaktusandar);exit();
	        $queryseatime = $this->Model->kueri("
	            SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
	            ");
	        // var_dump($queryseatime->num_rows());
	        // exit();
	        //ini error karena dengan seatime tidak ditemuka harusnya jetty tidak masuk
	        if($queryseatime->num_rows() == 0){
	        	echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'welcome"; </script>';
	            exit();
	        }else{
	        	$update = $this->Model->updatedata('shipment', array('id' => $id), array('idjetty' => '2', 'idpelabuhanbantuan' => $idpelabuhan));
	        	$waktu = $this->Model->konversitime($queryseatime->row()->seatime);

		        //waktuarrival = waktu depature + seatime
		        $waktuarrival = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktudeparture)));
		        
		        $waktu = $this->Model->konversitime($estwaktusandar);
		        $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktuarrival)));        

		        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
		        $estwaktucomm = $query->row()->estimasiwaktu;
		        $waktu = $this->Model->konversitime($estwaktucomm);
		        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

		        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
		        $flowrate = $query->row()->flowrate;

		        // $hitung = number_format($_POST['jumlah']/$flowrate);
		        $sumjumlah = 0;
		        foreach($getdetailshipment->result() as $rowdetailshipment){
		            $sumjumlah += $rowdetailshipment->jumlah;
		        }
		        $hitung = $sumjumlah/$flowrate;

		        $format = $hitung.':00:00';
		        $waktu = $this->Model->konversitime($format);
		        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
		        
		        //lepas
		        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
		        $estwaktulepas = $query->row()->estimasiwaktu;
		        $waktu = $this->Model->konversitime($estwaktulepas);
		        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

		        //berangkat
		        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
		        $estwaktuberangkat = $query->row()->estimasiwaktu;
		        $waktu = $this->Model->konversitime($estwaktuberangkat);
		        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

		        // $datetime1 = new DateTime($_POST['waktudatang']);
		        // $datetime2 = new DateTime($waktuberangkat);
		        // $interval = $datetime1->diff($datetime2);
		        // $date = $interval->format('%H:%i:%s');
		        // // $elapsed = $interval->format('%H:%i HRS');
		        // $elapsed = date('H:i', strtotime($date)).' HRS';

		        $date1 = new DateTime($waktuarrival);
		        $date2 = new DateTime($waktuberangkat);

		        $diff = $date2->diff($date1);

		        $hours = $diff->h;
		        $minutes = $diff->i;
		        $hours = $hours + ($diff->days*24);
		        $elapsed = $hours.':'.$minutes.' HRS';

		        $datawaktu = array(            
		            'waktu_datang' =>$waktuarrival,
		            'seatime' => $queryseatime->row()->seatime,
		            'waktu_sandar' => $waktusandar,
		            'waktu_comm' => $waktucomm,
		            'waktu_comp' => $waktucomp,
		            'waktu_lepas' => $waktulepas,
		            'waktu_berangkat' => $waktuberangkat,
		            'ipt' => $elapsed,
		            'jumlah' => $hitung
		        );

		        $datatosave = array(
		        	'idkapal' => $idkapal,
		            'idasal' => $idasal,
		            'idtujuan' => $idtujuan,
		            'arrival' => $waktuarrival,
		            'berthed' => $waktusandar,
		            'comm' => $waktucomm,
		            'comp' => $waktucomp,
		            'unberthed' => $waktulepas,
		            'departure' => $waktuberangkat,
		            'proses' => '0',
		            'status' => 'simulasi',
		            'idbantuan' => $idshipment,
		            'prosesbantuan' => '1',
		            'idpelabuhanbantuan'=>$idtujuan,
		        );
		        if($getshipment->row()->proses == '0' && $getshipment->row()->prosesbantuan == '0'){
		        	$dataproduktosave = array();
			        $this->db->trans_begin();

			        $this->db->insert('shipment', $datatosave);

			        $idshipmentnew = $this->db->insert_id();
			        foreach ($getdetailshipment->result() as $row) {
			        	$dataproduktosave[] = array(
			        		'idshipment' => $idshipmentnew,
			                'idproduk' => $row->idproduk,
			                'jumlah' => $row->jumlah,
			                'idsatuan' => $row->idsatuan,
			        	);
			        }

			        $this->db->insert_batch('detailshipment', $dataproduktosave);

			        $this->db->update('shipment', array('status' => 'proses'), array('id' => $idshipment));
			        if ($this->db->trans_status() === FALSE)
			        {
			            $this->db->trans_rollback();
			            echo '<script>alert("Gagal diproses ke jetty 1");window.location.href = "'.base_url().'welcome"; </script>';
			                exit();
			        }
			        else
			        {
			            $this->db->trans_commit();
			            echo '<script>alert("Berhasil diproses ke jetty 2");window.location.href = "'.base_url().'welcome"; </script>';
			                exit();
			        }
		        }else{
					$update = $this->Model->updatedata('shipment', array('id' => $id), array('status' => 'proses'));
					if($update){
						echo '<script>alert("berhasil diproses ke jetty 2");window.location.href = "'.base_url().'welcome";</script>';		
					}else{
						echo '<script>alert("Gagal diproses ke jetty 2");window.location.href = "'.base_url().'welcome";</script>';		
					}
				}
	        }
		}
	}

	// public function proses_jetty_2($id){
	// 	$idshipment = $id;
	// 	$idpelabuhan = $this->session->userdata('idpelabuhan');		

	// 	$cek = $this->Model->kueri("SELECT * FROM shipment WHERE idjetty = '2' AND idpelabuhanbantuan = '$idpelabuhan'");

	// 	if($cek->num_rows() == 1){
	// 		echo '<script>alert("Tidak diizinkan karena masih ada kapal di jetty 2");window.location.href = "'.base_url().'welcome";</script>';	
	// 	}else{
	// 		$update = $this->Model->updatedata('shipment', array('id' => $id), array('idjetty' => '2', 'idpelabuhanbantuan' => $idpelabuhan));
	// 		$getshipment = $this->Model->getdata('shipment', array('id' => $id));
	// 		$getdetailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $id));

	// 		$idkapal = $getshipment->row()->idkapal;
	// 		$idasal = $getshipment->row()->idasal;
	// 		$idtujuan = $getshipment->row()->idtujuan;

	// 		$waktudeparture = $getshipment->row()->departure;


	// 		$query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where idtujuan='$idtujuan' order by departure desc limit 1";
	//         $query1 = $this->Model->kueri($query);

	//         $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");

	//         // if($query1->num_rows() == 0){
	            
	//         //     $estwaktusandar = $query2->row()->estimasiwaktu;
	            
	//         // }else{
	//         //     $estwaktusandar = $query1->row()->departure;
	            
	//         // }
	//         $estwaktusandar = $query2->row()->estimasiwaktu;
	//         // var_dump($estwaktusandar);exit();
	//         $queryseatime = $this->Model->kueri("
	//             SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
	//             ");
	//         if($queryseatime->num_rows() == 0){
	//         	echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'welcome"; </script>';
	//                     exit();
	//         }

	//         $waktu = $this->Model->konversitime($queryseatime->row()->seatime);

	//         //waktuarrival = waktu depature + seatime
	//         $waktuarrival = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktudeparture)));
	        
	//         $waktu = $this->Model->konversitime($estwaktusandar);
	//         $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktuarrival)));        

	//         $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
	//         $estwaktucomm = $query->row()->estimasiwaktu;
	//         $waktu = $this->Model->konversitime($estwaktucomm);
	//         $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

	//         $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
	//         $flowrate = $query->row()->flowrate;

	//         // $hitung = number_format($_POST['jumlah']/$flowrate);
	//         $sumjumlah = 0;
	//         foreach($getdetailshipment->result() as $rowdetailshipment){
	//             $sumjumlah += $rowdetailshipment->jumlah;
	//         }
	//         $hitung = $sumjumlah/$flowrate;

	//         $format = $hitung.':00:00';
	//         $waktu = $this->Model->konversitime($format);
	//         $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
	        
	//         //lepas
	//         $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
	//         $estwaktulepas = $query->row()->estimasiwaktu;
	//         $waktu = $this->Model->konversitime($estwaktulepas);
	//         $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

	//         //berangkat
	//         $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
	//         $estwaktuberangkat = $query->row()->estimasiwaktu;
	//         $waktu = $this->Model->konversitime($estwaktuberangkat);
	//         $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

	//         // $datetime1 = new DateTime($_POST['waktudatang']);
	//         // $datetime2 = new DateTime($waktuberangkat);
	//         // $interval = $datetime1->diff($datetime2);
	//         // $date = $interval->format('%H:%i:%s');
	//         // // $elapsed = $interval->format('%H:%i HRS');
	//         // $elapsed = date('H:i', strtotime($date)).' HRS';

	//         $date1 = new DateTime($waktuarrival);
	//         $date2 = new DateTime($waktuberangkat);

	//         $diff = $date2->diff($date1);

	//         $hours = $diff->h;
	//         $minutes = $diff->i;
	//         $hours = $hours + ($diff->days*24);
	//         $elapsed = $hours.':'.$minutes.' HRS';

	//         $datawaktu = array(            
	//             'waktu_datang' =>$waktuarrival,
	//             'seatime' => $queryseatime->row()->seatime,
	//             'waktu_sandar' => $waktusandar,
	//             'waktu_comm' => $waktucomm,
	//             'waktu_comp' => $waktucomp,
	//             'waktu_lepas' => $waktulepas,
	//             'waktu_berangkat' => $waktuberangkat,
	//             'ipt' => $elapsed,
	//             'jumlah' => $hitung
	//         );

	//         $datatosave = array(
	//         	'idkapal' => $idkapal,
	//             'idasal' => $idasal,
	//             'idtujuan' => $idtujuan,
	//             'arrival' => $waktuarrival,
	//             'berthed' => $waktusandar,
	//             'comm' => $waktucomm,
	//             'comp' => $waktucomp,
	//             'unberthed' => $waktulepas,
	//             'departure' => $waktuberangkat,
	//             'proses' => '0',
	//             'status' => 'simulasi',
	//             'idbantuan' => $idshipment,
	//             'prosesbantuan' => '1',
	//             'idpelabuhanbantuan'=>$idtujuan,
	//         );
	//         if($getshipment->row()->proses == '0' && $getshipment->row()->prosesbantuan == '0'){
	//         	$dataproduktosave = array();
	// 	        $this->db->trans_begin();

	// 	        $this->db->insert('shipment', $datatosave);

	// 	        $idshipmentnew = $this->db->insert_id();
	// 	        foreach ($getdetailshipment->result() as $row) {
	// 	        	$dataproduktosave[] = array(
	// 	        		'idshipment' => $idshipmentnew,
	// 	                'idproduk' => $row->idproduk,
	// 	                'jumlah' => $row->jumlah,
	// 	                'idsatuan' => $row->idsatuan,
	// 	        	);
	// 	        }

	// 	        $this->db->insert_batch('detailshipment', $dataproduktosave);

	// 	        $this->db->update('shipment', array('status' => 'proses'), array('id' => $idshipment));
	// 	        if ($this->db->trans_status() === FALSE)
	// 	        {
	// 	            $this->db->trans_rollback();
	// 	            echo '<script>alert("Gagal diproses ke jetty 2");window.location.href = "'.base_url().'welcome"; </script>';
	// 	                exit();
	// 	        }
	// 	        else
	// 	        {
	// 	            $this->db->trans_commit();
	// 	            echo '<script>alert("Berhasil diproses ke jetty 2");window.location.href = "'.base_url().'welcome"; </script>';
	// 	                exit();
	// 	        }
	//         }else{

	// 			$update = $this->Model->updatedata('shipment', array('id' => $id), array('status' => 'proses', 'idpelabuhanbantuan' => $idpelabuhan));
	// 			if($update){
	// 				echo '<script>alert("berhasil diproses ke jetty 2");window.location.href = "'.base_url().'welcome";</script>';		
	// 			}else{
	// 				echo '<script>alert("Gagal diproses ke jetty 2");window.location.href = "'.base_url().'welcome";</script>';		
	// 			}
	// 		}
				
	// 	}

	// }

	/* created by vendetta 2018-11-05 19:44:00 $waktudeparture dalam bentuk strtotime*/
	public function getwaktudariwaktudeparture($waktudeparture, $idshipment=""){
		$id = $idshipment;
		$getshipment = $this->Model->getdata('shipment', array('id' => $id));
		$getdetailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $id));

		$idkapal = $getshipment->row()->idkapal;
		$idasal = $getshipment->row()->idasal;
		$idtujuan = $getshipment->row()->idtujuan;

		$waktudeparture = $getshipment->row()->departure;


		$query = "select DATE_ADD(departure,INTERVAL 1 hour) as departure from shipment where idtujuan='$idtujuan' order by departure desc limit 1";
        $query1 = $this->Model->kueri($query);

        $query2 = $this->Model->kueri("select * from estimasiwaktu where idlistket = 1 ");

        // if($query1->num_rows() == 0){
            
        //     $estwaktusandar = $query2->row()->estimasiwaktu;
            
        // }else{
        //     $estwaktusandar = $query1->row()->departure;
            
        // }
        $estwaktusandar = $query2->row()->estimasiwaktu;
        // var_dump($estwaktusandar);exit();
        $queryseatime = $this->Model->kueri("
            SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )
            ");
        // var_dump($queryseatime->num_rows());
        // exit();
        //ini error karena dengan seatime tidak ditemuka harusnya jetty tidak masuk
        if($queryseatime->num_rows() == 0){
        	echo '<script>alert("Seatime tidak ditemukan");window.location.href = "'.base_url().'welcome"; </script>';
            exit();
        }else{
        
        	$waktu = $this->Model->konversitime($queryseatime->row()->seatime);

	        //waktuarrival = waktu depature + seatime
	        $waktuarrival = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktudeparture)));
	        
	        $waktu = $this->Model->konversitime($estwaktusandar);
	        $waktusandar = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktuarrival)));        

	        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 2 ");
	        $estwaktucomm = $query->row()->estimasiwaktu;
	        $waktu = $this->Model->konversitime($estwaktucomm);
	        $waktucomm = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktusandar)));

	        $query = $this->Model->kueri("select * from kapal where id = '$idkapal'");
	        $flowrate = $query->row()->flowrate;

	        // $hitung = number_format($_POST['jumlah']/$flowrate);
	        $sumjumlah = 0;
	        foreach($getdetailshipment->result() as $rowdetailshipment){
	            $sumjumlah += $rowdetailshipment->jumlah;
	        }
	        $hitung = $sumjumlah/$flowrate;

	        $format = $hitung.':00:00';
	        $waktu = $this->Model->konversitime($format);
	        $waktucomp = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomm)));
	        
	        //lepas
	        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 4 ");
	        $estwaktulepas = $query->row()->estimasiwaktu;
	        $waktu = $this->Model->konversitime($estwaktulepas);
	        $waktulepas = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktucomp)));

	        //berangkat
	        $query = $this->Model->kueri("select * from estimasiwaktu where idlistket = 5 ");
	        $estwaktuberangkat = $query->row()->estimasiwaktu;
	        $waktu = $this->Model->konversitime($estwaktuberangkat);
	        $waktuberangkat = date('Y-m-d H:i:s',strtotime($waktu,strtotime($waktulepas)));

	        // $datetime1 = new DateTime($_POST['waktudatang']);
	        // $datetime2 = new DateTime($waktuberangkat);
	        // $interval = $datetime1->diff($datetime2);
	        // $date = $interval->format('%H:%i:%s');
	        // // $elapsed = $interval->format('%H:%i HRS');
	        // $elapsed = date('H:i', strtotime($date)).' HRS';

	        $date1 = new DateTime($waktuarrival);
	        $date2 = new DateTime($waktuberangkat);

	        $diff = $date2->diff($date1);

	        $hours = $diff->h;
	        $minutes = $diff->i;
	        $hours = $hours + ($diff->days*24);
	        $elapsed = $hours.':'.$minutes.' HRS';

	        $datawaktu = array(            
	            'waktu_datang' =>$waktuarrival,
	            'seatime' => $queryseatime->row()->seatime,
	            'waktu_sandar' => $waktusandar,
	            'waktu_comm' => $waktucomm,
	            'waktu_comp' => $waktucomp,
	            'waktu_lepas' => $waktulepas,
	            'waktu_berangkat' => $waktuberangkat,
	            'ipt' => $elapsed,
	            'jumlah' => $hitung
	        );
	        return json_encode($datawaktu);	        
        }
	}
	/* created by vendetta 2018-11-05 19:44:00*/

	/*created by vendetta 2018-11-06 08:18:00*/
	public function proyeksistokasal() {
		$iddetailshipment = $this->input->post('iddetailshipment', true);
		$getdetailshipment = $this->Model->getdata('detailshipment', array('id' => $iddetailshipment));
		$idshipment = $getdetailshipment->row()->idshipment;
		$getshipment = $this->Model->getdata('shipment', array('id' => $idshipment));


        $idpelabuhan = $getshipment->row()->idasal;
        $idkapal = $getshipment->row()->idkapal;
        $idtujuan = $getshipment->row()->idtujuan;
         
        $idproduk = $getdetailshipment->row()->idproduk;  
        $jumlah = $getdetailshipment->row()->jumlah;  
        $tanggalset = $getshipment->row()->comm;
        
        //ambil stok real
        $stokreal = $this->Model->getdata("stok",array("idpelabuhan"=>$idpelabuhan,"idproduk"=>$idproduk))->row();
        if(!empty($stokreal)) {
            $stokawal = $stokreal->pumpable;
            $mutasi = $stokreal->dot;

            //dikurangin sampai seminggu tapi tergantung stok
            $tanggal_sekarang = date("Y-m-d");

            $arrayproyeksi = array();
            $mulai = new DateTime($tanggal_sekarang);
            $tanggal_proyeksi = $mulai->modify('-3 days');
            $mulaiproyeksi = new DateTime($tanggal_proyeksi->format('Y-m-d'));
            for($i=0;$i<12;$i++) {
                if($stokawal > 0) {
                    $j = $i + 1;
                    
                    if($stokawal - $mutasi > 0){

                        if($i<2) {
                            $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                            $arrayproyeksi[$i]['stok'] =  $stokawal + $mutasi;
                            $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                            $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                            $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                            $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal - $mutasi ;
                            
                            $stokawal = $arrayproyeksi[$i]['stok'];
                            $mulaiproyeksi->add(new DateInterval('P1D'));
                        }else {
                            $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                            $arrayproyeksi[$i]['stok'] =  $stokawal - $mutasi;
                            $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                            $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                            $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                            $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal - $mutasi ;
                            
                            $stokawal = $arrayproyeksi[$i]['stok'];
                            $mulaiproyeksi->add(new DateInterval('P1D'));
                        }

                    }
                }
            }
            $data = array(
                "tanggalreal" => $tanggal_sekarang,
                "stokreal"    => $stokreal->pumpable,
                "mutasi"      => $stokreal->dot,
                "tanggalset" => $tanggalset,
                "jumlah"    => $jumlah, 
                "proyeksi"    => $arrayproyeksi,
                "idasal"    => $idpelabuhan,
                "idtujuan" => $idtujuan,
                "idkapal" => $idkapal,
            );
            $this->load->view("simulasiplanner/ajaxproyeksistokasal",$data);
        }
    }

    public function proyeksistoktujuan() {       
		$iddetailshipment = $this->input->post('iddetailshipment', true);
		$getdetailshipment = $this->Model->getdata('detailshipment', array('id' => $iddetailshipment));
		$idshipment = $getdetailshipment->row()->idshipment;
		$getshipment = $this->Model->getdata('shipment', array('id' => $idshipment));

		$idpelabuhan = $getshipment->row()->idtujuan;
        $idkapal = $getshipment->row()->idkapal;
        $idasal = $getshipment->row()->idasal;
         
        $idproduk = $getdetailshipment->row()->idproduk;  
        $jumlah = $getdetailshipment->row()->jumlah;  
        $tanggalset = $getshipment->row()->comm;
        
        //ambil stok real
        $stokreal = $this->Model->getdata("stok",array("idpelabuhan"=>$idpelabuhan,"idproduk"=>$idproduk))->row();
        if(!empty($stokreal)) {

            $tanggal_sekarang = date("Y-m-d");
            $stokintransit = 0;
            //ambil shipment yang tanggal 
                $query = $this->Model->kueri("select *,sum(jumlah)as jumlah_total from shipment left join detailshipment on shipment.id=detailshipment.idshipment  
                    where idtujuan='$idpelabuhan' and date(arrival) = '$tanggal_sekarang' and idproduk='$idproduk'
                    group by shipment.id");
                if($query->num_rows() > 0) {
                    $stokawal = $stokreal->pumpable + $query->row()->jumlah_total;
                    $stokintransit = $stokawal; 
                }else {
                    $stokawal = $stokreal->pumpable;
                    $stokintransit = 0;
                }

            $mutasi = $stokreal->dot;

            //dikurangin sampai seminggu tapi tergantung stok
            $arrayproyeksi = array();
            $mulai = new DateTime($tanggal_sekarang);
            $tanggal_proyeksi = $mulai->add(new DateInterval('P1D'));
            $mulaiproyeksi = new DateTime($tanggal_proyeksi->format('Y-m-d'));
            // $mulaiproyeksi = new DateTime(date("Y-m-d"));
            for($i=0;$i<7;$i++) {
                if($stokawal > 0) {
                    $j = $i + 1;
                    if($stokawal - $mutasi > 0){
                        $arrayproyeksi[$i]['tanggal'] = $mulaiproyeksi->format('Y-m-d');
                        $arrayproyeksi[$i]['stok'] = $stokawal - $mutasi;
                        $arrayproyeksi[$i]['ketahanan'] = $arrayproyeksi[$i]['stok']/$stokreal->dot;
                        $arrayproyeksi[$i]['deadstok'] = $stokreal->deadstok;
                        $arrayproyeksi[$i]['safestok'] = $stokreal->safestok;
                        $arrayproyeksi[$i]['ullage'] = $stokreal->safestok - $stokreal->deadstok - $stokawal + $mutasi ;
                        $stokawal = $arrayproyeksi[$i]['stok'];
                        $mulaiproyeksi->add(new DateInterval('P1D'));
                    }
                }
            }
            $data = array(
                "tanggalreal" => $tanggal_sekarang,
                "stokreal"    => $stokreal->pumpable,
                "stokintransit" => $stokintransit,
                "mutasi"      => $stokreal->dot,
                "proyeksi"    => $arrayproyeksi,
                "tanggalset" => $tanggalset,
                "jumlah"    => $jumlah,
                "idasal"    => $idpelabuhan,
                // "idasal" => $idasal,
                "idkapal" => $idkapal
            );
            $this->load->view("simulasiplanner/ajaxproyeksistoktujuan",$data);
        }
    }
	/*created by vendetta 2018-11-06 08:18:00*/

}
