<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends CI_Controller {

	private $table = 'stok';

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
		// $this->Model->getjoinwhere($this->table, );
		$query = $this->Model->kueri("select pelabuhan.nama as nama_pelabuhan, produk.nama as nama_produk, stok.*, stok.pumpable/stok.dot as ketahanan from stok join pelabuhan on stok.idpelabuhan = pelabuhan.id join produk on stok.idproduk = produk.id");
		$data = array(
			'page' => 'stok/data',
			'link' =>'',
			'list' => $query,
			'menu' => 'stok',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li class="active">Stok</li>
			</ol>',
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'stok/formtambah',
			'link' =>'',
			'menu' =>'stok',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li><a href="#">Stok</a></li>
				<li class="active">Tambah Stok</li>
			</ol>',
            'pelabuhan' => $this->Model->get('pelabuhan')->result(),
            'produk' => $this->Model->get('produk')->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idproduk" => $this->input->post('idproduk',true),
			'pumpable' => $this->input->post('pumpable',true),
			'dot' => $this->input->post('dot',true),
			'safestok' => $this->input->post('safestok',true),
			'deadstok' => $this->input->post('deadstok',true),
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'stok');
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'stok');
		}
		$data = array(
			'page' => 'stok/formedit',
			'link' =>'',
            'menu' =>'stok',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li><a href="#">Stok</a></li>
				<li class="active">Ubah Stok</li>
			</ol>',
            "idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idproduk" => $this->input->post('idproduk',true),
			'pelabuhan' => $this->Model->get('pelabuhan')->result(),
            'produk' => $this->Model->get('produk')->result(),
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function update() {
		$data = array(
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idproduk" => $this->input->post('idproduk',true),
			'pumpable' => $this->input->post('pumpable',true),
			'dot' => $this->input->post('dot',true),
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'stok');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'stok');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'stok');
	}

}