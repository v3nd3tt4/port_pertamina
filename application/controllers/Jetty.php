<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jetty extends CI_Controller {

	private $table = 'listjetty';
	private $table_detailjetty = 'detailjetty';
	private $table_pelabuhan = 'pelabuhan';
	private $table_produk = 'produk';

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
		$data = array(
			'page' => 'jetty/data',
			'link' =>'',
			'menu' => 'jetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li class="active">List Jetty</li>
			</ol>',
			'list' => $this->Model->get($this->table)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function detail() {
		$id = $this->uri->segment(3);
		$data = array(
			'page' => 'jetty/detail',
			'link' =>'',
			'menu' => 'jetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Jetty</a></li>
				<li class="active">Detail Jetty</li>
			</ol>',
			'list' => $this->Model->kueri(
				"
				SELECT $this->table.*,$this->table_detailjetty.*,$this->table_pelabuhan.nama as pelabuhan,$this->table_produk.nama as produk 
					FROM $this->table 
					JOIN $this->table_detailjetty ON $this->table_detailjetty.idjetty = $this->table.id
					JOIN $this->table_pelabuhan ON $this->table_pelabuhan.id = $this->table_detailjetty.idpelabuhan 
					JOIN $this->table_produk ON $this->table_produk.id = $this->table_detailjetty.idproduk
					WHERE idjetty='$id'
				"
			)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'jetty/formtambah',
			'link' =>'',
			'menu' => 'jetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Jetty</a></li>
				<li class="active">Tambah Jetty</li>
			</ol>',
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambahdetail() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'jetty');
		}
		$data = array(
			'page' => 'jetty/formtambahdetail',
			'link' =>'',
			'menu' => 'jetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Jetty</a></li>
				<li class="active">Tambah Detail Jetty</li>
			</ol>',
			'pelabuhan' => $this->Model->getdataall('pelabuhan')->result(),
			'produk' => $this->Model->getdataall('produk')->result(),
			'satuan' => $this->Model->getdataall('listsatuan')->result(),
			'id' => $id
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'jetty');
	 	}
		$data = array(
			'page' => 'jetty/formedit',
			'link' =>'',
			'menu' => 'jetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Jetty</a></li>
				<li class="active">Ubah Jetty</li>
			</ol>',
			'form' => $this->Model->getdata($this->table,array("id"=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formeditdetail() {
		$id = $this->uri->segment(3);
		$idjetty = $this->uri->segment(4);
		if(!is_numeric($id)) {
			redirect(base_url().'jetty');
	 	}
		$data = array(
			'page' => 'jetty/formeditdetail',
			'link' =>'',
			'menu' => 'jetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Jetty</a></li>
				<li class="active">Ubah Detail Jetty</li>
			</ol>',
			'pelabuhan' => $this->Model->getdataall('pelabuhan')->result(),
			'produk' => $this->Model->getdataall('produk')->result(),
			'id' => $id,
			'form' => $this->Model->getdata($this->table_detailjetty,array('id'=>$id))->row()

		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"nama" => $this->input->post('nama',true),
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'jetty');
	}

	public function storedetail() {
		$data = array(
			"idjetty" => $this->input->post('id'),
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idproduk" => $this->input->post('idproduk',true),
			"kap" => $this->input->post('kap',true),
			"occ" => $this->input->post('occ',true),
			"jalurpipa" => $this->input->post('jalurpipa',true),
			"diameterpipa" => $this->input->post('diameterpipa',true),
			"satuandiameter" => $this->input->post('satuandiameter',true),
			"utilisasi" => $this->input->post('utilisasi',true),
			"maxload" => $this->input->post('maxload',true),
			"kedalamanair" => $this->input->post('kedalamanair',true),
			"ket" => $this->input->post('ket',true),
		);
		$result = $this->Model->insertdata($this->table_detailjetty,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'jetty/detail/'.$this->input->post('id'));
	}

	public function update() {
		$data = array(
			"nama" => $this->input->post('nama',true),
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'jetty');
	}

	public function updatedetail() {
		$data = array(
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idproduk" => $this->input->post('idproduk',true),
			"kap" => $this->input->post('kap',true),
			"occ" => $this->input->post('occ',true),
			"jalurpipa" => $this->input->post('jalurpipa',true),
			"diameterpipa" => $this->input->post('diameterpipa',true),
			"satuandiameter" => $this->input->post('satuandiameter',true),
			"utilisasi" => $this->input->post('utilisasi',true),
			"maxload" => $this->input->post('maxload',true),
			"kedalamanair" => $this->input->post('kedalamanair',true),
			"ket" => $this->input->post('ket',true),
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table_detailjetty,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'jetty/detail/'.$this->input->post('idjetty'));
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'jetty');
		}
		$this->Model->removedata($this->table_detailjetty,array('idjetty'=>$id));
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'jetty');
	}

	public function destroydetail() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'jetty');
		}
		$result = $this->Model->removedata($this->table_detailjetty,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'jetty/detail/'.$this->uri->segment(4));
	}
}
