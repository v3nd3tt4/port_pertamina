<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sandarjetty extends CI_Controller {

    private $table = 'sandarjetty';
    private $table_jetty = 'listjetty';
    private $table_kapal = 'kapal';


    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
		$data = array(
			'page' => 'sandarjetty/data',
            'link' =>'',
            'menu' =>'sandarjetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li class="active">Sandar Jetty</li>
			</ol>',
			'list' => $this->Model->kueri(
                
                "
                SELECT $this->table.*,$this->table_jetty.nama AS jetty,$this->table_kapal.nama as kapal 
                FROM $this->table 
                    JOIN $this->table_jetty ON ($this->table_jetty.id=$this->table.idjetty)
                    JOIN $this->table_kapal ON ($this->table_kapal.id=$this->table.idkapal)
                "

            )->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$query = $this->Model->kueri("select detailjetty.*, pelabuhan.nama as nama_pelabuhan, produk.nama as nama_produk, listjetty.nama as nama_jetty from detailjetty join pelabuhan on detailjetty.idpelabuhan = pelabuhan.id  join produk on detailjetty.idproduk = produk.id join listjetty on listjetty.id = detailjetty.idjetty");
		$data = array(
			'page' => 'sandarjetty/formtambah',
			'link' =>'',
            'menu' =>'sandarjetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li><a href="#">Sandar Jetty</a></li>
				<li class="active">Tambah Sandar Jetty</li>
			</ol>',
            'jetty' => $this->Model->get($this->table_jetty)->result(),
            'detailjetty' => $query,
            'kapal' => $this->Model->get($this->table_kapal)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'sandarjetty');
		}
		$query = $this->Model->kueri("select detailjetty.*, pelabuhan.nama as nama_pelabuhan, produk.nama as nama_produk, listjetty.nama as nama_jetty from detailjetty join pelabuhan on detailjetty.idpelabuhan = pelabuhan.id  join produk on detailjetty.idproduk = produk.id join listjetty on listjetty.id = detailjetty.idjetty");
		$data = array(
			'page' => 'sandarjetty/formedit',
			'link' =>'',
            'menu' =>'sandarjetty',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planenr</a></li>
				<li><a href="#">Sandar Jetty</a></li>
				<li class="active">Ubah Sandar Jetty</li>
			</ol>',
            'jetty' => $this->Model->get($this->table_jetty)->result(),
            'kapal' => $this->Model->get($this->table_kapal)->result(),
            'detailjetty' => $query,
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"idjetty" => $this->input->post('idjetty',true),
			"idkapal" => $this->input->post('idkapal',true)
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'sandarjetty');
	}

	public function update() {
		$data = array(
			"idjetty" => $this->input->post('idjetty',true),
			"idkapal" => $this->input->post('idkapal',true)
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'sandarjetty');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'sandarjetty');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'sandarjetty');
	}
}
