<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        
    }
	public function index()
	{
		$data = array(
			'page' => 'login',			
            'script' => 'script/script_dashboard',
            'isi_chat' => $this->Model->kueri("select * from tb_chat order by id_chat ASC")

		);
		$this->load->view('login', $data);
	}

	public function proseslogin(){
		$username = $this->input->post('username', true);
		$password = $this->input->post('password', true);
		$cek = $this->Model->getdata('userlogin', array('namauser' => $username, 'password' => $password));
		if($cek->num_rows() != 0){
			$datasession = array(
				'namauser' => $username,
				'password' => $password,
				'akses' => $cek->row()->akses,
				'idpelabuhan' => $cek->row()->idpelabuhan
			);
			$this->session->set_userdata($datasession);
			echo '<script>alert("User ditemukan");window.location.href = "'.base_url().'welcome";</script>';

		}else{
			echo '<script>alert("User tidak ditemukan");window.location.href = "'.base_url().'login";</script>';
		}
	}

	public function proseslogout(){
		$this->session->sess_destroy();
		echo '<script>alert("berhasil logout");window.location.href = "'.base_url().'login";</script>';
	}
}