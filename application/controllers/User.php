<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }
	public function index()
	{
		$data = array(
			'page' => 'user/data',			
            'script' => 'script/script_dashboard',
            'isi_user' => $this->Model->kueri("select pelabuhan.nama, userlogin.* from userlogin join pelabuhan on pelabuhan.id = userlogin.idpelabuhan "),
            'menu' => 'user',
            'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">User</a></li>
			</ol>',

		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'user/formtambah',
			'link' =>'',
			'menu' =>'user',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">User</a></li>
				<li class="active">Tambah User</li>
			</ol>',
            'pelabuhan' => $this->Model->get('pelabuhan')->result(),
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"namauser" => $this->input->post('username',true),
			'password' => $this->input->post('password',true),
			'akses' => $this->input->post('akses',true),
		);
		$result = $this->Model->insertdata('userlogin',$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'user');
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'user');
		}
		$data = array(
			'page' => 'user/formedit',
			'link' =>'',
            'menu' =>'user',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">user</a></li>
				<li class="active">Ubah user</li>
			</ol>',
            "idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idproduk" => $this->input->post('idproduk',true),
			'pelabuhan' => $this->Model->get('pelabuhan')->result(),
            'produk' => $this->Model->get('produk')->result(),
			'form' => $this->Model->getdata('userlogin',array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function update() {
		$data = array(
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"namauser" => $this->input->post('username',true),
			'password' => $this->input->post('password',true),
			'akses' => $this->input->post('akses',true),
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata('userlogin',array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'user');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'user');
		}
		$result = $this->Model->removedata('userlogin',array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'user');
	}
}