<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waiting extends CI_Controller {

	private $table = 'listwaiting';

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
		$data = array(
			'page' => 'waiting/data',
            'link' =>'',
            'menu' =>'waiting',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li class="active">List Waiting</li>
			</ol>',
			'list' => $this->Model->get($this->table)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'waiting/formtambah',
			'link' =>'',
            'menu' =>'waiting',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Waiting</a></li>
				<li class="active">Tambah Waiting</li>
			</ol>',
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'waiting');
		}
		$data = array(
			'page' => 'waiting/formedit',
			'link' =>'',
            'menu' =>'waiting',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Waiting</a></li>
				<li class="active">Ubah Waiting</li>
			</ol>',
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"nama" => $this->input->post('nama',true)
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'waiting');
	}

	public function update() {
		$data = array(
			"nama" => $this->input->post('nama',true)
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'waiting');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'waiting');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'waiting');
	}
}
