<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_pertamina extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
    }

	public function index()
	{
		$data = array(
			'page' => 'template_pertamina/data',
            'link' =>'',
            'menu' =>'template_pertamina',
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}
}