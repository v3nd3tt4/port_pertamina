<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kapal extends CI_Controller {

    private $table = 'kapal';
    private $table_tipe = 'listtipekapal';

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
		$data = array(
			'page' => 'kapal/data',
            'link' =>'',
            'menu' =>'kapal',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li class="active">List Kapal</li>
			</ol>',
			'list' => $this->Model->kueri(
                
                "SELECT $this->table.*,$this->table_tipe.nama AS tipe FROM $this->table 
                    JOIN $this->table_tipe ON ($this->table_tipe.id=$this->table.idtipe)"

            )->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'kapal/formtambah',
			'link' =>'',
            'menu' =>'kapal',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Kapal</a></li>
				<li class="active">Tambah Kapal</li>
			</ol>',
            'tipe' => $this->Model->get($this->table_tipe)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'kapal');
		}
		$data = array(
			'page' => 'kapal/formedit',
			'link' =>'',
            'menu' =>'kapal',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Kapal</a></li>
				<li class="active">Ubah Kapal</li>
			</ol>',
            'tipe' => $this->Model->get($this->table_tipe)->result(),
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"nama" => $this->input->post('nama',true),
			"idtipe" => $this->input->post('idtipe',true),
			"kapasitas" => $this->input->post('kapasitas',true),
			"satuankapasitas" => $this->input->post('satuankapasitas',true),
			"flowrate" => $this->input->post('flowrate',true),
			"satuanflowrate" => $this->input->post('satuanflowrate',true),
			"jenisangkut" => $this->input->post('jenis',true)
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'kapal');
	}

	public function update() {
		$data = array(
			"nama" => $this->input->post('nama',true),
			"idtipe" => $this->input->post('idtipe',true),
			"kapasitas" => $this->input->post('kapasitas',true),
			"satuankapasitas" => $this->input->post('satuankapasitas',true),
			"flowrate" => $this->input->post('flowrate',true),
			"satuanflowrate" => $this->input->post('satuanflowrate',true),
			"jenisangkut" => $this->input->post('jenis',true)
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'kapal');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'kapal');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'kapal');
	}
}
