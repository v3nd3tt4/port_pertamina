<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estimasiwaktu extends CI_Controller {

    private $table = 'estimasiwaktu';
    private $table_kapal = 'kapal';
    private $table_pelabuhan = 'pelabuhan';
    private $table_keterangan = 'listketerangan';

    function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }

	public function index()
	{
		$data = array(
			'page' => 'estimasiwaktu/data',
            'link' =>'',
            'menu' =>'estimasiwaktu',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li class="active">Estimasi Waktu</li>
			</ol>',
			'list' => $this->Model->kueri(
                
                "
                SELECT $this->table.*,$this->table_kapal.nama AS kapal,
                        $this->table_pelabuhan.nama AS pelabuhan,
                        $this->table_keterangan.nama AS keterangan FROM $this->table 
                    JOIN $this->table_kapal ON ($this->table_kapal.id=$this->table.idkapal)
                    JOIN $this->table_pelabuhan ON ($this->table_pelabuhan.id=$this->table.idpelabuhan)
                    JOIN $this->table_keterangan ON ($this->table_keterangan.id=$this->table.idlistket)
                "

            )->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'estimasiwaktu/formtambah',
			'link' =>'',
            'menu' =>'estimasiwaktu',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li><a href="#">Estimasi Waktu</a></li>
				<li class="active">Tambah Estimasi Waktu</li>
			</ol>',
            'kapal' => $this->Model->get($this->table_kapal)->result(),
            'pelabuhan' => $this->Model->get($this->table_pelabuhan)->result(),
            'keterangan' => $this->Model->get($this->table_keterangan)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'estimasiwaktu');
		}
		$data = array(
			'page' => 'estimasiwaktu/formedit',
			'link' =>'',
            'menu' =>'estimasiwaktu',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Planner</a></li>
				<li><a href="#">Estimasi Waktu</a></li>
				<li class="active">Ubah Estimasi Waktu</li>
			</ol>',
            'kapal' => $this->Model->get($this->table_kapal)->result(),
            'pelabuhan' => $this->Model->get($this->table_pelabuhan)->result(),
            'keterangan' => $this->Model->get($this->table_keterangan)->result(),
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"idkapal" => $this->input->post('idkapal',true),
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idlistket" => $this->input->post('idlistket',true),
			"estimasiwaktu" => $this->input->post('estimasiwaktu',true),
			"status" => $this->input->post('status',true)
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'estimasiwaktu');
	}

	public function update() {
		$data = array(
			"idkapal" => $this->input->post('idkapal',true),
			"idpelabuhan" => $this->input->post('idpelabuhan',true),
			"idlistket" => $this->input->post('idlistket',true),
			"estimasiwaktu" => $this->input->post('estimasiwaktu',true),
			"status" => $this->input->post('status',true)
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'estimasiwaktu');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'estimasiwaktu');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'estimasiwaktu');
	}
}
