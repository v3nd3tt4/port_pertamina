<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {

	private $table = 'liststatus';
	function __construct() {
        parent::__construct();
        $this->load->model('Model');
        if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
    }
	public function index()
	{
		$data = array(
			'page' => 'status/data',
			'link' =>'',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li class="active">List Status</li>
			</ol>',
			'list' => $this->Model->get($this->table)->result()
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'status/formtambah',
			'link' =>'',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Satuan</a></li>
				<li class="active">Tambah Status</li>
			</ol>',
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'status');
		}
		$data = array(
			'page' => 'status/formedit',
			'link' =>'',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Satuan</a></li>
				<li class="active">Ubah Status</li>
			</ol>',
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data = array(
			"nama" => $this->input->post('nama',true),
		);
		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'status');
	}

	public function update() {
		$data = array(
			"nama" => $this->input->post('nama',true),
			
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'status');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'status');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'status');
	}
}
