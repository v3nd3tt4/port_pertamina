<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rute extends CI_Controller {

	private $table = 'rute';
	private $table_kapal = 'kapal';
	private $table_pelabuhan = 'pelabuhan';

	public function __construct(){
		parent::__construct();
		$this->load->model('Model');
		if($this->session->userdata('namauser') == '' || empty($this->session->userdata('namauser'))){
            echo 'Silahkan login terlebih dahulu.....';
            echo'<script>window.location.href="'.base_url().'login";</script>';
        }
	}


	public function index()
	{
		$query="SELECT r.id,r.idkapal,r.idasal,r.idtujuan,r.seatime,k.nama as namakapal,pl.nama as namaasal,ph.nama as namatujuan FROM rute r JOIN kapal k on r.idkapal=k.id JOIN pelabuhan pl on r.idasal=pl.id JOIN pelabuhan ph on r.idtujuan=ph.id";
		$data = array(
			'page' => 'rutekapal/data',
			'link' =>'',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li class="active">List Rute Kapal</li>
			</ol>',
			'list'=>$this->Model->kueri($query)->result(),
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formtambah(){
		$data = array(
			'page' => 'rutekapal/formtambah',
			'link' =>'',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Rute Kapal</a></li>
				<li class="active">Tambah Rute Kapal</li>
			</ol>',
			'kapal'=>$this->Model->getdataall('kapal')->result(),
			'pelabuhan'=>$this->Model->getdataall('pelabuhan')->result(),
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function formedit() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'rute');
		}
		$data = array(
			'page' => 'rutekapal/formedit',
			'link' =>'',
			'breadcumb' => '<ol class="breadcrumb">
				<li><a href="#">Settings</a></li>
				<li><a href="#">List Rute Kapal</a></li>
				<li class="active">Ubah Rute Kapal</li>
			</ol>',
			'kapal'=>$this->Model->getdataall('kapal')->result(),
			'pelabuhan'=>$this->Model->getdataall('pelabuhan')->result(),
			'form' => $this->Model->getdata($this->table,array('id'=>$id))->row() 
		);
		$this->load->view('template_pertamina/wrapper', $data);
	}

	public function store() {
		$data=array(
			'idkapal'=>$this->input->post('idkapal'),
			'idasal'=>$this->input->post('idasal'),
			'idtujuan'=>$this->input->post('idtujuan'),
			'seatime'=>$this->input->post('seatime'),
		);

		$result = $this->Model->insertdata($this->table,$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'rute');
	}

	public function update() {
		$data=array(
			'idkapal'=>$this->input->post('idkapal'),
			'idasal'=>$this->input->post('idasal'),
			'idtujuan'=>$this->input->post('idtujuan'),
			'seatime'=>$this->input->post('seatime'),
		);
		$id = $this->input->post('id',true);
		$result = $this->Model->updatedata($this->table,array('id'=>$id),$data);
		if($result) {
			
		}else {

		}
		redirect(base_url().'rute');
	}

	public function destroy() {
		$id = $this->uri->segment(3);
		if(!is_numeric($id)) {
			redirect(base_url().'rute');
		}
		$result = $this->Model->removedata($this->table,array('id'=>$id));
		if($result) {
			
		}else {

		}
		redirect(base_url().'rute');
	}
}
