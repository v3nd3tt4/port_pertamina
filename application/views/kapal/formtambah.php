<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah Kapal <i class="fa fa-pencil pull-right"></i></h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/store" method="post">
							<div class="form-group">
								<label for="nama">Nama Kapal</label>
								<input name="nama" type="text" class="form-control" id="nama" aria-describedby="namaHelp" placeholder="Masukkan Kapal" required>
							</div>
							<div class="form-group">
								<label for="idtipe">Tipe Kapal</label>
								<select name="idtipe" id="idtipe" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Tipe</option>
									<?php if(!empty($tipe)){foreach($tipe as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="kapasitas">Kapasitas</label>
								<input name="kapasitas" type="text" class="form-control" id="kapasitas" aria-describedby="namaHelp" placeholder="Masukkan Kapasitas" required>
							</div>
							<div class="form-group">
								<label for="satuankapasitas">Satuan Kapasitas</label>
								<input name="satuankapasitas" type="text" class="form-control" id="satuankapasitas" aria-describedby="namaHelp" placeholder="Masukkan Satuan Kapasitas" required>
							</div>
							<div class="form-group">
								<label for="flowrate">Flow Rate</label>
								<input name="flowrate" type="text" class="form-control" id="flowrate" aria-describedby="namaHelp" placeholder="Masukkan Flow Rate" required>
							</div>
							<div class="form-group">
								<label for="satuanflowrate">Satuan Flow Rate</label>
								<input name="satuanflowrate" type="text" class="form-control" id="satuanflowrate" aria-describedby="namaHelp" placeholder="Masukkan Satuan Flow Rate" required>
							</div>
							<div class="form-group">
								<label for="satuanflowrate">Jenis</label>
								<select name="jenis" id="jenis" type="text" class="form-control"  aria-describedby="namaHelp" required>
									<option value="">--pilih--</option>
									<option value="0">Minyak</option>
									<option value="1">LPG</option>
									<option value="2">Semua</option>
								</select>
							</div>
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>