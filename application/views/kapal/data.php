<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="container-fluid">
			<h4 class="c-grey-900 mT-10 mB-30"></h4>	
			<div class="row">
				<div class="col-md-12">
					<div class="bgc-white bd bdrs-3 p-20 mB-20">
						<a href="<?=base_url().$menu?>/formtambah" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>	Tambah Data</a>
						<h4 class="c-grey-900 mB-20">Data Kapal</h4>
						<br>
						<div id="dataTable_wrapper" class="dataTables_wrapper">
							<table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%" id="dataTable">
								<thead>
									<tr role="row" class="header-table">
										<th>No</th>
										<th>Nama</th>
										<th>Tipe</th>
										<th>Kapasitas</th>
										<th>Satuan Kapasitas</th>
										<th>Flow Rate</th>
										<th>Satuan Flow Rate</th>
										<th>Jenis</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; if(!empty($list)){foreach($list as $row){ ?>
									<tr>
										<td><?=$i++?></td>
										<td><?=$row->nama?></td>
										<td><?=$row->tipe?></td>
										<td><?=$row->kapasitas?></td>
										<td><?=$row->satuankapasitas?></td>
										<td><?=$row->flowrate?></td>
										<td><?=$row->satuanflowrate?></td>
										<td>
											<?php 
											if($row->jenisangkut == '0'){
												echo 'Minyak';
											}else if($row->jenisangkut == '1'){
												echo 'LPG';
											}else{
												echo 'Semua';
											}
											?>
												
										</td>
										<td>
											<a class="btn btn-warning" href="<?=base_url().$menu?>/formedit/<?=$row->id?>"><i class="fa fa-edit"></i> </a>
											<a class="btn btn-danger" href="<?=base_url().$menu?>/destroy/<?=$row->id?>" onclick="return confirm('Hapus Data ini?')"><i class="fa fa-remove"></i></a>
										</td>
									</tr>
									<?php }}?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>