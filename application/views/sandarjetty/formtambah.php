<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah Sandar Jetty <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/store" method="post">
							<div class="form-group">
								<label for="idjetty">Jetty</label>
								<select name="idjetty" id="idjetty" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Jetty</option>
									<?php if(!empty($detailjetty)){foreach($detailjetty->result() as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama_jetty?> - <?=$row->nama_pelabuhan?> - <?=$row->nama_produk?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idkapal">Kapal</label>
								<select name="idkapal" id="idkapal" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Kapal</option>
									<?php if(!empty($kapal)){foreach($kapal as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>