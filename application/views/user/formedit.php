<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah User <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/update" method="post">
							<div class="form-group">
								<label for="idpelabuhan">Pelabuhan</label>
								<input type="hidden" name="id" id="id" value="<?=$form->id?>">
								<select name="idpelabuhan" id="idpelabuhan" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih pelabuhan</option>
									<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
									<option value="<?=$row->id?>" <?=$row->id == $form->idpelabuhan ? 'selected' : '' ?> ><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="username" id="username" class="form-control" value="<?=$form->namauser?>">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" id="password" class="form-control" value="<?=$form->password?>">
							</div>
							<div class="form-group">
								<label>Level</label>
								<select  name="akses" id="akses" class="form-control">
									<option value="">--pilih--</option>
									<option value="atasan" <?=$form->akses == 'atasan' ? 'selected' : ''?>>atasan</option>
									<option value="admin" <?=$form->akses == 'admin' ? 'selected' : ''?>>admin</option>
									<!-- <option value="planner" <?=$form->akses == 'planner' ? 'selected' : ''?>>planner</option> -->
									<option value="planner lpg" <?=$form->akses == 'planner lpg' ? 'selected' : ''?>>planner lpg</option>
									<option value="planner minyak" <?=$form->akses == 'planner minyak' ? 'selected' : ''?>>planner minyak</option>
									<option value="admin kapal">admin kapal</option>
								</select>
							</div>
							
							<button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>