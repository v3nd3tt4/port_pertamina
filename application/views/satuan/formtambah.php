<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah Satuan  <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url()?>satuan/store" method="post">
							<div class="form-group">
								<label for="nama">Nama Satuan</label>
								<input type="text" class="form-control" name="nama" aria-describedby="namaHelp" placeholder="Masukkan Nama Satuan">
							</div>
							<div class="form-group">
								<label for="simbol">Simbol</label>
								<input type="text" class="form-control" name="simbol" aria-describedby="simbolHelp" placeholder="Masukkan Simbol">
							</div>
							<button type="submit" class="btn btn-info"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>