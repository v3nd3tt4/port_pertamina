<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="container-fluid">
			<h4 class="c-grey-900 mT-10 mB-30"></h4>	
			<div class="row">
				<div class="col-md-12">
					<div class="bgc-white bd bdrs-3 p-20 mB-20">
						<a href="<?=base_url().$menu?>/formtambahdetail/<?=$this->uri->segment(3)?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i>	Tambah Data</a>
						<h4 class="c-grey-900 mB-20">Detail Jetty </h4>
						<br>
						<div id="dataTable_wrapper" class="dataTables_wrapper table-responsive">
							<table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%" id="dataTable">
								<thead>
									<tr role="row">
										<th>No</th>
										<th>Pelabuhan</th>
										<th>Produk</th>
										<th>KAP</th>
										<th>OCC</th>
										<th>Jalur Pipa</th>
										<th>Diamater Pipa</th>
										<th>Satuan Diameter</th>
										<th>Utilisasi</th>
										<th>Maxload</th>
										<th>Kedalaman Air</th>
										<th>Ket</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>

									<?php
                                        $no = 1;
                                        if(!empty($list)) {foreach($list as $row){
					    			?>
					    			<tr>
	                                    <td><?=$no++?></td>
						    			<td><?=$row->pelabuhan?></td>
						    			<td><?=$row->produk?></td>
						    			<td><?=$row->kap?></td>
						    			<td><?=$row->occ?></td>
						    			<td><?=$row->jalurpipa?></td>
						    			<td><?=$row->diameterpipa?></td>
						    			<td><?=$row->satuandiameter?></td>
						    			<td><?=$row->utilisasi?></td>
						    			<td><?=$row->maxload?></td>
						    			<td><?=$row->kedalamanair?></td>
						    			<td><?=$row->ket?></td>
						    			<td>
	                                        <a class="btn btn-warning" href="<?=base_url().$menu?>/formeditdetail/<?=$row->id?>/<?=$row->idjetty?>"><i class="fa fa-edit"></i> Ubah</a>
											<a class="btn btn-danger" href="<?=base_url().$menu?>/destroydetail/<?=$row->id?>/<?=$row->idjetty?>" onclick="return confirm('Hapus Data ini?')"><i class="fa fa-remove"></i> Hapus</a>
						    			</td>
					    			</tr>
					    			<?php }}?>
									
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>