<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Ubah Detail Jetty <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url()?>jetty/updatedetail" method="post">
							<div class="form-group">
								<label for="exampleInputEmail1">Pelabuhan</label>
                                <input type="hidden" name="id" value="<?=$id?>">
                                <input type="hidden" name="idjetty" value="<?=$form->idjetty?>">
								<select class="form-control" name="idpelabuhan" required>
									<option value="">--Pilih Pelabuhan--</option>
									<?php
									foreach($pelabuhan as $p){
									?>
									<option <?=($p->id==$form->idpelabuhan)?'selected':''?> value="<?=$p->id?>"><?=$p->nama?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Produk</label>
								<select class="form-control" name="idproduk" required>
									<option value="">--Pilih Produk--</option>
									<?php
									foreach($produk as $k){
									?>
									<option  <?=($k->id==$form->idproduk)?'selected':''?> value="<?=$k->id?>"><?=$k->nama?></option>
									<?php
									}
									?>
								</select>
							</div>
							
							<div class="form-group">
								<label for="exampleInputEmail1">Kap</label>
								<input type="number" class="form-control" id="kap" name="kap" placeholder="" value="<?=$form->kap?>" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">OCC</label>
								<input type="number" class="form-control" id="occ" name="occ" placeholder="" value="<?=$form->occ?>" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Jalur Pipa</label>
								<input type="number" class="form-control" name="jalurpipa" placeholder="" value="<?=$form->jalurpipa?>" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Diameter Pipa</label>
								<input type="number" class="form-control" name="diameterpipa" placeholder="" value="<?=$form->diameterpipa?>" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Satuan Diameter</label>
								<input type="number" class="form-control" name="satuandiameter" placeholder="" value="<?=$form->satuandiameter?>" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Utilisasi</label>
								<textarea name="utilisasi" class="form-control" required><?=$form->utilisasi?></textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Kedalaman Air</label>
								<input type="number" class="form-control" name="kedalamanair" placeholder="" value="<?=$form->kedalamanair?>" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Ket</label>
								<textarea name="ket" class="form-control" required><?=$form->ket?></textarea>
							</div>
							<button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>