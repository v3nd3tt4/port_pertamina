<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah Jetty <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url()?>jetty/storedetail" method="post">
							<div class="form-group">
								<label for="exampleInputEmail1">Pelabuhan</label>
                                <input type="hidden" name="id" value="<?=$id?>">
								<select class="form-control" name="idpelabuhan" required>
									<option value="">--Pilih Pelabuhan--</option>
									<?php
									foreach($pelabuhan as $p){
									?>
									<option value="<?=$p->id?>"><?=$p->nama?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Produk</label>
								<select class="form-control" name="idproduk" required>
									<option value="">--Pilih Produk--</option>
									<?php
									foreach($produk as $k){
									?>
									<option value="<?=$k->id?>"><?=$k->nama?></option>
									<?php
									}
									?>
								</select>
							</div>
							
							<div class="form-group">
								<label for="exampleInputEmail1">Kap</label>
								<input type="number" class="form-control" id="kap" name="kap" placeholder="" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">OCC</label>
								<input type="number" class="form-control" id="occ" name="occ" placeholder="" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Jalur Pipa</label>
								<input type="number" class="form-control" name="jalurpipa" placeholder="" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Diameter Pipa</label>
								<input type="number" class="form-control" name="diameterpipa" placeholder="" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Satuan Diameter</label>
								<select class="form-control" name="satuandiameter" required>
									<option value="">--Pilih Produk--</option>
									<?php
									foreach($satuan as $k){
									?>
									<option value="<?=$k->id?>"><?=$k->nama?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Utilisasi</label>
								<textarea name="utilisasi" class="form-control" required></textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Kedalaman Air</label>
								<input type="number" class="form-control" name="kedalamanair" placeholder="" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Ket</label>
								<textarea name="ket" class="form-control" required></textarea>
							</div>
							<button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>