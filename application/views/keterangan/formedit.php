<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Edit Keterangan <i class="fa fa-pencil pull-right"></i></h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/update" method="post">
							<div class="form-group">
								<label for="nama">Nama Keterangan</label>
                                <input type="hidden" name="id" value="<?=@$form->id?>" required>
								<input type="text" class="form-control" value="<?=@$form->nama?>" name="nama" aria-describedby="namaHelp" placeholder="Masukkan Keterangan">
							</div>
							<div class="form-group">
								<label for="status">Status</label>
								<select name="status" id="status" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Status</option>
									<option <?=($form->status=='1')?'selected':''?> value="1">1</option>
									<option <?=($form->status=='0')?'selected':''?> value="0">0</option>
								</select>
							</div>
							<button type="submit" class="btn btn-info"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>