<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah Pelabuhan <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/store" method="post">
							<div class="form-group">
								<label for="kode">Kode</label>
								<input type="text" class="form-control" name="kode" aria-describedby="simbolHelp" placeholder="Masukkan Kode Pelabuhan">
							</div>
							<div class="form-group">
								<label for="nama">Nama Pelabuhan</label>
								<input type="text" class="form-control" name="nama" aria-describedby="namaHelp" placeholder="Masukkan Nama Pelabuhan">
							</div>
							<div class="form-group">
								<label for="nama">Baseline Pelabuhan</label>
								<input type="text" class="form-control" name="baseline" aria-describedby="namaHelp" placeholder="Masukkan Baseline Pelabuhan">
							</div>
							<button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>