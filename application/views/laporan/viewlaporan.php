<script type="text/javascript" src="<?=base_url()?>assets_chart/canvasjs/canvasjs.min.js"></script>

<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	exportEnabled: true,
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	title: {
		text: "Pengeluaran Kas Kecil Masing-Masing Pos Pengeluaran"
	},
	axisY: {
		title: "Base Line",
		// suffix: "%",
		// includeZero: false
	},
	axisX: {
		title: "Keterangan"
	},
	data: [{
		type: "column",
		// yValueFormatString: "#,##0.0#\"%\"",
		dataPoints: [
			<?php foreach($row->result() as $data){?>
			{ label: "<?=$data->namajenis?>", y: <?=$data->total?> },
			<?php }?>
				
		]
	}]
});
chart.render();

}
</script>
<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-12">
			</div>
			<div class="masonry-item col-md-12" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
						
				</div>
			</div>
		</div>
	</div>
</main>