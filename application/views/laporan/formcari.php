<script type="text/javascript" src="<?=base_url()?>assets/jquery.datetimepicker.full.min.js"></script>
<link href="<?=base_url()?>assets/jquery.datetimepicker.min.css" rel="stylesheet">
<script type="text/javascript">
	$(document).ready(function(){
		$('#daritanggal').datetimepicker({
                format:'Y-m-d H:i:s',
        });
        $('#hinggatanggal').datetimepicker({
                format:'Y-m-d H:i:s',
        });
	});
</script>
<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Filter Pencarian<i class="fa fa-neuter pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/viewlaporan" method="post">
							<div class="form-group">
								<label for="nama">Pelabuhan</label>
								<select name="idpelabuhan" class="form-control">
									<option value="">--Pilih Pelabuhan--</option>
									<?php
									foreach($pelabuhan as $p){
									?>
									<option value="<?=$p->id?>"><?=$p->nama?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="nama">Dari Tanggal</label>
								<input type="text" class="form-control" name="daritanggal" id="daritanggal" aria-describedby="namaHelp" >
							</div>
							<div class="form-group">
								<label for="nama">Hingga Tanggal</label>
								<input type="text" class="form-control" name="hinggatanggal" id="hinggatanggal" aria-describedby="namaHelp" >
							</div>
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>