
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>    
            <div class="row" style="border: thin solid #fff">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                       <!--  <a href="<?=base_url().$menu?>/formtambah" class="btn btn-info pull-right"><i class="fa fa-plus"></i>   Tambah Data</a> -->
                        <!-- <h4 class="c-grey-900 mB-20">Estimasi Waktu</h4>
                        <br> -->
                        <h3 style="margin-top: 0px">Port Schedule</h3>
                        <div id="dataTable_wrapper" class="dataTables_wrapper">
                            <?php $no=1; foreach($data->result() as $row){

                                // $datetime1 = new DateTime($row->arrival);
                                // $datetime2 = new DateTime($row->departure);
                                // $interval = $datetime1->diff($datetime2);
                                // $date = $interval->format('%H:%i:%s');
                                // // $elapsed = $interval->format('%H:%i HRS');
                                // $elapsed = date('H:i', strtotime($date)).' HRS';

                                $date1 = new DateTime($row->arrival);
                                $date2 = new DateTime($row->departure);

                                $diff = $date2->diff($date1);

                                $hours = $diff->h;
                                $minutes = $diff->i;
                                $hours = $hours + ($diff->days*24);

                                $elapsed = $hours.':'.$minutes.' HRS';

                                $detailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $row->id));

                                ?>
                                <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?> ( 
                                <?php $i=0;foreach($detailshipment->result() as $rowdetailshipment){ 
                                
                                    // if($i==0){
                                    //     echo $this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama.' <button class="btn btn-warning btn-xs btn_cek_proyeksi" value="'.$rowdetailshipment->id.'">Proyeksi Stok</button>'; 
                                    // }else{
                                    //     echo ', '.$this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama.' <button class="btn btn-warning btn-xs btn_cek_proyeksi" value="'.$rowdetailshipment->id.'">Proyeksi Stok</button>'; 
                                    // }

                                    if($i==0){
                                        echo $this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
                                    }else{
                                        echo ', '.$this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
                                    }
                                    $i++;
                                }?>

                                )<br/>
                                antrian : <?=$row->antrian?>, Jetty: <?=$row->idjetty?>, Pelabuhan : <?=$this->Model->getdata('pelabuhan', array('id' => $row->idpelabuhanbantuan))->row()->nama?> 
                                <br>
                                <?php
                                if($this->session->userdata('akses')=='admin kapal'){
                                ?>
                                 <a href="#" class="btn btn-success btn-sm proses_jetty_1" id="<?=$row->id?>" onclick="return confirm('Apakah anda yakin?')">Port Activity
                                </a> 
                                <br>&nbsp
                                <?php
                                }
                                ?>
                               

                                
                                <!-- <?php
                                    if($row->prosesbantuan=='0' and $row->idasal == $this->session->userdata('idpelabuhan')){
                                        $keterangan = '';
                                ?>
                                <br/><br/>
                                <a href="<?=base_url()?>welcome/proses_jetty_1/<?=$row->id?>" class="btn btn-danger btn-sm proses_jetty_1" id="<?=$row->id?>" onclick="return confirm('Apakah anda yakin?')">proses ke jetty 1</a> 
                                <a href="<?=base_url()?>welcome/proses_jetty_2/<?=$row->id?>" class="btn btn-primary btn-sm proses_jetty_2" id="<?=$row->arrival?>" onclick="return confirm('Apakah anda yakin?')">proses ke jetty 2</a>
                                <?php
                                    }elseif($row->prosesbantuan=='1' and $row->idtujuan == $this->session->userdata('idpelabuhan')){
                                ?>
                                <a href="<?=base_url()?>welcome/proses_jetty_1/<?=$row->id?>" class="btn btn-danger btn-sm proses_jetty_1" id="<?=$row->id?>" onclick="return confirm('Apakah anda yakin?')">proses ke jetty 1</a> 
                                <a href="<?=base_url()?>welcome/proses_jetty_2/<?=$row->id?>" class="btn btn-primary btn-sm proses_jetty_2" id="<?=$row->arrival?>" onclick="return confirm('Apakah anda yakin?')">proses ke jetty 2</a>
                                <?php
                                    }elseif($row->prosesbantuan === NULL){


                                ?>
                                <a href="<?=base_url()?>welcome/proses_jetty_1/<?=$row->id?>" class="btn btn-danger btn-sm proses_jetty_1" id="<?=$row->id?>" onclick="return confirm('Apakah anda yakin?')">proses ke jetty 1</a> 
                                <a href="<?=base_url()?>welcome/proses_jetty_2/<?=$row->id?>" class="btn btn-primary btn-sm proses_jetty_2" id="<?=$row->arrival?>" onclick="return confirm('Apakah anda yakin?')">proses ke jetty 2</a>
                                <?php
                                    }else{

                                        echo '<button class="btn btn-warning btn-sm">Tidak bisa di edit karena masih ada proses yang terjadi</button>';
                                    }
                                ?> -->
                                
                                
                            <div class="table-responsive">    
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%">
                                
                                <thead>
                                    <tr role="row">                                     
                                        <th>arrival</th>
                                        <th>berthed</th>
                                        <th>comm</th>
                                        <th>comp</th>
                                        <th>unberthed</th>
                                        <th>departure</th>                              
                                        <th>Tujuan</th>
                                        <th>IPT</th>
                                        <!-- <th>Status</th>
                                        <th>Aksi</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=$row->arrival?></td>
                                        <td><?=$row->berthed?></td>
                                        <td><?=$row->comm?></td>
                                        <td><?=$row->comp?></td>
                                        <td><?=$row->unberthed?></td>
                                        <td><?=$row->departure?></td>               
                                        <td><?=$this->Model->getdata('pelabuhan', array('id' => $row->idtujuan))->row()->nama?></td>
                                        <td style="color: green"><?=$elapsed?></td>
                                        <!-- <td><?=$row->status?></td>
                                        <td>
                                            <?php if($row->status === NULL || $row->status == 'simulasi'){
                                                echo '<a href="'.base_url().'simulasiplanner/prosessimulasinya/'.$row->id.'" class="btn btn-primary btn-sm">Proses</a>';
                                            }else if($row->status == 'proses'){
                                                echo '<a href="'.base_url().'simulasiplanner/donesimulasi/'.$row->id.'" class="btn btn-warning btn-sm">Done</a>';
                                            }else{
                                                echo '';
                                            }?>
                                            <button class="btn btn-danger btn-sm">Hapus</button>
                                        </td> -->
                                    </tr>                                   
                                </tbody>
                            </table>
                            </div>
                            <?php 
                            $idkapal = $row->idkapal;
                            $idtujuan = $row->idtujuan;
                            $query = $this->Model->kueri("SELECT * FROM sandarjetty JOIN detailjetty ON detailjetty.`id` = sandarjetty.`idjetty` JOIN listjetty ON listjetty.`id` = detailjetty.`idjetty` WHERE sandarjetty.`idkapal` = '$idkapal' AND detailjetty.`idpelabuhan` = '$idtujuan'");?>
                            <?php 
                            // if($query->num_rows() == 0){
                            //     echo 'rekomendasi jetty tidak ditemukan';
                            // }else{
                            //     echo "rekomendasi jetty: ". $query->row()->nama;
                            // }
                            ?>
                            Ketahanan Stok hingga <?=date_format(date_create($row->arrival),'d-m-Y')?> brp?
                            <br>
                            Ketahanan Stok pada <?=date_format(date_create($row->comp),'d-m-Y')?> brp?
                            <br>
                            Ullage hingga <?=date_format(date_create($row->arrival),'d-m-Y')?> brp?
                            <br>
                            Ullage pada <?=date_format(date_create($row->comp),'d-m-Y')?> brp?
                            <hr/>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>

            <?php if($this->session->userdata('akses')=='admin kapal'){
            ?>

            <!-- from japalid -->
            <div class="row" style="border: thin solid #fff">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                       
                        <?php 
                    $idpelabuhan = $this->session->userdata('idpelabuhan');
                    if(!empty($datajetty)) {
                        echo '<h3 style="margin-top: 0px">Port Activity</h3>'; 
                        
                        foreach($datajetty as $jetty){ 

                            $idjetty = $jetty->id;
		                    $kuerishipment = $this->Model->kueri("SELECT * FROM shipment WHERE `status` = 'proses' AND idpelabuhanbantuan = '$idpelabuhan' and idjetty = '$idjetty' group by shipment.id ORDER BY berthed ASC");
                            
                            ?>
                        <?php if($kuerishipment->num_rows() > 0) echo '<h4>'.$jetty->nama.'</h4>'; ?>

                        <div id="dataTable_wrapper" class="dataTables_wrapper">
                            
                            <?php
                            $no=1; foreach($kuerishipment->result() as $row){

                                // $datetime1 = new DateTime($row->arrival);
                                // $datetime2 = new DateTime($row->departure);
                                // $interval = $datetime1->diff($datetime2);
                                // $date = $interval->format('%H:%i:%s');
                                // // $elapsed = $interval->format('%H:%i HRS');
                                // $elapsed = date('H:i', strtotime($date)).' HRS';

                                $date1 = new DateTime($row->arrival);
                                $date2 = new DateTime($row->departure);

                                $diff = $date2->diff($date1);

                                $hours = $diff->h;
                                $minutes = $diff->i;
                                $hours = $hours + ($diff->days*24);

                                $elapsed = $hours.':'.$minutes.' HRS';

                                $detailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $row->id));


                                ?>
                                <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?> ( 
                                <?php $i = 0;foreach($detailshipment->result() as $rowdetailshipment){ 
                                    if($i == 0){
                                        echo $this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
                                    }else{
                                        echo ', '.$this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
                                    }
                                    
                                }?>

                                ) <?=$row->proses=='0' ? 'Loading ' : 'Discharge '?>
                                <br><a href="<?=base_url()?>welcome/batal_ke_jetty/<?=$row->id?>" class="btn btn-success btn-sm" onclick="return confirm('Apakah anda yakin?')">Kembalikan ke port schedule</a> 
                                <?php if($row->status != "done") { ?>
                                <a id="<?=$row->id?>" href="<?=base_url()?>welcome/selesai_shipment/<?=$row->id?>" class="btn btn-info btn-sm" ><i class="fa fa-check" onclick="return confirm('Apakah anda yakin?')" aria-hidden="true"></i> Selesai</a>
                                <!-- <a id="<?=$row->id?>" href="#" class="btn btn-info btn-sm selesai_shipment"><i class="fa fa-check" aria-hidden="true"></i> Selesai</a> -->
                                <?php }?>
                                <br> &nbsp;
                                <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%">
                                
                                <thead>
                                    <tr role="row">                                     
                                        <th>arrival<br/> <button class="btn btn-info btn-xs update_waktujetty<?=$row->id?>1" id="<?=$row->id?>">Update waktu</button></th>
                                        <th>berthed <br/> <button class="btn btn-info btn-xs update_waktujetty<?=$row->id?>2" id="<?=$row->id?>">Update waktu</button></th>
                                        <th>comm <br/> <button class="btn btn-info btn-xs update_waktujetty<?=$row->id?>3" id="<?=$row->id?>">Update waktu</button></th>
                                        <th>comp <br/> <button class="btn btn-info btn-xs update_waktujetty<?=$row->id?>4" id="<?=$row->id?>">Update waktu</button></th>
                                        <th>unberthed <br/> <button class="btn btn-info btn-xs update_waktujetty<?=$row->id?>5" id="<?=$row->id?>">Update waktu</button></th>
                                        <th>departure<br/> <button class="btn btn-info btn-xs update_waktujetty<?=$row->id?>6" id="<?=$row->id?>">Update waktu</button></th>                              
                                        <th>Tujuan</th>
                                        <th>IPT</th>
                                        <!-- <th>Status</th>
                                        <th>Aksi</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="text" name="arrival" id="arrival<?=$row->id?>" value="<?=$row->arrival?>" class="form-control picker">
                                            <input type="hidden" name="idkapal1" id="idkapal<?=$row->id?>" value="<?=$row->idkapal?>" class="form-control picker">
                                            
                                        </td>
                                        <td>
                                            <input type="text" name="berthed" id="berthed<?=$row->id?>"value="<?=$row->berthed?>" class="form-control picker">
                                        </td>
                                        <td>
                                            <input type="text" name="comm" id="comm<?=$row->id?>" value="<?=$row->comm?>" class="form-control picker">
                                        </td>
                                        <td>
                                            <input type="text" name="comp" id="comp<?=$row->id?>" value="<?=$row->comp?>" class="form-control picker">
                                        </td>
                                        <td>
                                            <input type="text" name="unberthed" id="unberthed<?=$row->id?>" value="<?=$row->unberthed?>" class="form-control picker"> 
                                        </td>
                                        <td>
                                            <input type="text" name="departure" id="departure<?=$row->id?>"  value="<?=$row->departure?>" class="form-control picker">
                                        </td>               
                                        <td><?=$this->Model->getdata('pelabuhan', array('id' => $row->idtujuan))->row()->nama?></td>
                                        <td style="color: green"><?=$elapsed?></td>
                                        <!-- <td><?=$row->status?></td>
                                        <td>
                                            <?php if($row->status === NULL || $row->status == 'simulasi'){
                                                echo '<a href="'.base_url().'simulasiplanner/prosessimulasinya/'.$row->id.'" class="btn btn-primary btn-sm">Proses</a>';
                                            }else if($row->status == 'proses'){
                                                echo '<a href="'.base_url().'simulasiplanner/donesimulasi/'.$row->id.'" class="btn btn-warning btn-sm">Done</a>';
                                            }else{
                                                echo '';
                                            }?>
                                            <button class="btn btn-danger btn-sm">Hapus</button>
                                        </td> -->
                                    </tr>                                   
                                </tbody>
                            </table>
                            </div>
                            <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>
                                        arrival - berthed<br/>
                                        <input type="hidden" name="idjetty21" id="idjetty2<?=$row->id?>" value="<?=$row->id?>">
                                        <select class="form-control" name="j2wait11" id="j2wait1<?=$row->id?>">
                                            <option value="">--pilih--</option>
                                            <?php $wait_arrival_berthed = ''; foreach($waiting->result() as $wait1){
                                                if($row->waiting1 == $wait1->id){
                                                    $selected = 'selected';
                                                    $wait_arrival_berthed = $wait1->nama;
                                                }else{
                                                    $selected = '';
                                                }
                                            
                                            echo '<option value="'.$wait1->id.'" '.$selected.' >'.$wait1->nama.'</option>';
                                            }?>
                                        </select><br/>
                                        <button class="btn btn-warning btn-xs btn_simpan_wait1<?=$row->id?>">Simpan</button>
                                    </td>
                                    <td>
                                        berthed - comm<br/>
                                         <select class="form-control" name="j2wait21" id="j2wait2<?=$row->id?>">
                                            <option value="">--pilih--</option>
                                            <?php $wait_berthed_comm = ''; foreach($waiting->result() as $wait1){
                                                if($row->waiting2 == $wait1->id){
                                                    $selected = 'selected';
                                                    $wait_berthed_comm = $wait1->nama;
                                                }else{
                                                    $selected = '';
                                                }
                                            echo '<option value="'.$wait1->id.'" '.$selected.'>'.$wait1->nama.'</option>';
                                            }?>
                                        </select><br/>
                                        <button class="btn btn-warning btn-xs btn_simpan_wait2<?=$row->id?>">Simpan</button>
                                    </td>
                                    <td>
                                        comm - comp<br/>
                                         <select class="form-control" name="j2wait31" id="j2wait3<?=$row->id?>">
                                            <option value="">--pilih--</option>
                                            <?php $wait_comm_comp = ''; foreach($waiting->result() as $wait1){
                                                if($row->waiting3 == $wait1->id){
                                                    $selected = 'selected';
                                                    $wait_comm_comp = $wait1->nama;
                                                }else{
                                                    $selected = '';
                                                }
                                            echo '<option value="'.$wait1->id.'" '.$selected.'>'.$wait1->nama.'</option>';
                                            }?>
                                        </select><br/>
                                        <button class="btn btn-warning btn-xs btn_simpan_wait3<?=$row->id?>">Simpan</button>
                                    </td>
                                    <td>
                                        comp - unberthed<br/>
                                         <select class="form-control" name="j2wait41" id="j2wait4<?=$row->id?>">
                                            <option value="">--pilih--</option>
                                            <?php $wait_comp_unberthed = ''; foreach($waiting->result() as $wait1){
                                                if($row->waiting4 == $wait1->id){
                                                    $selected = 'selected';
                                                    $wait_comp_unberthed = $wait1->nama;
                                                }else{
                                                    $selected = '';
                                                }
                                            echo '<option value="'.$wait1->id.'" '.$selected.'>'.$wait1->nama.'</option>';
                                            }?>
                                        </select><br/>
                                        <button class="btn btn-warning btn-xs btn_simpan_wait4<?=$row->id?>">Simpan</button>
                                    </td>
                                    <td>
                                        unberthed - departure<br/>
                                         <select class="form-control" name="j2wait51" id="j2wait5<?=$row->id?>">
                                            <option value="">--pilih--</option>
                                            <?php $wait_unberthed_departur = ''; foreach($waiting->result() as $wait1){
                                                if($row->waiting5 == $wait1->id){
                                                    $selected = 'selected';
                                                    $wait_unberthed_departur = $wait1->nama;
                                                }else{
                                                    $selected = '';
                                                }
                                            echo '<option value="'.$wait1->id.'" '.$selected.'>'.$wait1->nama.'</option>';
                                            }?>
                                        </select><br/>
                                        <button class="btn btn-warning btn-xs btn_simpan_wait5<?=$row->id?>">Simpan</button>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            </div>
                            <?php 
                            $idkapal = $row->idkapal;
                            $idtujuan = $row->idtujuan;
                            ?>
                                <!-- ajax chat reminder -->
                                <script>
                                    var countdown = 30 * 60 * 1000;
                                    var c = 30 * 60 * 1000;

                                    setInterval(function(){
                                        //reminder arrival - berthed
                                        var time_berthed = new Date('<?=$row->berthed?>');
                                        var waiting_berthed = '<?=$wait_arrival_berthed?>';
                                        var time_comm = new Date('<?=$row->comm?>');
                                        var waiting_comm = '<?=$wait_berthed_comm?>';
                                        var time_comp = new Date('<?=$row->comp?>');
                                        var waiting_comp = '<?=$wait_comm_comp?>';
                                        var time_unberthed = new Date('<?=$row->unberthed?>');
                                        var waiting_unberthed = '<?=$wait_comp_unberthed?>';
                                        var time_departur = new Date('<?=$row->departure?>');
                                        var waiting_departur = '<?=$wait_unberthed_departur?>';
                                        var time_real = new Date();
                                        var id_asal = '<?=$row->idasal?>';
                                        var id_tujuan = '<?=$row->idtujuan?>';
                                        countdown -= 5000;
                                        var grog = countdown;
                                        if (typeof(Storage) !== "undefined") {
                                            // Code for localStorage/sessionStorage.
                                            if(localStorage.getItem("last_minute") != null) {
                                                localStorage.setItem("last_minute",countdown);
                                                countdown = localStorage.getItem("last_minute");
                                                if(localStorage.getItem("last_minute")=='0') {
                                                    countdown = c;
                                                }
                                            }else{
                                                localStorage.setItem("last_minute",c);
                                                countdown = c;
                                            }
                                        }

                                        if (grog <= 0) {
                                            if(time_berthed < time_real && waiting_berthed != 'WAITING DONE') {
                                                showNotif('Limit Waktu Berthed <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->berthed))?>');
                                                // console.log('Saat ini telah melebihi Time Berthed untuk kapal <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?>');
                                                var data = {
                                                        id_user:'Aplikasi',
                                                        tanggal:'<?=date('Y-m-d H:i:s')?>',
                                                        isi_chat:'Limit Waktu Berthed <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->berthed))?>',
                                                        id_asal: id_asal,
                                                        id_tujuan: id_tujuan
                                                    };
                                                $.ajax({
                                                    url: '<?=base_url()?>welcome/simpan_chat',
                                                    type: 'POST',
                                                    data: data,
                                                    dataType: 'JSON',
                                                    success: function(msg){
                                                        if(msg.status == 'sukses'){
                                                            localStorage.setItem("last_minute", 0);
                                                        }else if(msg.status){

                                                        }
                                                    }
                                                })

                                            }
                                            //reminder berthed - comm
                                            else if(time_comm < time_real && waiting_comm != 'WAITING DONE') {
                                                showNotif('Limit Waktu Comm <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->comm))?>');
                                                // console.log('Saat ini telah melebihi Time Comm untuk kapal <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?>');
                                                var data = {
                                                        id_user:'Aplikasi',
                                                        tanggal:'<?=date('Y-m-d H:i:s')?>',
                                                        isi_chat:'Limit Waktu Comm <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->comm))?>',
                                                        id_asal: id_asal,
                                                        id_tujuan: id_tujuan
                                                    };
                                                $.ajax({
                                                    url: '<?=base_url()?>welcome/simpan_chat',
                                                    type: 'POST',
                                                    data: data,
                                                    dataType: 'JSON',
                                                    success: function(msg){
                                                        if(msg.status == 'sukses'){
                                                            localStorage.setItem("last_minute", 0);
                                                        }else if(msg.status){

                                                        }
                                                    }
                                                })
                                            }
                                            //reminder comm - comp
                                            else if(time_comp < time_real && waiting_comp != 'WAITING DONE') {
                                                showNotif('Limit Waktu Comp <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->comp))?>');
                                                // console.log('Saat ini telah melebihi Time Comp untuk kapal <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?>');
                                                var data = {
                                                        id_user:'Aplikasi',
                                                        tanggal:'<?=date('Y-m-d H:i:s')?>',
                                                        isi_chat:'Limit Waktu Comp <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->comp))?>',
                                                        id_asal: id_asal,
                                                        id_tujuan: id_tujuan
                                                    };
                                                $.ajax({
                                                    url: '<?=base_url()?>welcome/simpan_chat',
                                                    type: 'POST',
                                                    data: data,
                                                    dataType: 'JSON',
                                                    success: function(msg){
                                                        if(msg.status == 'sukses'){
                                                            localStorage.setItem("last_minute", 0);
                                                        }else if(msg.status){

                                                        }
                                                    }
                                                })
                                            }
                                            //reminder comp - unberthed
                                            else if(time_unberthed < time_real && waiting_unberthed != 'WAITING DONE') {
                                                showNotif('Limit Waktu Unberthed <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->unberthed))?>');
                                                // console.log('Saat ini telah melebihi Time Unberthed untuk kapal <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?>');
                                                var data = {
                                                        id_user:'Aplikasi',
                                                        tanggal:'<?=date('Y-m-d H:i:s')?>',
                                                        isi_chat:'Limit Waktu Unberthed <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->unberthed))?>',
                                                        id_asal: id_asal,
                                                        id_tujuan: id_tujuan
                                                    };
                                                $.ajax({
                                                    url: '<?=base_url()?>welcome/simpan_chat',
                                                    type: 'POST',
                                                    data: data,
                                                    dataType: 'JSON',
                                                    success: function(msg){
                                                        if(msg.status == 'sukses'){
                                                            localStorage.setItem("last_minute", 0);
                                                        }else if(msg.status){

                                                        }
                                                    }
                                                })
                                            }
                                            //reminder unberthed - departur
                                            else if(time_departur < time_real && waiting_departur != 'WAITING DONE') {
                                                showNotif('Limit Waktu Departure <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->departure))?>');
                                                // console.log('Saat ini telah melebihi Time Departur untuk kapal <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?>');
                                                var data = {
                                                        id_user:'Aplikasi',
                                                        tanggal:'<?=date('Y-m-d H:i:s')?>',
                                                        isi_chat:'Limit Waktu Departure <?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> melebihi <?=date('d/m/y H:i',strtotime($row->departure))?>',
                                                        id_asal: id_asal,
                                                        id_tujuan: id_tujuan
                                                    };
                                                $.ajax({
                                                    url: '<?=base_url()?>welcome/simpan_chat',
                                                    type: 'POST',
                                                    data: data,
                                                    dataType: 'JSON',
                                                    success: function(msg){
                                                        if(msg.status == 'sukses'){
                                                            localStorage.setItem("last_minute", 0);
                                                        }else if(msg.status){

                                                        }
                                                    }
                                                })
                                            }
                                        } else {
                                            
                                        }

                                    }, 5000);
                                </script>

                                <script>
                                $(document).on('click', '.update_waktujetty<?=$row->id?>1', function(e){
                                    e.preventDefault();
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var tanggal = $('#arrival<?=$row->id?>').val();
                                    var idkapal=$('#idkapal<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty1tgl1',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+tanggal+'&idkapal='+idkapal,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.update_waktujetty<?=$row->id?>2', function(e){
                                    e.preventDefault();
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var tanggal = $('#berthed<?=$row->id?>').val();
                                    var idkapal = $('#idkapal<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty1tgl2',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+tanggal+'&idkapal='+idkapal,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.update_waktujetty<?=$row->id?>3', function(e){
                                    e.preventDefault();
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var tanggal = $('#comm<?=$row->id?>').val();
                                    var idkapal = $('#idkapal<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty1tgl3',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+tanggal+'&idkapal='+idkapal,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.update_waktujetty<?=$row->id?>4', function(e){
                                    e.preventDefault();
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var tanggal = $('#comp<?=$row->id?>').val();
                                    var idkapal = $('#idkapal<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty1tgl4',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+tanggal+'&idkapal='+idkapal,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.update_waktujetty<?=$row->id?>5', function(e){
                                    e.preventDefault();
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var tanggal = $('#unberthed<?=$row->id?>').val();
                                    var idkapal = $('#idkapal<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty1tgl5',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+tanggal+'&idkapal='+idkapal,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.update_waktujetty<?=$row->id?>6', function(e){
                                    e.preventDefault();
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var tanggal = $('#departure<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty1tgl6',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+tanggal,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.selesai_shipment', function(e){
                                    e.preventDefault();
                                    $('#modal_selesai_shipment').modal();
                                    var id = $(this).attr('id');
                                    $(document).on('click','.ya_selesai', function(e){
                                        e.preventDefault();
                                        $('#notif_selesai').html('Loading...');
                                        $.ajax({
                                            url: '<?=base_url()?>welcome/selesai_shipment',
                                            type: 'POST',
                                            data: 'id='+id,
                                            success: function(msg){
                                                $('#notif_selesai').html(msg);                    
                                            }
                                        });
                                    });
                                });

                                $(document).on('click', '.btn_simpan_wait1<?=$row->id?>', function(e){
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var status = $('#j2wait1<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty2arbe',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+status,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.btn_simpan_wait2<?=$row->id?>', function(e){
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var status = $('#j2wait2<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty2arbe2',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+status,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.btn_simpan_wait3<?=$row->id?>', function(e){
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var status = $('#j2wait3<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty2arbe3',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+status,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.btn_simpan_wait4<?=$row->id?>', function(e){
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var status = $('#j2wait4<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty2arbe4',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+status,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });

                                $(document).on('click', '.btn_simpan_wait5<?=$row->id?>', function(e){
                                    var id = $('#idjetty2<?=$row->id?>').val();
                                    var status = $('#j2wait5<?=$row->id?>').val();
                                    $.ajax({
                                        url: '<?=base_url()?>welcome/simpanjetty2arbe5',
                                        type: 'POST',
                                        data: 'id='+id+'&status='+status,
                                        dataType: 'JSON',
                                        success: function(msg){
                                            if(msg.status == 'sukses'){
                                                alert('berhasil diupdate');
                                                location.reload();
                                            }else if(msg.status == 'gagal'){
                                                alert('gagal diupdate');
                                            }
                                        }
                                    });
                                });
                                </script>

                                <!-- end ajax chat reminder -->
                            <?php
                            $query = $this->Model->kueri("SELECT * FROM sandarjetty JOIN detailjetty ON detailjetty.`id` = sandarjetty.`idjetty` JOIN listjetty ON listjetty.`id` = detailjetty.`idjetty` WHERE sandarjetty.`idkapal` = '$idkapal' AND detailjetty.`idpelabuhan` = '$idtujuan'");?>
                            <?php if($query->num_rows() == 0){
                                echo 'rekomendasi jetty tidak ditemukan';
                            }else{
                                echo "rekomendasi jetty: ". $query->row()->nama;
                            }
                            ?>
                            
                            <hr/>
                            <?php }?>
                        </div>

                    </div>
                <?php }}?>
                </div>
            </div>
            <!-- end japalid -->
            <?php
            }
            ?>
        </div>
    </div>
<br>
<br>
<br>
<br>
</main>




    

</div>

<!-- modal hapus -->
<div class="modal fade" id="modal_selesai_shipment" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Selesai</h4>
            </div>
            <div class="modal-body">
                <div class="section"> 
                    <p>Apakah anda akan selesai ?</p>
                    <p id="notif_selesai"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-success ya_selesai">Ya</button>
                <button type="submit" class="btn btn-sm btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>

<script>
Notification.requestPermission(function(status) {
    console.log('Notification permission status:', status);
});

function showNotif(msg) {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
        alert("This browser does not support system notifications");
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var img = '<?=base_url()?>assets/img/logo_pertamina.jpg';
        var text = msg;
        var notification = new Notification('Notification', { body: text, icon: img });
    }

    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
            var img = '<?=base_url()?>assets/img/logo_pertamina.jpg';
            var text = msg;
            var notification = new Notification('Notification', { body: text, icon: img });
        }
        });
    }
}

</script>

<!-- Modal -->
<div id="modal_proyeksi" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Proyek Stok</h4>
      </div>
      <div class="modal-body">
        <div id="result_proyeksi_stok_asal"></div>
        <div id="result_proyeksi_stok_tujuan"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
