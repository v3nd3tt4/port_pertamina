<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah Stok <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/store" method="post">
							<div class="form-group">
								<label for="idpelabuhan">Pelabuhan</label>
								<select name="idpelabuhan" id="idpelabuhan" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih pelabuhan</option>
									<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idproduk">Produk</label>
								<select name="idproduk" id="idproduk" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Produk</option>
									<?php if(!empty($produk)){foreach($produk as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label>Pumpable</label>
								<input type="number" name="pumpable" id="pumpable" class="form-control">
							</div>
							<div class="form-group">
								<label>Dot</label>
								<input type="number" name="dot" id="dot" class="form-control">
							</div>
							<div class="form-group">
								<label>Safe Stok</label>
								<input type="number" name="safestok" id="safestok" class="form-control">
							</div>
							<div class="form-group">
								<label>Dead Stok</label>
								<input type="number" name="deadstok" id="deadstok" class="form-control">
							</div>
							<button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>