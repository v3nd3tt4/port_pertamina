<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah tipekapal <i class="fa fa-pencil pull-right"></i></h3>
					<div class="mT-30">
						<form action="<?=base_url()?>tipekapal/store" method="post">
							<div class="form-group">
								<label for="exampleInputEmail1">Nama Tipekapal</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nama" placeholder="Enter tipekapal">
							</div>
							
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>