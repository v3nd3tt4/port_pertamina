<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h3 class="header-form">Tambah Estimasi Waktu <i class="fa fa-pencil pull-right"></i> </h3>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/store" method="post">
							<div class="form-group">
								<label for="idkapal">Kapal</label>
								<select name="idkapal" id="idkapal" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Kapal</option>
									<?php if(!empty($kapal)){foreach($kapal as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idpelabuhan">Pelabuhan</label>
								<select name="idpelabuhan" id="idpelabuhan" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Pelabuhan</option>
									<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idlistket">Keterangan</label>
								<select name="idlistket" id="idlistket" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Keterangan</option>
									<?php if(!empty($keterangan)){foreach($keterangan as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="estimasiwaktu">Estimasi Waktu</label>
								<input name="estimasiwaktu" type="text" class="form-control" id="estimasiwaktu" aria-describedby="namaHelp" placeholder="Masukkan Estimasi Waktu (00:00:00)" required>
							</div>
							<div class="form-group">
								<label for="status">Status</label>
								<select name="status" id="status" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Status</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</div>
							<button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>