<!-- <?php if(count($datadischarge->result()) != 0){ echo '<h4>Status Discharge</h4>';}?> -->
							<?php $no=1; foreach($datadischarge->result() as $row){

								// $datetime1 = new DateTime($row->arrival);
						  //       $datetime2 = new DateTime($row->departure);
						  //       $interval = $datetime1->diff($datetime2);
						  //       $date = $interval->format('%H:%i:%s');

						  //       //$elapsed = $interval->format('%H:%i HRS');
						  //       $elapsed = date('H:i', strtotime($date)).' HRS';
								echo '<h4>Status Discharge</h4>';
						        $date1 = new DateTime($row->arrival);
                                $date2 = new DateTime($row->departure);

                                $diff = $date2->diff($date1);

                                $hours = $diff->h;
                                $minutes = $diff->i;
                                $hours = $hours + ($diff->days*24);

                                $elapsed = $hours.':'.$minutes.' HRS';
						        

						        $detailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $row->id));

								?>
								<?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?> ( <?php $i=0;foreach($detailshipment->result() as $rowdetailshipment){ 
									if($i==0){
										echo $this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
									}else{
										echo ', '.$this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
									}
									$i++;
								}?>

								) 
								<br/>
								antrian : <?=$row->antrian?>, Jetty: <?=$row->idjetty?>, Pelabuhan : <?=$this->Model->getdata('pelabuhan', array('id' => $row->idpelabuhanbantuan))->row()->nama?> 
							<br/>
							<a href="<?=base_url()?>simulasiplanner/hapus_planner/<?=$row->id?>" class="btn btn-danger btn-sm" id="<?=$row->id?>" onclick="return confirm('Apakah anda yakin?')">Hapus</a>
							<a href="#" class="btn btn-success btn-sm btn_edit_waktu_planner<?=$row->id?>" id="<?=$row->id?>">Edit waktu</a>
							<a href="#" class="btn btn-warning btn-sm btn_update_waktu_planner<?=$row->id?>" style="display: none" onclick="return confirm('Apakah anda yakin?')">Update waktu</a>
							<a href="#" class="btn btn-primary btn-sm btn_batal_waktu_planner<?=$row->id?>" style="display: none">Batal update waktu</a>
							<div class="resultedit"></div>
							<div class="table-responsive">
							<form id="formubahloading<?=$row->id?>">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%">
								
								<thead>
									<tr role="row">										
										<th>arrival</th>
										<th>berthed</th>
										<th>comm</th>
										<th>comp</th>
										<th>unberthed</th>
										<th>departure</th>								
										<th>Tujuan</th>
										<th>IPT</th>
										<!-- <th>Status</th>
										<th>Aksi</th> -->
									</tr>
								</thead>
								<tbody>
									<tr id="<?=$row->id?>" >
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">				
												<input type="hidden" name="idshipmenteditwaktu" id="idshipmenteditwaktu<?=$row->id?>"  class="form-control">								
												<input type="text" name="waktuarrivaledit" id="waktuarrivaledit<?=$row->id?>" value="<?=$row->arrival?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->arrival?></div>												
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktuberthededit" id="waktuberthededit<?=$row->id?>" value="<?=$row->berthed?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->berthed?></div>												
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktucommdedit" id="waktucommdedit<?=$row->id?>" value="<?=$row->comm?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->comm?></div>												
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktucompdedit" id="waktucompdedit<?=$row->id?>" value="<?=$row->comp?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->comp?></div>
										</td>	
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktuunberthededit" id="waktuunberthededit<?=$row->id?>" value="<?=$row->unberthed?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->unberthed?></div>
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktudepartureedit" id="waktudepartureedit<?=$row->id?>" value="<?=$row->departure?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->departure?></div>
										</td>				
										<td><?=$this->Model->getdata('pelabuhan', array('id' => $row->idtujuan))->row()->nama?></td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="elapsededit" id="elapsededit<?=$row->id?>" value="<?=$elapsed?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$elapsed?></div>
										</td>
										
										<!-- <td><?=$row->status?></td>
										<td>
											<?php if($row->status === NULL || $row->status == 'simulasi'){
												echo '<a href="'.base_url().'simulasiplanner/prosessimulasinya/'.$row->id.'" class="btn btn-primary btn-sm">Proses</a>';
											}else if($row->status == 'proses'){
												echo '<a href="'.base_url().'simulasiplanner/donesimulasi/'.$row->id.'" class="btn btn-warning btn-sm">Done</a>';
											}else{
												echo '';
											}?>
											<button class="btn btn-danger btn-sm">Hapus</button>
										</td> -->
									</tr>
									<!-- <tr id="<?=$row->id?>" class="edit_row" title="Klik untuk edit">
										<td><?=$row->arrival?></td>
										<td><?=$row->berthed?></td>
										<td><?=$row->comm?></td>
										<td><?=$row->comp?></td>
										<td><?=$row->unberthed?></td>
										<td><?=$row->departure?></td>				
										<td><?=$this->Model->getdata('pelabuhan', array('id' => $row->idtujuan))->row()->nama?></td>
										<td style="color: green"><?=$elapsed?></td>
										< <td><?=$row->status?></td>
										<td>
											<?php if($row->status === NULL || $row->status == 'simulasi'){
												echo '<a href="'.base_url().'simulasiplanner/prosessimulasinya/'.$row->id.'" class="btn btn-primary btn-sm">Proses</a>';
											}else if($row->status == 'proses'){
												echo '<a href="'.base_url().'simulasiplanner/donesimulasi/'.$row->id.'" class="btn btn-warning btn-sm">Done</a>';
											}else{
												echo '';
											}?>
											<button class="btn btn-danger btn-sm">Hapus</button>
										</td> 
									</tr> -->									
								</tbody>
							</table>
							</form>
							
							<script type="text/javascript">
								$(document).on('change', '#waktuarrivaledit<?=$row->id?>', function(e){
								    var idshipment = $('#idshipmenteditwaktu<?=$row->id?>').val();
								    var waktuarrivaledit = $('#waktuarrivaledit<?=$row->id?>').val();
								    $.ajax({
								      url: '<?=base_url()?>simulasiplanner/getwaktuedit',
								      type: 'POST',
								      data: {'idshipment':idshipment,'arrival':waktuarrivaledit},
								      dataType: 'JSON',
								      success: function(msg){
								        $('#waktuberthededit<?=$row->id?>').val(msg.waktu_sandar);
								        $('#waktucommdedit<?=$row->id?>').val(msg.waktu_comm);
								        $('#waktucompdedit<?=$row->id?>').val(msg.waktu_comp);
								        $('#waktuunberthededit<?=$row->id?>').val(msg.waktu_lepas);
								        $('#waktudepartureedit<?=$row->id?>').val(msg.waktu_berangkat);
								        $('#elapsededit<?=$row->id?>').val(msg.ipt);
								      }
								    })
								  });


								  $(document).on('click', '.btn_update_waktu_planner<?=$row->id?>', function(e){
								    e.preventDefault();
								    var idshipment = $('#idshipmenteditwaktu<?=$row->id?>').val();
								    var waktuarrivaledit = $('#waktuarrivaledit<?=$row->id?>').val();
								    var waktuberthededit = $('#waktuberthededit<?=$row->id?>').val();
								    var waktucommdedit = $('#waktucommdedit<?=$row->id?>').val();
								    var waktucompdedit = $('#waktucompdedit<?=$row->id?>').val();
								    var waktuunberthededit = $('#waktuunberthededit<?=$row->id?>').val();
								    var waktudepartureedit = $('#waktudepartureedit<?=$row->id?>').val();
								    
								    var formnya = $('#formubahloading'+<?=$row->id?>).serialize();
								    // alert(datanew);
								    $.ajax({
								      url: '<?=base_url()?>simulasiplanner/updatewaktusimulasiplanner',
								      type: 'POST',
								      data: formnya,
								      success: function(msg){
								        $('.resultedit').html(msg);
								      }
								    });
								  });

								  $(document).on('click','.btn_edit_waktu_planner<?=$row->id?>', function(e){
								    e.preventDefault();
								    var idshipment = $(this).attr('id');
								    $('#idshipmenteditwaktu'+idshipment).val(idshipment);
								    $('.btn_update_waktu_planner<?=$row->id?>').show();
								    $('.btn_batal_waktu_planner<?=$row->id?>').show();
								    $('.btn_edit_waktu_planner<?=$row->id?>').hide();
								    $('.hidewaktu<?=$row->id?>').show();
								    $('.showwaktu<?=$row->id?>').hide();
								  });

								  $(document).on('click', '.btn_batal_waktu_planner<?=$row->id?>', function(e){
								    e.preventDefault();
								    $('.btn_update_waktu_planner<?=$row->id?>').hide();
								    $('.btn_batal_waktu_planner<?=$row->id?>').hide();
								    $('.btn_edit_waktu_planner<?=$row->id?>').show();
								    $('.hidewaktu<?=$row->id?>').hide();
								    $('.showwaktu<?=$row->id?>').show();
								    location.reload();
								  });
							</script>
							</div>
							<?php 
							$idkapal = $row->idkapal;
							$idtujuan = $row->idtujuan;
							$query = $this->Model->kueri("SELECT * FROM sandarjetty JOIN detailjetty ON detailjetty.`id` = sandarjetty.`idjetty` JOIN listjetty ON listjetty.`id` = detailjetty.`idjetty` WHERE sandarjetty.`idkapal` = '$idkapal' AND detailjetty.`idpelabuhan` = '$idtujuan'");?>
							<?php if($query->num_rows() == 0){
								echo 'rekomendasi jetty tidak ditemukan';
							}else{
								echo "rekomendasi jetty: ". $query->row()->nama;
							}
							?>
							
							<hr/>
							<?php }?>