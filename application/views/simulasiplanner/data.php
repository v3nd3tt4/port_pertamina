<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="container-fluid">
			<h4 class="c-grey-900 mT-10 mB-30"></h4>

			<!-- <div class="row">
				<div class="col-md-2">
					<h3>Proyeksi Stok</h3>
					<select name="pilihproduk" id="pilihproduk" class="form-control">
						<option value="">Pilih Produk</option>
						<?php if(!empty($produk)){foreach($produk as $row){ ?>
						<option value="<?=$row->id?>"><?=$row->nama?></option>
						<?php }}?>
					</select>
				</div>
				<div class="col-md-12">
					<h3 id="loadernya" style="display:none">Sedang Proyeksi Stok...</h3>
					<div id="proyeksistok"></div>
				</div>
			</div> -->
	<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="bgc-white bd bdrs-3 p-20 mB-20">
						<!-- <a href="<?=base_url().$menu?>/formtambah" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>	Tambah Data</a> -->
						<h4 class="c-grey-900 mB-20">Data Simulasi Planner</h4>
						<div class="alert alert-danger"><strong>Perhatian, </strong>pilih terlebih dahulu kapal, asal, tujuan, produk, jumlah produk, satuan sebelum memilih waktu datang</div>
						<div id="dataTable_wrapper" class="dataTables_wrapper">
							<form id="form_simulasi" action="<?=base_url()?>simulasiplanner/store" method="POST">
								<button type="submit" class="btn btn-primary pull-right">Simpan</button>
							<select name="idkapal" id="idkapal" aria-describedby="statusHelp" required>
								<option value="">Pilih Kapal</option>
								<?php if(!empty($kapal)){foreach($kapal as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
								<?php }}?>
							</select>
							<select name="idasal" id="idasal" aria-describedby="statusHelp" required>
								<option value="">Pilih asal</option>
								<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?> </option>
								<?php }}?>
							</select>
							<select name="idtujuan" id="idtujuan"  aria-describedby="statusHelp" required>
								<option value="">Pilih tujuan</option>
								<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?> </option>
								<?php }}?>
							</select>
							<select name="proses" id="proses" required>
								<option value="">Pilih proses</option>
								<option value="0">Loading</option>
								<option value="1">Discharge</option>
							</select>
							<select name="idjetty" id="idjetty" required>
								<option value="">Pilih jetty</option>
								<option value="1">jetty 1</option>
								<option value="2">jetty 2</option>
							</select>
							<br/>	<br/>
							<select name="idproduk" id="idproduk" aria-describedby="statusHelp" required>
								<option value="">Pilih produk</option>
								<?php if(!empty($produk)){foreach($produk as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?> </option>
								<?php }}?>
							</select>
							<input type="text" name="jumlah" id="jumlah" placeholder="Jumlah" />			
							<select name="idsatuan" id="idsatuan" aria-describedby="statusHelp" required>
								<option value="">Pilih satuan</option>
								<?php if(!empty($satuan)){foreach($satuan as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?> </option>
								<?php }}?>
							</select>
							<button type="button" class="btn btn-xs btn-success btn_tambah_produk">Tambah produk</button>						
							<br/><br/>
							<div class="table-responsive">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%">					
								<thead>
									<tr role="row">										
										<th>arrival <button class="btn btn-xs btn-success set_waktu">Set</button> <a href="<?=base_url()?>simulasiplanner" class="btn btn-danger btn-xs" onclick="return confirm('Data inputan akan dikosongkan semua')">Reset</a></th>
										<th>berthed</th>
										<th>comm</th>
										<th>comp</th>
										<th>unberthed</th>
										<th>departure</th>								
										<!-- <th>Tujuan</th> -->
										<th>IPT</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
										<input required type="text" name="waktudatang" id="waktudatang" class="form-control" /></td>
										<td>
											<input required type="text" name="waktu_sandar" id="waktu_sandar" class="form-control"/></td>
										</td>
										<td>
											<input required type="text" name="waktu_comm" readonly="" id="waktu_comm" class="form-control"/></td>
										</td>
										<td>
											<input required type="text" name="waktu_comp" readonly="" id="waktu_comp" class="form-control"/></td>
										</td>
										<td>
											<input required type="text" name="waktu_lepas" readonly="" id="waktu_lepas" class="form-control"/></td>
										</td>
										<td>
											<input required type="text" name="waktu_berangkat" readonly="" id="waktu_berangkat" class="form-control"/></td>
										</td>
										<!-- <td>
											<select name="idtujuan" id="idtujuan"  aria-describedby="statusHelp" class="form-control" required>
												<option value="">Pilih tujuan</option>
												<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
												<option value="<?=$row->id?>"><?=$row->nama?> </option>
												<?php }}?>
											</select>
										</td> -->
										<td>
											<input required type="text" name="ipt" readonly="" id="ipt" class="form-control"/></td>
										</td>
									</tr>
								</tbody>
							</table></div>
							<div id="result_waiting_ullage"></div> <input type="hidden" name="val_waiting_ullage" id="val_waiting_ullage"/>
							<br/>
							
							<div class="table-responsive">
							<table class="table" id="resulttambahproduk">
								
							</table>
							</div>
							</form>
							<hr/>
							 <div id="tampiledit">
                  			</div>
							<br/>
							<h4>Pilih Pelabuhan yang akan ditampilkan</h4>
							<form action=""  method="GET">
								<select name="idpelabuhanbantuan">
									<option value="">--pilih--</option>
									<?php
									foreach($pelabuhan as $rowpelabuhan){
										$parampelabuhan = $this->input->get('idpelabuhanbantuan', true);
									?>
									<option value="<?=$rowpelabuhan->id?>" <?=($rowpelabuhan->id==$parampelabuhan)?'selected':''?>><?=$rowpelabuhan->nama?></option>
									<?php
									}
									?>
								</select>
								<button class="btn btn-warning btn-sm">Tampilkan</button>
								<a href="<?=base_url()?>simulasiplanner" class="btn btn-danger btn-sm" onclick="return confirm('Data yang akan ditampilkan adalah data berdasar pelabuhan user login?')">Reset</a>
							</form>
							<br/><br/>
							<!-- <?php if(count($dataloading->result()) != 0){ echo '<h4>Status Loading</h4>';}?> -->
							<?php $no=1; foreach($dataloading->result() as $row){
								if($row->proses == '0'){
									$status = 'Loading';
								}else{
									$status = 'Discharge';
								}
								echo '<h4>Status '.$status.'</h4>'.'Posisi Kapal Proses ini di : '.$row->namapelabuhan. ' ;	';
								// $datetime1 = new DateTime($row->arrival);
						  //       $datetime2 = new DateTime($row->departure);
						  //       $interval = $datetime1->diff($datetime2);
						  //       $date = $interval->format('%H:%i:%s');

						  //       //$elapsed = $interval->format('%H:%i HRS');
						  //       $elapsed = date('H:i', strtotime($date)).' HRS';

						        $date1 = new DateTime($row->arrival);
                                $date2 = new DateTime($row->departure);

                                $diff = $date2->diff($date1);

                                $hours = $diff->h;
                                $minutes = $diff->i;
                                $hours = $hours + ($diff->days*24);

                                $elapsed = $hours.':'.$minutes.' HRS';
						        

						        $detailshipment = $this->Model->getdata('detailshipment', array('idshipment' => $row->id));

								?>
								<?=$this->Model->getdata('kapal', array('id' => $row->idkapal))->row()->nama?> from <?=$this->Model->getdata('pelabuhan', array('id' => $row->idasal))->row()->nama?> ( <?php $i=0;foreach($detailshipment->result() as $rowdetailshipment){ 
									if($i==0){
										echo $this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
									}else{
										echo ', '.$this->Model->getdata('produk', array('id' => $rowdetailshipment->idproduk))->row()->nama.' '.$rowdetailshipment->jumlah.' '.$this->Model->getdata('listsatuan', array('id' => $rowdetailshipment->idsatuan))->row()->nama; 
									}
									$i++;
								}?>

								)
								<br/>
								antrian : <?=$row->antrian?>, Jetty: <?=$row->idjetty?>, Pelabuhan : <?=$this->Model->getdata('pelabuhan', array('id' => $row->idpelabuhanbantuan))->row()->nama?> 

							<br/>
							<form action="<?=base_url()?>simulasiplanner/updateantrian" method="POST">
							<a href="<?=base_url()?>simulasiplanner/hapus_planner/<?=$row->id?>" class="btn btn-danger btn-sm" id="<?=$row->id?>" onclick="return confirm('Apakah anda yakin?')">Hapus</a> 
							<a href="#" class="btn btn-success btn-sm btn_edit_waktu_planner<?=$row->id?>" id="<?=$row->id?>">Edit waktu</a>
							<a href="#" class="btn btn-warning btn-sm btn_update_waktu_planner<?=$row->id?>" style="display: none" onclick="return confirm('Apakah anda yakin?')">Update waktu</a>
							<a href="#" class="btn btn-primary btn-sm btn_batal_waktu_planner<?=$row->id?>" style="display: none">Batal update waktu</a>
							
								<input type="hidden" name="idshipmentupdateantrian" id="idshipmentupdateantrian" value="<?=$row->id?>">
								<select name="editantrian" id="editantrian">
									<option value="">--pilih--</option>
									<?php 
									$q = "select count(*) as jumlahdata from shipment where idjetty='".$row->idjetty."' and idpelabuhanbantuan='".$row->idpelabuhanbantuan."' and date(arrival) = '".date('Y-m-d', strtotime($row->arrival))."'";

										$q = $this->Model->kueri($q);
										for($i=1;$i<=$q->row()->jumlahdata;$i++){
									?>
										<option value="<?php echo $i; ?>">Antrian <?=$i?></option>
									<?php
										}

									?>
								</select>
								<button type="submit" onclick="return confirm('Apakah anda yakin?')" class="btn btn-primary btn-sm">Update antrian</button>
							</form>
							<div class="resultedit"></div>
							<div class="table-responsive">
							<form id="formubahloading<?=$row->id?>">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%">
								
								<thead>
									<tr role="row">										
										<th>arrival</th>
										<th>berthed</th>
										<th>comm</th>
										<th>comp</th>
										<th>unberthed</th>
										<th>departure</th>								
										<th>Tujuan</th>
										<th>IPT</th>
										<!-- <th>Status</th>
										<th>Aksi</th> -->
									</tr>
								</thead>
								<tbody>
									<tr id="<?=$row->id?>" >
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">				
												<input type="hidden" name="idshipmenteditwaktu" id="idshipmenteditwaktu<?=$row->id?>"  class="form-control">								
												<input type="text" name="waktuarrivaledit" id="waktuarrivaledit<?=$row->id?>" value="<?=$row->arrival?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->arrival?></div>												
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktuberthededit" id="waktuberthededit<?=$row->id?>" value="<?=$row->berthed?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->berthed?></div>												
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktucommdedit" id="waktucommdedit<?=$row->id?>" value="<?=$row->comm?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->comm?></div>												
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktucompdedit" id="waktucompdedit<?=$row->id?>" value="<?=$row->comp?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->comp?></div>
										</td>	
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktuunberthededit" id="waktuunberthededit<?=$row->id?>" value="<?=$row->unberthed?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->unberthed?></div>
										</td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="waktudepartureedit" id="waktudepartureedit<?=$row->id?>" value="<?=$row->departure?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$row->departure?></div>
										</td>				
										<td><?=$this->Model->getdata('pelabuhan', array('id' => $row->idtujuan))->row()->nama?></td>
										<td>
											<div class="hidewaktu<?=$row->id?>" style="display: none">												
												<input type="text" name="elapsededit" id="elapsededit<?=$row->id?>" value="<?=$elapsed?>" class="form-control datepick">
											</div>
											<div class="showwaktu<?=$row->id?>"><?=$elapsed?></div>
										</td>
										
										<!-- <td><?=$row->status?></td>
										<td>
											<?php if($row->status === NULL || $row->status == 'simulasi'){
												echo '<a href="'.base_url().'simulasiplanner/prosessimulasinya/'.$row->id.'" class="btn btn-primary btn-sm">Proses</a>';
											}else if($row->status == 'proses'){
												echo '<a href="'.base_url().'simulasiplanner/donesimulasi/'.$row->id.'" class="btn btn-warning btn-sm">Done</a>';
											}else{
												echo '';
											}?>
											<button class="btn btn-danger btn-sm">Hapus</button>
										</td> -->
									</tr>									
								</tbody>
							</table>
							</form>
							<script type="text/javascript">
								$(document).on('change', '#waktuarrivaledit<?=$row->id?>', function(e){
								    var idshipment = $('#idshipmenteditwaktu<?=$row->id?>').val();
								    var waktuarrivaledit = $('#waktuarrivaledit<?=$row->id?>').val();
								    $.ajax({
								      url: '<?=base_url()?>simulasiplanner/getwaktuedit',
								      type: 'POST',
								      data: {'idshipment':idshipment,'arrival':waktuarrivaledit},
								      dataType: 'JSON',
								      success: function(msg){
								        $('#waktuberthededit<?=$row->id?>').val(msg.waktu_sandar);
								        $('#waktucommdedit<?=$row->id?>').val(msg.waktu_comm);
								        $('#waktucompdedit<?=$row->id?>').val(msg.waktu_comp);
								        $('#waktuunberthededit<?=$row->id?>').val(msg.waktu_lepas);
								        $('#waktudepartureedit<?=$row->id?>').val(msg.waktu_berangkat);
								        $('#elapsededit<?=$row->id?>').val(msg.ipt);
								      }
								    })
								  });


								  $(document).on('click', '.btn_update_waktu_planner<?=$row->id?>', function(e){
								    e.preventDefault();
								    var idshipment = $('#idshipmenteditwaktu<?=$row->id?>').val();
								    var waktuarrivaledit = $('#waktuarrivaledit<?=$row->id?>').val();
								    var waktuberthededit = $('#waktuberthededit<?=$row->id?>').val();
								    var waktucommdedit = $('#waktucommdedit<?=$row->id?>').val();
								    var waktucompdedit = $('#waktucompdedit<?=$row->id?>').val();
								    var waktuunberthededit = $('#waktuunberthededit<?=$row->id?>').val();
								    var waktudepartureedit = $('#waktudepartureedit<?=$row->id?>').val();
								    
								    var formnya = $('#formubahloading'+<?=$row->id?>).serialize();
								    // alert(datanew);
								    $.ajax({
								      url: '<?=base_url()?>simulasiplanner/updatewaktusimulasiplanner',
								      type: 'POST',
								      data: formnya,
								      success: function(msg){
								        $('.resultedit').html(msg);
								      }
								    });
								  });

								  $(document).on('click','.btn_edit_waktu_planner<?=$row->id?>', function(e){
								    e.preventDefault();
								    var idshipment = $(this).attr('id');
								    $('#idshipmenteditwaktu'+idshipment).val(idshipment);
								    $('.btn_update_waktu_planner<?=$row->id?>').show();
								    $('.btn_batal_waktu_planner<?=$row->id?>').show();
								    $('.btn_edit_waktu_planner<?=$row->id?>').hide();
								    $('.hidewaktu<?=$row->id?>').show();
								    $('.showwaktu<?=$row->id?>').hide();
								  });

								  $(document).on('click', '.btn_batal_waktu_planner<?=$row->id?>', function(e){
								    e.preventDefault();
								    $('.btn_update_waktu_planner<?=$row->id?>').hide();
								    $('.btn_batal_waktu_planner<?=$row->id?>').hide();
								    $('.btn_edit_waktu_planner<?=$row->id?>').show();
								    $('.hidewaktu<?=$row->id?>').hide();
								    $('.showwaktu<?=$row->id?>').show();
								    location.reload();
								  });
							</script>
							</div>
							<?php 
							$idkapal = $row->idkapal;
							$idtujuan = $row->idtujuan;
							$query = $this->Model->kueri("SELECT * FROM sandarjetty JOIN detailjetty ON detailjetty.`id` = sandarjetty.`idjetty` JOIN listjetty ON listjetty.`id` = detailjetty.`idjetty` WHERE sandarjetty.`idkapal` = '$idkapal' AND detailjetty.`idpelabuhan` = '$idtujuan'");?>
							<?php if($query->num_rows() == 0){
								//echo 'rekomendasi jetty tidak ditemukan';
								//}else{
								//echo "rekomendasi jetty: ". $query->row()->nama;
							}
							


							?>
							<!-- Ketahanan Stok hingga <?=date_format(date_create($row->arrival),'d-m-Y')?> brp?
							<br>
							Ketahanan Stok pada <?=date_format(date_create($row->comp),'d-m-Y')?> brp?
							<br>
							Ullage hingga <?=date_format(date_create($row->arrival),'d-m-Y')?> brp?
							<br>
							Ullage pada <?=date_format(date_create($row->comp),'d-m-Y')?> brp? -->
							<?php
							if($row->antrian>1){
							?>
							<div class="alert alert-warning">
								Kapal ini Menunggu Jetty
							</div>
							
							<?php
							}
							?>
							

							<hr/>
							<?php }?>


							
							<br/><br/><br/>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<!-- Modal -->
<div id="modal_proyeksi" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Proyek Stok</h4>
      </div>
      <div class="modal-body">
        <div id="result_proyeksi_stok_asal"></div>
        <div id="result_proyeksi_stok_tujuan"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- modal hapus -->
<div class="modal fade" id="modal_hapus_planner" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="modal-title">Hapus Planner</h4>
          	</div>
          	<div class="modal-body">
          		<div class="section"> 
          			<p>Apakah anda yakin akan menghapus data ini ?</p>
          			<p id="notif_hapus_planner"></p>
          		</div>
          	</div>
          	<div class="modal-footer">
          		<button type="submit" class="btn btn-sm btn-danger ya_hapus_planner">Ya</button>
          		<button type="submit" class="btn btn-sm btn-default" data-dismiss="modal">Tidak</button>
          	</div>
        </div>
    </div>
</div>