<br>
<table class="table table-bordered">
    <thead>
        <tr class="alert alert-warning">
            <th>Stok Awal <?=@$tanggalreal?></th>
            <th>Mutasi/Hari</th>

            <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
            <th class="alert alert-primary">
                Stok <?=$row['tanggal']?>
            </th>
            <?php }}?>

        </tr>
    </thead>
    <tbody>
        <tr class="alert alert-warning">
            <td><?=@$stokreal?></td>
            <td><?=@$mutasi?></td>
            <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
            <td class="alert alert-primary">
                Stok <?=$row['stok']?>
            </td>
            <?php }}?>
        </tr>
        <tr>
            <td colspan="2">Ketahanan Stok</td>
            <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
            <td class="alert alert-primary">
                <?=($row['stok']=='0')?'0':$row['ketahanan']?>
            </td>
            <?php }}?>
        </tr>
        <tr>
            <td colspan="2">Deadstok</td>
            <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
            <td class="alert alert-primary">
                <?=($row['stok']=='0')?'0':$row['deadstok']?>
            </td>
            <?php }}?>
        </tr>
        <tr>
            <td colspan="2">Safestok</td>
            <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
            <td class="alert alert-primary">
                <?=($row['stok']=='0')?'0':$row['safestok']?>
            </td>
            <?php }}?>
        </tr>
        <tr>
            <td colspan="2">Ullage</td>
            <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
            <td class="alert alert-primary">
                <?=($row['stok']=='0')?'0':$row['ullage']?>
            </td>
            <?php }}?>
        </tr>
    </tbody>
</table>