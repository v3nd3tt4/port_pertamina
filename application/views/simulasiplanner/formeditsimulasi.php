
<h3>Edit Simulasi</h3>
									<form id="e_form_simulasi" method="POST">
										<button type="submit" class="btn btn-success pull-right">Edit</button>
<table class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%">
								
								<thead>
									<tr role="row">										
										<th>arrival</th>
										<th>berthed</th>
										<th>comm</th>
										<th>comp</th>
										<th>unberthed</th>
										<th>departure</th>								
										<!-- <th>Tujuan</th> -->
										<th>IPT</th>
										<!-- <th>Status</th>
										<th>Aksi</th> -->
									</tr>
								</thead>
								<tbody>
										<input type="hidden" name="e_idkapal" id="e_idkapal">
										<input type="hidden" name="jumlah" id="jumlah">
										<tr>
											<td>
												<input type="hidden" name="id" id="id">
												<input required type="text" name="e_waktudatang" id="e_waktudatang" class="form-control" />
											</td>
											<td>
												<input required type="text" name="e_waktu_sandar" id="e_waktu_sandar" class="form-control" />
											</td>										
											<td>
												<input required type="text" name="e_waktu_comm" id="e_waktu_comm" class="form-control" />
											</td>										
											<td>
												<input required type="text" name="e_waktu_comp" id="e_waktu_comp" class="form-control" />
											</td>										
											<td>
												<input required type="text" name="e_waktu_lepas" id="e_waktu_lepas" class="form-control" />
											</td>										
											<td>
												<input required type="text" name="e_waktu_berangkat" id="e_waktu_berangkat" class="form-control" />
											</td>
											<td>
												<input required type="text" name="e_ipt" id="e_ipt" class="form-control"/ readonly>
											</td>
											
										</tr>								
								</tbody>
							</table>
									</form>	

<script type="text/javascript">
	 $('#e_waktudatang').datetimepicker({
                format:'Y-m-d H:i:s',
        });
	$(document).on('change', '#e_waktudatang', function(e){
                e.preventDefault();
                //var waktu_datang = $('#e_waktudatang').val();
                // if(waktu_datang == ''){
                //         alert('waktu datang harus diisi');
                // }else{
                var data = $('#e_form_simulasi').serialize();
                // console.log($('#e_form_simulasi'));
                // alert(data);
                $.ajax({
                        url: '<?=base_url()?>simulasiplanner/e_getWaktu',
                        type: 'POST',
                        data: data, 
                        dataType: 'JSON',
                        success: function(msg){
                                $('#e_waktu_sandar').val(msg.e_waktu_sandar);
                                $('#e_waktu_comm').val(msg.e_waktu_comm);
                                $('#e_waktu_comp').val(msg.e_waktu_comp);
                                $('#e_waktu_lepas').val(msg.e_waktu_lepas);
                                $('#e_waktu_berangkat').val(msg.e_waktu_berangkat);
                                $('#e_ipt').val(msg.e_ipt);
                        }
                });
               // }                               
        });
</script>