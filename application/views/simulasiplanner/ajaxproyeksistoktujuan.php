<h4>Proyeksi Tujuan (<?=$this->Model->getdata('pelabuhan', array('id' => $idasal))->row()->nama?>)</h4>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr class="alert alert-warning">
                <th>Stok Awal <?=@$tanggalreal?></th>
                <th>Mutasi/Hari</th>

                <th>Stok Intransit <?=@$tanggalreal?></th>

                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <th class="alert alert-primary">
                    Stok <?=$row['tanggal']?>
                </th>
                <?php }}?>

            </tr>
        </thead>
        <tbody>
            <tr class="alert alert-warning">
                <td><?=@$stokreal?></td>
                <td><?=@$mutasi?></td>
                <td><?=@$stokintransit?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    Stok <?=$row['stok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Ketahanan Stok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ketahanan']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Deadstok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['deadstok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Safestok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['safestok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Ullage</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ullage']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Mutasi</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0': '+'.$jumlah?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Stok Akhir</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['stok']+$jumlah?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Ullage Akhir</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ullage']-$jumlah?>
                </td>
                <?php }}?>
            </tr>
        </tbody>
    </table>
</div>