
<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item col-md-6" style="position: absolute; left: 0%;top: 0:">
				<div class="bgc-white p-20 bd">
					<h6 class="c-grey-900">Tambah Simulasi</h6>
					<div class="mT-30">
						<form action="<?=base_url().$menu?>/prosessimulasi" method="post">
							<div class="form-group">
								<label for="idkapal">Kapal</label>
								<select name="idkapal" id="idkapal" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih Kapal</option>
									<?php if(!empty($kapal)){foreach($kapal as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idjetty">Asal</label>
								<select name="idasal" id="idasal" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih asal</option>
									<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?> </option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idjetty">Tujuan</label>
								<select name="idtujuan" id="idtujuan" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih tujuan</option>
									<?php if(!empty($pelabuhan)){foreach($pelabuhan as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?> </option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idjetty">Produk</label>
								<select name="idproduk" id="idproduk" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih produk</option>
									<?php if(!empty($produk)){foreach($produk as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?> </option>
									<?php }}?>
								</select>
							</div>
							<div class="form-group">
								<label for="idjetty">Waktu datang</label>
								<input type="text" name="waktudatang" id="waktudatang" class="form-control"/>								
							</div>
							<div class="form-group">
								<label for="idjetty">Jumlah</label>
								<input type="text" name="jumlah" id="jumlah" class="form-control"/>								
							</div>
							<div class="form-group">
								<label for="idjetty">Satuan</label>
								<select name="idsatuan" id="idsatuan" class="form-control" aria-describedby="statusHelp" required>
									<option value="">Pilih satuan</option>
									<?php if(!empty($satuan)){foreach($satuan as $row){ ?>
									<option value="<?=$row->id?>"><?=$row->nama?> </option>
									<?php }}?>
								</select>
							</div>
							<button type="submit" class="btn btn-primary">Proses</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>