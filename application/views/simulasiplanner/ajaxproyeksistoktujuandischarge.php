<h4>Proyeksi Tujuan (<?=$this->Model->getdata('pelabuhan', array('id' => $idasal))->row()->nama?>)</h4>
<!-- <div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr class="alert alert-warning">
                <th>Stok Awal <?=@$tanggalreal?></th>
                <th>Mutasi/Hari</th>

                <th>Stok Intransit <?=@$tanggalreal?></th>

                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <th class="alert alert-primary">
                    Stok <?=$row['tanggal']?>
                </th>
                <?php }}?>

            </tr>
        </thead>
        <tbody>
            <tr class="alert alert-warning">
                <td><?=@$stokreal?></td>
                <td><?=@$mutasi?></td>
                <td><?=@$stokintransit?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    Stok <?=$row['stok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Ketahanan Stok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ketahanan']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Deadstok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['deadstok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Safestok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['safestok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Ullage</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ullage']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Mutasi</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0': '+'.$jumlah?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Stok Akhir</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['stok']+$jumlah?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="3">Ullage Akhir</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ullage']-$jumlah?>
                </td>
                <?php }}?>
            </tr>
        </tbody>
    </table>
</div> -->

<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Keterangan</th>
                <th><?=@$tanggalreal?></th>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <th class="alert alert-primary">
                    Stok <?=$row['tanggal']?>
                </th>
                <?php }}?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Pumpable
                </td>
                <td><?=@$stokreal?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=$row['stok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td>
                    Ullage
                </td>
                <td><?=@$ullagereal?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ullage']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td>
                    Rencanca Discharge
                </td>
                <td><?=@$stokdischarge?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['stokdischargeloop']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td>
                    Waiting Ullage (volume)
                </td>
                <td><?=@$stokdischarge - @$ullagereal?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0': ($row['stokdischargeloop']-$row['ullage'] < 0 ? 0 : $row['stokdischargeloop']-$row['ullage'])?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td>
                    Waiting Ullage (Hari)
                </td>
                <td><?=(@$stokdischarge - @$ullagereal) / @$dotreal?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0': (($row['stokdischargeloop']-$row['ullage'])/ @$dotreal < 0 ? 0 : ($row['stokdischargeloop']-$row['ullage'])/ @$dotreal . '<input type="hidden" name="waiting_ullage" id="waiting_ullage" value="'.($row['stokdischargeloop']-$row['ullage'])/ @$dotreal.'"/> <button class="btn btn-xs btn-danger btn_setwaiting_ullage">Set Waiting Ullage</button>')?>
                </td>
                <?php }}?>
            </tr>
        </tbody>
    </table>
</div>