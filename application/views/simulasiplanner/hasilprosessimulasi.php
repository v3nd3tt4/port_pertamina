<main class="main-content bgc-grey-100">
	<div id="mainContent">
		<div class="row gap-20 masonry pos-r" style="position: relative; height: 1128px;">
			<div class="masonry-sizer col-md-6">
			</div>
			<div class="masonry-item w-100">
				<div class="row gap-20">
					<div class="col-md-6">
						<div class="bgc-white p-20 bd">
							<h6 class="c-grey-900">Hasil Simulasi</h6>
							<div class="mT-30">
								<table class="table table-striped">
									<tr>
										<td>Asal</td>
										<td><?=$this->Model->getdata('pelabuhan', array('id' => $datawaktu['idasal']))->row()->nama?></td>
									</tr>
									<tr>
										<td>Tujuan</td>
										<td><?=$this->Model->getdata('pelabuhan', array('id' => $datawaktu['idtujuan']))->row()->nama?></td>
									</tr>
									<tr>
										<td>Kapal</td>
										<td><?=$this->Model->getdata('kapal', array('id' => $datawaktu['idkapal']))->row()->nama?></td>
									</tr>
									<tr>
										<td>Produk</td>
										<td><?=$this->Model->getdata('produk', array('id' => $datawaktu['idproduk']))->row()->nama?></td>
									</tr>
									<tr>
										<td>Jumlah</td>
										<td><?=$datawaktu['jumlah']?></td>
									</tr>
									<tr>
										<td>Waktu datang</td>
										<td><?=$datawaktu['waktu_datang']?></td>
									</tr>
									<tr>
										<td>Waktu Sandar</td>
										<td><?=$datawaktu['waktu_sandar']?></td>
									</tr>
									<tr>
										<td>Waktu Mulai Bongkar Muat</td>
										<td><?=$datawaktu['waktu_comm']?></td>
									</tr>
									<tr>
										<td>Waktu Selesai Bongkar Muat</td>
										<td><?=$datawaktu['waktu_comp']?></td>
									</tr>
									<tr>
										<td>Waktu Lepas Pantai</td>
										<td><?=$datawaktu['waktu_lepas']?></td>
									</tr>
									<tr>
										<td>Waktu Berangkat</td>
										<td><?=$datawaktu['waktu_berangkat']?></td>
									</tr>
									<tr>
										<td>IPT</td>
										<td><?=$datawaktu['ipt']?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="bgc-white p-20 bd">
							<h6 class="c-grey-900">Asal</h6>
							<div class="mT-30">
								<table class="table table-striped">
									<tr>
										<td>Stok Awal</td>
										<td>: <?=$dataasal['stokawal']?></td>
									</tr>
									<tr>
										<td>Stok Mutasi</td>
										<td>: <?=$dataasal['stokmutasi']?></td>
									</tr>
									<tr>
										<td>Sisa</td>
										<td>: <?=$dataasal['sisa']?></td>
									</tr>
									<tr>
										<td>Ketahanan</td>
										<td>: <?=$dataasal['ketahanstok']?></td>
									</tr>
								</table>
							</div>
						</div>
						<br/>
						<div class="bgc-white p-20 bd">
							<h6 class="c-grey-900">Tujuan</h6>
							<div class="mT-30">
								<table class="table table-striped">
									<tr>
										<td>Stok Awal</td>
										<td>: <?=$datatujuan['stokawal']?></td>
									</tr>
									<tr>
										<td>Stok Mutasi</td>
										<td>: <?=$datatujuan['stokmutasi']?></td>
									</tr>
									<tr>
										<td>Sisa</td>
										<td>: <?=$datatujuan['sisa']?></td>
									</tr>
									<tr>
										<td>Ketahanan</td>
										<td>: <?=$datatujuan['ketahanstok']?></td>
									</tr>
								</table>
							</div>
						</div>
						<br/>
						<div class="bgc-white p-20 bd">
							<h6 class="c-grey-900"></h6>
							<div class="mT-30">
								<form action="<?=base_url().'simulasiplanner'?>/simpansimulasi" method="post">
									<textarea name="datawaktu" style="display: none" class="form-control"><?=json_encode($datawaktu)?></textarea>
									<textarea name="dataasal" style="display: none" class="form-control"><?=json_encode($dataasal)?></textarea>
									<textarea name="datatujuan" style="display: none" class="form-control"><?=json_encode($datatujuan)?></textarea>
								
									<button type="submit" class="btn btn-danger">Simpan simulasi</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</main>