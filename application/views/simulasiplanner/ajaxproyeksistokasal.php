<h4>Proyeksi Asal (<?=$this->Model->getdata('pelabuhan', array('id' => $idasal))->row()->nama?>)</h4>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr class="alert alert-warning">
                <th>Stok Awal <?=@$tanggalreal?></th>
                <th>Mutasi/Hari</th>

                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <th class="alert alert-primary">
                    Stok <?=$row['tanggal']?>
                </th>
                <?php }}?>

            </tr>
        </thead>
        <tbody>
            <tr class="alert alert-warning">
                <td><?=@$stokreal?></td>
                <td><?=@$mutasi?></td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    Stok <?=$row['stok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="2">Ketahanan Stok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ketahanan']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="2">Deadstok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['deadstok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="2">Safestok</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['safestok']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="2">Ullage</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?=($row['stok']=='0')?'0':$row['ullage']?>
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="2">Mutasi</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?php
                        // $k = "SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )";
                        // echo $k;
                        $ceksheament = $this->Model->kueri("SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )");
                        if($ceksheament->num_rows() != 0){
                            // var_dump(date('Y-m-d', strtotime($tanggalset)));
                            $seatime = $ceksheament->row()->seatime;
                            $waktuseatime = $this->Model->konversitimeminus($seatime);
                            $waktuseatimemundur = date('Y-m-d',strtotime($waktuseatime,strtotime($tanggalset)));
                            // echo $waktuseatimemundur.' '.$seatime;
                            if(strtotime($waktuseatimemundur) <= strtotime($row['tanggal'])){
                                $mutasi = '-'.$jumlah;
                            }else{
                                $mutasi = '-';
                            }
                            echo $mutasi;
                        }else{
                            echo 'sheatime belum ditemukan';
                        }                   
                        
                    ?>
                    
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="2">Stok Akhir</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?php
                        // $k = "SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )";
                        // echo $k;
                        $ceksheament = $this->Model->kueri("SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )");
                        if($ceksheament->num_rows() != 0){
                            // var_dump(date('Y-m-d', strtotime($tanggalset)));
                            $seatime = $ceksheament->row()->seatime;
                            $waktuseatime = $this->Model->konversitimeminus($seatime);
                            $waktuseatimemundur = date('Y-m-d',strtotime($waktuseatime,strtotime($tanggalset)));
                            // echo $waktuseatimemundur.' '.$seatime;
                            if(strtotime($waktuseatimemundur) <= strtotime($row['tanggal'])){
                                $stokakhir = $row['stok'] - $jumlah;
                            }else{
                                $stokakhir = '-';
                            }
                            echo $stokakhir;
                        }else{
                            echo 'sheatime belum ditemukan';
                        }                   
                        
                    ?>
                    
                </td>
                <?php }}?>
            </tr>
            <tr>
                <td colspan="2">Ullage Akhir</td>
                <?php if(!empty($proyeksi)){foreach($proyeksi as $row){ ?>
                <td class="alert alert-primary">
                    <?php
                        // $k = "SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )";
                        // echo $k;
                        $ceksheament = $this->Model->kueri("SELECT * FROM rute WHERE idkapal = '$idkapal' AND ((idasal = '$idasal' AND idtujuan = '$idtujuan') OR (idasal = '$idtujuan' AND idtujuan = '$idasal') )");
                        if($ceksheament->num_rows() != 0){
                            // var_dump(date('Y-m-d', strtotime($tanggalset)));
                            $seatime = $ceksheament->row()->seatime;
                            $waktuseatime = $this->Model->konversitimeminus($seatime);
                            $waktuseatimemundur = date('Y-m-d',strtotime($waktuseatime,strtotime($tanggalset)));
                            // echo $waktuseatimemundur.' '.$seatime;
                            if(strtotime($waktuseatimemundur) <= strtotime($row['tanggal'])){
                                $ullageakhir = $row['ullage'] - $jumlah;
                            }else{
                                $ullageakhir = '-';
                            }
                            echo $ullageakhir;
                        }else{
                            echo 'sheatime belum ditemukan';
                        }                   
                        
                    ?>
                    
                </td>
                <?php }}?>
            </tr>
        </tbody>
    </table>
</div>