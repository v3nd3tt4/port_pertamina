		<?php if(!empty($script)){ 
        	$this->load->view($script);
        }?>


</div>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-3"></div>
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <div class="chat-window" id="chat_window_1" style="">
                <div class="col-md-12" style="float: right;">

                    <div class="panel panel-default">
                        <div class="panel-heading top-bar">
                            <div class="col-md-8 col-xs-8">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> Chat - Petamina</h3>
                            </div>
                            <div class="col-md-4 col-xs-4" style="text-align: right;">
                                <a href="#"><span id="minim_chat_window" class="glyphicon glyphicon-minus icon_minim"></span></a>
                                <!-- <a href="#"><span class="glyphicon glyphicon-remove icon_close" data-id="chat_window_1"></span></a> -->
                            </div>
                        </div>
                        <div class="panel-body msg_container_base">
                            <div id="result_chat">
                                <?php foreach($isi_chat->result() as $row_isi_chat){?>
                                <div class="row msg_container <?=($row_isi_chat->id_user==$this->session->userdata('namauser'))?'base_sent':'base_receive'?>">
                                    <?php if($row_isi_chat->id_user==$this->session->userdata('namauser')) { ?>
                                    <div class="col-md-2 col-xs-2 avatar">
                                        <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                                    </div>
                                    <?php }?>
                                    <div class="col-md-10 col-xs-10">
                                        <div class="messages <?=($row_isi_chat->id_user==$this->session->userdata('namauser'))?'msg_sent':'msg_receive'?>">
                                            <h5><?=($row_isi_chat->id_user=='')?'Aplikasi':ucfirst($row_isi_chat->id_user)?></h5>
                                            <p <?=($row_isi_chat->id_user=='')?'class="text text-danger"':''?>><?=$row_isi_chat->isi_chat?></p>
                                            <time datetime="2009-11-13T20:00"><?=$row_isi_chat->tanggal?></time>
                                        </div>
                                    </div>
                                    <?php if($row_isi_chat->id_user!=$this->session->userdata('namauser')) { ?>
                                    <div class="col-md-2 col-xs-2 avatar">
                                        <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                                    </div>
                                    <?php }?>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <form id="form_pesan">
                            <div class="input-group">
                                
                                    <input id="btn-input" type="text" class="form-control input-sm chat_input" name="isi_chat" id="isi_chat" placeholder="Write your message here..." />
                                    <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                                    </span>
                                
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	</body>
	<script type="text/javascript">

    $(document).ready(function() {
        var $this = $('.panel-heading span.icon_minim');
        if (!$this.hasClass('panel-collapsed')) {
	        $this.parents('.panel').find('.panel-body').slideUp();
	        $this.addClass('panel-collapsed');
	        $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
	    }
    })

		setInterval(function(){
		 $("#result_chat").load(location.href + " #result_chat");
		 $('.msg_container_base').scrollTop($('.msg_container_base')[0].scrollHeight);
		}, 3000);
		
		$(document).on('click', '.panel-heading span.icon_minim', function (e) {
			e.preventDefault();
			var $this = $(this);
			if (!$this.hasClass('panel-collapsed')) {
				$this.parents('.panel').find('.panel-body').slideUp();
				$this.addClass('panel-collapsed');
				$this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
			} else {
				$this.parents('.panel').find('.panel-body').slideDown();
				$this.removeClass('panel-collapsed');
				$this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
			}
		});
	$(document).on('focus', '.panel-footer input.chat_input', function (e) {
		e.preventDefault();
	    var $this = $(this);
	    if ($('#minim_chat_window').hasClass('panel-collapsed')) {
	        $this.parents('.panel').find('.panel-body').slideDown();
	        $('#minim_chat_window').removeClass('panel-collapsed');
	        $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
	    }
	});
	$(document).on('click', '#new_chat', function (e) {
		e.preventDefault();
	    var size = $( ".chat-window:last-child" ).css("margin-left");
	     size_total = parseInt(size) + 400;
	    alert(size_total);
	    var clone = $( "#chat_window_1" ).clone().appendTo( ".container" );
	    clone.css("margin-left", size_total);
	});
	$(document).on('click', '.icon_close', function (e) {
		e.preventDefault();
	    //$(this).parent().parent().parent().parent().remove();
	    $( "#chat_window_1" ).remove();
	});
	$(document).on('click', '#btn-chat', function(e){
		e.preventDefault();
		var data = $('#form_pesan').serialize();
		$('#form_pesan')[0].reset();
		$.ajax({
			url: '<?=base_url()?>welcome/simpan_chat',
			type: 'POST',
			data: data,
			dataType: 'JSON',
			success: function(msg){
				if(msg.status == 'sukses'){
					// location.reload();
				}else if(msg.status){

				}
			}
		});
	});
	</script>
</html>