<?php
  $hakakses=$this->session->userdata('akses');
?>
<nav class="navbar navbar-inverse" style="border-radius: 0px; background: #D90000; border: none;">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="<?=base_url()?>welcome">Home</a></li>
        <?php
        if($hakakses=='admin' || $hakakses=='root'){
        ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?=base_url()?>satuan">List Satuan</a></li>
            <li><a href="<?=base_url()?>status">List Status</a></li>
            <li><a href="<?=base_url()?>tipekapal">List Tipe Kapal</a></li>
            <li><a href="<?=base_url()?>waiting">List Waiting</a></li>
            <li><a href="<?=base_url()?>keterangan">List Keterangan</a></li>
            <li><a href="<?=base_url()?>produk">List Produk</a></li>
            <li><a href="<?=base_url()?>pelabuhan">List Pelabuhan</a></li>
            <li><a href="<?=base_url()?>kapal">List Kapal</a></li>
            <li><a href="<?=base_url()?>rute">List Rute Kapal</a></li>
            <li><a href="<?=base_url()?>jetty">List Jetty</a></li>
            <li><a href="<?=base_url()?>user">List User</a></li>
          </ul>
        </li>
        <?php
        }elseif($hakakses=='planner lpg'||$hakakses=='planner minyak' || $hakakses=='root'){
        ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Planner <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?=base_url()?>stok">List Stok</a></li>
            <li><a href="<?=base_url()?>sandarjetty">List Sandar Jetty</a></li>
            <li><a href="<?=base_url()?>estimasiwaktu">List Estimasi Waktu</a></li>
            <li><a href="<?=base_url()?>simulasiplanner">Simulasi Planner</a></li>
          </ul>
        </li>
        <?php 
        }
        ?>
        
        
        <li><a href="<?=base_url()?>laporan">Report</a></li> 
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> <?=$this->session->userdata('namauser')?></a></li>
        <li><a href="<?=base_url()?>login/proseslogout"><span class="glyphicon glyphicon-log-in"></span> logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">

        <?=$breadcumb?>