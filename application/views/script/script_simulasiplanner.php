<script type="text/javascript" src="<?=base_url()?>assets/jquery.datetimepicker.full.min.js"></script>
<link href="<?=base_url()?>assets/jquery.datetimepicker.min.css" rel="stylesheet">

<script type="text/javascript">
$(document).ready(function(){
	$('#waktudatang').datetimepicker({
      format:'Y-m-d H:i:s',
  });

  $('#waktuarrivaledit').datetimepicker({
      format:'Y-m-d H:i:s',
  });

  $('.datepick').datetimepicker({
      format:'Y-m-d H:i:s',
  })

  $('.hidewaktu').hide();

  


	$(document).on('click', '.set_waktu', function(e){
		e.preventDefault();
		var waktu_datang = $('#waktudatang').val();
    var jumlah = $('#jumlah').val(); 
    
		if(waktu_datang == ''){
			alert('waktu datang harus diisi');
		}else if(jumlah == ''){

    }else{
      var data = $('#form_simulasi').serialize();
			$.ajax({
				url: '<?=base_url()?>simulasiplanner/getWaktu',
				type: 'POST',
				data: data, 
				dataType: 'JSON',
				success: function(msg){
          $('#waktu_sandar').val(msg.waktu_sandar);
          $('#waktu_comm').val(msg.waktu_comm);
          $('#waktu_comp').val(msg.waktu_comp);
          $('#waktu_lepas').val(msg.waktu_lepas);
          $('#waktu_berangkat').val(msg.waktu_berangkat);
          $('#ipt').val(msg.ipt);
				}
			});
		}        			
	});

    $(document).on('click','.edit_row', function(e){
        e.preventDefault();
        loadTable();
        var id = $(this).attr('id');
          $.ajax({
              url: '<?=base_url()?>simulasiplanner/ambil_shipment',
              type: 'POST',
              data: 'id='+id,
              dataType: 'JSON',
              success: function(msg){
                  $('#e_waktudatang').val(msg.arrival);
                  $('#e_idkapal').val(msg.idkapal);
                  $('#e_waktu_sandar').val(msg.berthed);
                  $('#e_waktu_comm').val(msg.comm);
                  $('#e_waktu_comp').val(msg.comp);
                  $('#e_waktu_lepas').val(msg.unberthed);
                  $('#e_waktu_berangkat').val(msg.departure);
                  $('#e_ipt').val(msg.ipt);
                  $('#id').val(msg.id);
              }
          });
    });

    function loadTable() {
          $('#tampiledit').load('<?=base_url()?>simulasiplanner/editsimulasi',function(){})
    };

        

        $(document).on('change', '#waktudatang', function(e){
                e.preventDefault();
                var waktu_datang = $('#waktudatang').val();
                var produk = $('#idproduk').val();
                var jumlah = $('#jumlah').val();
                if(produk == '' || jumlah == ''){
                        alert('pilih terlebih dahulu kapal, asal, tujuan, produk, jumlah produk, satuan sebelum memilih waktu datang');
                }else{
                        var data = $('#form_simulasi').serialize();
                        $.ajax({
                                url: '<?=base_url()?>simulasiplanner/getWaktu',
                                type: 'POST',
                                data: data, 
                                dataType: 'JSON',
                                success: function(msg){
                                        $('#waktu_sandar').val(msg.waktu_sandar);
                                        $('#waktu_comm').val(msg.waktu_comm);
                                        $('#waktu_comp').val(msg.waktu_comp);
                                        $('#waktu_lepas').val(msg.waktu_lepas);
                                        $('#waktu_berangkat').val(msg.waktu_berangkat);
                                        $('#ipt').val(msg.ipt);
                                }
                        });
                }                               
        });
        
        $(document).on('click', '.btn_tambah_produk', function(e){
            e.preventDefault();

            var html = '';
            var idproduk = $('#idproduk').val();
            var namaproduk = $('#idproduk option:selected').text();
            var jumlah = $('#jumlah').val();
            var idsatuan = $('#idsatuan').val();
            var namasatuan = $('#idsatuan option:selected').text();
            // var tgldatang = $('#waktudatang').val();
            if(idproduk=='' || jumlah == '' || idsatuan==''){
                alert('produk, jumlah, dan satuan harus diisi ');
            }else{
                html += '<tr>';
                html += '<td><input type="hidden" name="idproduk1[]" value="'+idproduk+'" class="form-control" readonly/></td>';
                html += '<td><input type="text" name="namaproduk[]" value="'+namaproduk+'" class="form-control" readonly/></td>';
                html += '<td><input type="text" name="jumlahproduk[]" value="'+jumlah+'" class="form-control"  readonly/></td>';
                html += '<td><input type="hidden" name="idsatuan1[]" value="'+idsatuan+'" class="form-control" readonly/></td>';
                html += '<td><input type="text" name="namasatuan[]" value="'+namasatuan+'" class="form-control" readonly/></td>';
                html += '<td><button type="button" class="btn btn-sm btn-danger btn_hapus_row">Hapus</button><button type="button" class="btn btn-sm btn-warning btn_cek_proyeksi" value="'+idproduk+'_'+jumlah+'">Cek Proyeksi</button></td>';
                html += '</tr>';
                $('#resulttambahproduk').append(html);
            }
            
        });

        $(document).on('click', '.btn_hapus_row', function(e){
            e.preventDefault();
             $(this).closest('tr').remove();
        });

        $(document).on('click', '.btn_cek_proyeksi', function(e){
            e.preventDefault();
            
            var produk = $(this).val();
            // alert(produk);
            var idasal = $('#idasal').val();
            var idtujuan = $('#idtujuan').val();
            var idkapal = $('#idkapal').val();
            var tgldatang = $('#waktudatang').val();
            var proses = $('#proses').val();
            // alert(tgldatang);
            // $("#result_proyeksi_stok_asal").load('<?=base_url()?>simulasiplanner/proyeksistokasal/'+produk+'/'+idasal,functionf() {
            // });
            // $("#result_proyeksi_stok_tujuan").load('<?=base_url()?>simulasiplanner/proyeksistoktujuan/'+produk+'/'+idtujuan,function() {
            // });
            if(idkapal == '' || idtujuan == '' || idasal == ''){
                alert("kapal, tujuan dan asal harus diisi");
            }else{
                if(proses == '1'){
                  $('#modal_proyeksi').modal();
                  $.ajax({
                      url: '<?=base_url()?>simulasiplanner/proyeksistoktujuandischarge',
                      data: 'idproduk='+produk+'&idpelabuhan='+idtujuan+'&tgldatang='+tgldatang+'&idtujuan='+idasal+'&idkapal='+idkapal,
                      type: 'POST',
                      success: function(msg){
                          $("#result_proyeksi_stok_tujuan").html(msg);
                      }
                  });
                }else if(proses == '0'){
                  $('#modal_proyeksi').modal();
                  $.ajax({
                      url: '<?=base_url()?>simulasiplanner/proyeksistokasal',
                      data: 'idproduk='+produk+'&idpelabuhan='+idasal+'&tgldatang='+tgldatang+'&idtujuan='+idtujuan+'&idkapal='+idkapal,
                      type: 'POST',
                      success: function(msg){
                          $("#result_proyeksi_stok_asal").html(msg);
                      }
                  });
                  $.ajax({
                      url: '<?=base_url()?>simulasiplanner/proyeksistoktujuan',
                      data: 'idproduk='+produk+'&idpelabuhan='+idtujuan+'&tgldatang='+tgldatang+'&idtujuan='+idasal+'&idkapal='+idkapal,
                      type: 'POST',
                      success: function(msg){
                          $("#result_proyeksi_stok_tujuan").html(msg);
                      }
                  });
                }
                
            }
            
           
        });

        $(document).on('click', '.btn_setwaiting_ullage', function(e){
          e.preventDefault();
          var waiting_ullage = $('#waiting_ullage').val();
          
          var val_waiting_ullage = $('#val_waiting_ullage').val();
           // $('#val_waiting_ullage').val(waiting_ullage);
          if(val_waiting_ullage == ''){
            $('#val_waiting_ullage').val(waiting_ullage);
            $('#result_waiting_ullage').html('Waiting Ullage adalah '+waiting_ullage+' x 24 jam');
          }else{
            $('#val_waiting_ullage').val(parseInt(val_waiting_ullage,10)+parseInt(waiting_ullage,10));
            $('#result_waiting_ullage').html('Waiting Ullage adalah '+ (parseInt(val_waiting_ullage,10)+parseInt(waiting_ullage,10))+ ' x 24 jam');
          }
          
          $('#modal_proyeksi').modal('hide');
        });
});


$("#pilihproduk").change(function(e) {
        $("#loadernya").show();
        $("#proyeksistok").empty();
        var produk = $('#pilihproduk').val();
        if(produk!='') {
        $("#proyeksistok").load('<?=base_url()?>simulasiplanner/proyeksistok/'+produk,function() {
          $("#loadernya").hide();
        })
        }
});

$(document).on('click', '.hapus-planner', function(e){
      e.preventDefault();
      $('#modal_hapus_planner').modal();
      var id = $(this).attr('id');
      $(document).on('click','.ya_hapus_planner', function(e){
          e.preventDefault();
          $('#notif_hapus_planner').html('Loading...');
          $.ajax({
              url: '<?=base_url()?>simulasiplanner/hapus_planner',
              type: 'POST',
              data: 'id='+id,
              success: function(msg){
                  $('#notif_hapus_planner').html(msg);                    
              }
          });
      });
  });

</script>